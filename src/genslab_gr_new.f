c     ==================================================================
      program main
c     ==--------------------------------------------------------------==
c     == This is a utility program to generate a graphene structure   ==
c     ==--------------------------------------------------------------==
      implicit none
      integer :: iatom,natom
      integer, parameter :: matom = 400
      integer :: zatom
      integer :: ii,ilayer,nlayer,nvaclayer,i1,i2,n1,n2
      integer :: ivac
      double precision :: a_0, c_0, a, a_sf, bohr,rlaydist,rvac,z
      double precision :: cslb(3)
      double precision :: a1(3),a2(3),a3(3),a1_sc(3),a2_sc(3),a3_sc(3)
      double precision :: tau(3,matom),taus(3,matom),tau_tmp(3)
      double precision :: tau0_tmp(3,4)
      double precision :: vac
      character(len=2) :: atmnm
      character(len=3) :: num
      character(len=20) :: filen
      character(len=256) :: str
      integer :: input,iout,stdin,stdout,stderr, ios,len
      integer :: ixyz,iatps
      character(len=80) :: infile,outfile
      logical :: fexist
c ... arguments
      integer :: iarg, iargc, narg, marg
      character(len=80) :: arg, arg2
c ... options
      integer :: ipw=0, istate=0
c ... others
      integer :: idum
      character(len=2) :: c2
c     ==--------------------------------------------------------------==
c     == set constants                                                ==
c     ==--------------------------------------------------------------==
      stdin  = 05
      stdout = 06
      stderr = 00
      input  = 10
      iout   = 20
      ixyz   = 20
      bohr = 0.529177
      num = '   '
      atmnm = 'C'
      a_0 = 0.d0
      c_0 = 0.d0
      n1 = 1
      n2 = 1
c     ==--------------------------------------------------------------==
c ... arguments
      narg=iargc()
      iarg=0
      loop_arg: do while(iarg<narg)
        iarg=iarg+1
        call getarg(iarg,arg)
        select case(trim(arg))
          case('-pw') 
            ipw=1
          case('-state')
            istate=1
          case('-e')
            iarg=iarg+1
            call getarg(iarg,arg2)
            atmnm=arg2
          case('-a')
            iarg=iarg+1
            call getarg(iarg,arg2)
            read(arg2,*,iostat=ios)a_0
            if(ios /= 0)then
              write(c2,'(i2)')iarg
              c2=adjustl(c2)
              write(stderr,'(a,a)')
     &        '*** ERROR occurs while reading the argument# ',trim(c2)
            endif
          case('-c')
            iarg=iarg+1
            call getarg(iarg,arg2)
            read(arg2,*,iostat=ios)c_0
            if(ios/=0)then
              c2=adjustl(c2)
              write(stderr,'(a,a)')
     &        '*** ERROR occurs while reading the argument# ',trim(c2)
            endif 
          case('-s')
            iarg=iarg+1
            call getarg(iarg,arg2)
            read(arg2,*,iostat=ios)n1
            if(ios/=0)then
              c2=adjustl(c2)
              write(stderr,'(a,a)')
     &        '*** ERROR occurs while reading the argument# ',trim(c2)
            endif 
            iarg=iarg+1
            call getarg(iarg,arg2)
            read(arg2,*,iostat=ios)n2
            if(ios/=0)then
              c2=adjustl(c2)
              write(stderr,'(a,a)')
     &        '*** ERROR occurs while reading the argument# ',trim(c2)
            endif 
          case('-n1')
            iarg=iarg+1
            call getarg(iarg,arg2)
            read(arg2,*,iostat=ios)n1
            if(ios/=0)then
              c2=adjustl(c2)
              write(stderr,'(a,a)')
     &        '*** ERROR occurs while reading the argument# ',trim(c2)
            endif 
          case('-n2')
            iarg=iarg+1
            call getarg(iarg,arg2)
            read(arg2,*,iostat=ios)n2
            if(ios/=0)then
              c2=adjustl(c2)
              write(stderr,'(a,a)')
     &        '*** ERROR occurs while reading the argument# ',trim(c2)
            endif 
        end select 
      enddo loop_arg
      if(abs(a_0 * c_0) .gt. 1.d-6)then
        input = stdin
      endif 
c     ==--------------------------------------------------------------==
      stop
c     ==--------------------------------------------------------------==
      if(input/=stdin)then
        write(stdout,'(a,$)')'Enter input file> '
        read(stdin,'(a)')infile
      endif
      write(stdout,'(a,$)')'Enter output file> '
      read(stdin,'(a)')outfile
      if(input/=stdin)then
        len=index(infile,' ')
        write(stdout,'(/,/,a,a)')'Input file : ',infile(:len-1)
      endif
      len=index(outfile,' ')
      write(stdout,'(a,a)')    'Output file: ',outfile(:len-1) 
      if(input/=stdin)then
        inquire(file=infile,exist=fexist)
        if(.not.fexist)then
          len=index(infile,' ')
          write(stderr,'(/,a,a)') infile(:len-1),' does not exist.'
          stop
        endif
      endif
      if(input/=stdin)then
        open(input,file=infile,iostat=ios,status='old')
        if(ios.ne.0)then
          write(stderr,'(a)')
     &    '*** ERROR occurs while opening the input file.'
          stop
        endif
      endif
      open(iout,file=outfile,status='unknown')
c     ==--------------------------------------------------------------==
      write(iout,'(/,a,a,a,/,a,a,a,/,a,a,a,/)')
     &' ***',
     &'***********************************************************',
     &'****',
     &' ***',
     &' This program generates a graphene structure               ',
     &' ***',
     &' ***',
     &'***********************************************************',
     &'****'
c     ==--------------------------------------------------------------==
      if(input/=stdin)then
        read(input,'(a)') atmnm
        read(input,*) a_0, c_0
        read(input,*) n1, n2
      endif
      a_0 = dble(a_0)
      c_0 = dble(c_0)
      nlayer = 1
      nvaclayer = 1
c     ==--------------------------------------------------------------==
c     == set lattice vectors                                          ==
c     ==--------------------------------------------------------------==
      do ii = 1, 3
        a1(ii) = 0.0d0
        a2(ii) = 0.0d0
        a3(ii) = 0.0d0
      enddo
      a = a_0
      a_sf = a_0
      rlaydist = c_0 * 0.5d0
      rvac = rlaydist * dble(nvaclayer)
      a1(1) = a
      a1(2) = 0.0d0
      a2(1) = -0.5d0 * a
      a2(2) =  0.5d0 * sqrt(3.d0) * a
      a3(3) = c_0
      a1_sc(:) = a1(:) * dble(n1)
      a2_sc(:) = a2(:) * dble(n2)
      a3_sc(:) = a3(:)
c     center of slab (cslb)
      cslb(1) = 0.0d0
      cslb(2) = 0.0d0
      cslb(3) = 0.0d0
c     ==--------------------------------------------------------------==
      natom = 2 * n1 * n2
      iatom = 0
      do i2 = 1, n2
        do i1 = 1, n1
          tau0_tmp(1,1) = 0.0d0
          tau0_tmp(2,1) = 0.0d0
          tau0_tmp(3,1) = 0.0d0
          tau0_tmp(1,2) = 1.d0/3.d0
          tau0_tmp(2,2) = 2.d0/3.d0
          tau0_tmp(3,2) = 0.0d0
          iatom = iatom + 1
          tau(1,iatom) = tau0_tmp(1,1) * a1(1) + tau0_tmp(2,1) * a2(1)
          tau(2,iatom) = tau0_tmp(1,1) * a1(2) + tau0_tmp(2,1) * a2(2)
          tau(3,iatom) = tau0_tmp(3,1)
          tau(1,iatom) = tau(1,iatom) 
     &                   + dble(i1-1) * a1(1) + dble(i2-1) * a2(1)
          tau(2,iatom) = tau(2,iatom) 
     &                   + dble(i1-1) * a1(2) + dble(i2-1) * a2(2)
          iatom = iatom + 1
          tau(1,iatom) = tau0_tmp(1,2) * a1(1) + tau0_tmp(2,2) * a2(1)
          tau(2,iatom) = tau0_tmp(1,2) * a1(2) + tau0_tmp(2,2) * a2(2)
          tau(3,iatom) = tau0_tmp(3,2)
          tau(1,iatom) = tau(1,iatom) 
     &                   + dble(i1-1) * a1(1) + dble(i2-1) * a2(1)
          tau(2,iatom) = tau(2,iatom) 
     &                   + dble(i1-1) * a1(2) + dble(i2-1) * a2(2)
        enddo
      enddo
c     ==--------------------------------------------------------------==
      write(iout,*)
      write(iout,'(a,f18.8)')' a_0= ',a_0
      write(iout,'(a,f18.8)')' c_0= ',c_0
      write(iout,'(a,f18.8)')' Inter layer distance: ',rlaydist
      write(iout,'(/,a,i2,a)')' Slab of ',nlayer,' ML'
      write(iout,'(a,i2,a)')
     &' Vacuum region corresponds to ',nvaclayer,' ML'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac,' Bohr'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac*Bohr
     &,' Angstrom'
      write(iout,'(a,f6.2,a)')
     &' Size of the supercell along the z axis: '
     &,abs(a3(3)),' Bohr'
      write(iout,'(a,f6.2,a)')
     &' Size of the supercell along the z axis: '
     &,abs(a3(3))*Bohr,' Angstrom'
      write(iout,'(/,a)')      ' Primitive lattice vectors:'
      write(iout,'(3f20.12)')(a1(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3(ii),ii=1,3)
      write(iout,'(/,a)')      ' Supercell lattice vectors:'
      write(iout,'(3f20.12)')(a1_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3_sc(ii),ii=1,3)
      write(iout,'(a,i5)')   ' Number of atoms:',natom
      write(iout,'(a)')      ' Atoms: '
      do iatom=1,natom
        write(iout,'(3f20.12,3i5)')(tau(ii,iatom),ii=1,3),1,1,1
      enddo 
      write(iout,'(/,a)')
     &' Atoms (center of the slab is set to the origin):'
      do iatom=1,natom
        write(iout,'(3f20.12,3i5)')(tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
      enddo 
      if(ipw.eq.1)then
c ... PW output
        filen='atps'
        open(iatps,file=filen,status='unknown')
        write(iatps,'(a)')       ' ibrav = 4'
        write(iatps,'(a,i3)')    ' nat   =',natom
        write(iatps,'(a,i1)')    ' ntyp  =',1
        write(iatps,'(a,f20.12)')' celldm(1) = ',a1_sc(1)
        write(iatps,'(a,f20.12)')' celldm(3) = ',a3_sc(3)/a1_sc(1)
        write(iatps,'(a)')'CELL_PARAMETERS (bohr)'
        write(iatps,'(3f20.12)')(a1_sc(ii),ii=1,3)
        write(iatps,'(3f20.12)')(a2_sc(ii),ii=1,3)
        write(iatps,'(3f20.12)')(a3_sc(ii),ii=1,3)
        write(iatps,'(a)')'ATOMIC_POSITIONS (bohr)'
        do iatom=1,natom
          write(iatps,'(a,3f20.12,3i5)')
     &    trim(atmnm),(tau(ii,iatom),ii=1,3),1,1,1
        enddo 
        close(iatps) 
c ... PW output (Angstrom)
        filen='atps_angstrom'
        open(iatps,file=filen,status='unknown')
        write(iatps,'(a)')       ' ibrav = 4'
        write(iatps,'(a,i3)')    ' nat   =',natom
        write(iatps,'(a,i1)')    ' ntyp  =',1
        write(iatps,'(a,f20.12)')' A = ',a1_sc(1)*bohr
        write(iatps,'(a,f20.12)')' C = ',a3_sc(3)*bohr
        write(iatps,'(a)')'CELL_PARAMETERS (angstrom)'
        write(iatps,'(3f20.12)')(a1_sc(ii)*bohr,ii=1,3)
        write(iatps,'(3f20.12)')(a2_sc(ii)*bohr,ii=1,3)
        write(iatps,'(3f20.12)')(a3_sc(ii)*bohr,ii=1,3)
        write(iatps,'(a)')'ATOMIC_POSITIONS (angstrom)'
        do iatom=1,natom
          write(iatps,'(a,3f20.12,3i5)')
     &    trim(atmnm),(tau(ii,iatom)*bohr,ii=1,3),1,1,1
        enddo 
        close(iatps) 
      endif
c     ==--------------------------------------------------------------==
c     == generate XSF file                                            ==
c     ==--------------------------------------------------------------==
      if(natom.lt.10)then
        write(num,'(i1)')natom
        filen=trim(atmnm)//num(1:1)//'.xsf'
      elseif(natom.lt.100)then
        write(num,'(i2)')natom
        filen=trim(atmnm)//num(1:2)//'.xsf'
      elseif(natom.lt.1000)then
        write(num,'(i3)')natom
        filen=trim(atmnm)//num(1:3)//'.xsf'
      endif
      write(*,'(/,a,a)')'Generating ',filen
      open(ixyz,file=filen,status='unknown')
      write(ixyz,'(a)')' SLAB'
      write(ixyz,'(a)')' PRIMVEC'
      write(ixyz,'(3f15.6)')(a1_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(3f15.6)')(a2_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(3f15.6)')(a3_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(a)')' PRIMCOORD'
      idum=1
      write(ixyz,'(i5,i1)')natom,idum
      do iatom=1,natom
        idum=zatom(trim(atmnm))
        write(ixyz,'(i5,3(f15.9,2x))')idum
     &  ,(tau(ii,iatom)*bohr,ii=1,3)
      enddo
      close(ixyz)
c     ==--------------------------------------------------------------==
      stop
      end
c     ==================================================================
