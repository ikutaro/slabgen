      program main
!     ==--------------------------------------------------------------==
!     == This is a utility program to generate an fcc (111)           ==
!     ==--------------------------------------------------------------==
      implicit none
      integer :: iatm,iatmp,natm,matm
      integer :: ii,jj,ilayer,nlayer,nvaclayer,i1,i2,n1,n2
      integer :: it1,it2,is1,is2,ie1,ie2
      real(kind=8) :: d111,d322
      real(kind=8) :: a_0, a, a_sf,bohr,rlaydist,rvac,z,area,vol
      real(kind=8) :: theta, cost, shift
      real(kind=8) :: a1(3),a2(3),a3(3),a1_sc(3),a2_sc(3),a3_sc(3)
      real(kind=8), allocatable :: tau(:,:),taus(:,:), tau_new(:,:)
      real(kind=8) :: tau0_tmp(3),cslb(3)
      character(len=2) :: atmnm
      character(len=3) :: num
      integer :: input,iout,ixsf,stdin,stdout,stderr,ios,len
      character(len=80) :: infile,outfile,filen,str
      integer :: icps
      character(len=80) :: cpsfile
      logical :: fexist
      real(kind=8) :: sum
      real(kind=8) :: xmat(3,3),xmati(3,3)
      real(kind=8) :: ab(3),bc(3),ca(3)
      real(kind=8) :: pi
      integer :: zatom
!     ==--------------------------------------------------------------==
!     == set constants                                                ==
!     ==--------------------------------------------------------------==
      stdin =05
      stdout=06
      stderr=00
      input =10
      iout  =20
      ixsf  =25
      bohr=0.529177d0
      pi=4.d0*atan(1.d0)
!     cost=cos(theta), where theta is angle between (332) and (111) planes
!     for (332): cos(theta)= (7*sqrt(6)/6) / sqrt(17/2)
      cost=(7.d0*sqrt(6.d0)/6.d0) / sqrt(8.5d0)
      theta=acos(cost)
!     ==--------------------------------------------------------------==
      write(stdout,'(a,$)') 'Enter input file> '
      read(stdin,'(a)') infile
      write(stdout,'(/,a,a,a)')'Input file : ',trim(infile)
      write(stdout,'(a,$)') 'Enter output file> '
      read(stdin,'(a)') outfile
      len=index(outfile,' ')
      write(stdout,'(/,a,a,a)')'Output file: ',trim(outfile)
      inquire(file=infile,exist=fexist)
      if(.not.fexist)then
        write(stderr,'(/,a,a)') trim(infile),' not found.'
        stop
      endif
      open(input,file=infile,iostat=ios,status='old')
      if(ios.ne.0)then
        write(stderr,'(a,a,a)')&
     &  '*** ERROR occurs while opening ',trim(infile)
        stop
      endif
      open(iout,file=outfile,status='unknown')
!     ==--------------------------------------------------------------==
      write(iout,'(/,a,a,a,/,a,a,a/,a,a,a,/)')&
     &' ***',&
     &'***********************************************************',&
     &'****',&
     &' ***',&
     &' This program generates an fcc (322) (nxm) clean surface   ',&
     &' ***',&
     &' ***',&
     &'***********************************************************',&
     &'****'
!     ==--------------------------------------------------------------==
      read(input,'(a)') atmnm ! Atomic species
      read(input,*) a_0       ! Lattice constant of the bulk crystal
      read(input,*) nlayer    ! # of atomic layers
      read(input,*) nvaclayer ! # of atomic layers for vacuum
      read(input,*) n1,n2     ! supercell dimension (n1 is dummy at present)
!
!     ==--------------------------------------------------------------==
!     == set lattice vectors                                          ==
!     ==--------------------------------------------------------------==
      a_0=dble(a_0)
      a_sf=a_0/sqrt(2.0d0)     ! Lattice constant for (111) surface
!                              ! (edge of trianglar lattice)
      rlaydist=a_0/sqrt(3.0d0) ! Interlayer distance for (111)
      rlaydist=rlaydist/cost   ! Converted to that for (322)
      rvac=rlaydist*dble(nvaclayer) ! Vaccum thickness 
      d111=a_0/sqrt(3.0d0)
      d322=d111/cost
! ... set the lattice constant
      a1(:)=0.d0; a2(:)=0.d0; a3(:)=0.d0
      a1_sc(:)=0.d0; a2_sc(:)=0.d0; a3_sc(:)=0.d0
      a1(1)=sqrt(17.d0)*a_sf
      a2(2)=a_sf
      a3(3)=rlaydist*dble(nlayer)+rlaydist*dble(nvaclayer)
      do ii=1,2
        a1_sc(ii)=dble(n1)*a1(ii)
        a2_sc(ii)=dble(n2)*a2(ii)
        a3_sc(ii)=a3(ii)
      enddo
      a1_sc(3)=a1(3)
      a2_sc(3)=a2(3)
      a3_sc(3)=a3(3)
      ab(1)=a1_sc(2)*a2_sc(3)-a1_sc(3)*a2_sc(2)
      ab(2)=a1_sc(3)*a2_sc(1)-a1_sc(1)*a2_sc(3)
      ab(3)=a1_sc(1)*a2_sc(2)-a1_sc(2)*a2_sc(1)
      area=0.0d0
      do ii=1,3
        area=area+ab(ii)*ab(ii)
      enddo
      area=abs(area)
      area=sqrt(area)
      vol=0.0d0
      do ii=1,3
        vol=vol+ab(ii)*a3_sc(ii)
      enddo
      vol=abs(vol)
! ... center of slab
      cslb(1)=0.d0
      cslb(2)=0.d0
      cslb(3)=-rlaydist*dble(nlayer-1)*0.5d0
!     ==--------------------------------------------------------------==
!     == set atomic positions                                         ==
!     ==--------------------------------------------------------------==
      matm=6*nlayer*n1*n2
      allocate(tau(3,matm),taus(3,matm))
      allocate(tau_new(3,matm))
      tau(:,:)=0.d0
      taus(:,:)=0.d0
      tau_new(:,:)=0.d0
! ... first, set the (111) planes
      iatm=0
      do ilayer=1,nlayer
        z=(-1.0d0)*dble(ilayer-1)*d111
        if(mod(ilayer,3).eq.1)then
          do ii=1,5
            iatm=iatm+1
            tau(1,iatm)=dble(ii-1)*0.5d0*sqrt(3.d0)*a_sf
            tau(2,iatm)=dble(mod(ii,2)-1)*0.5d0*a_sf
            tau(3,iatm)=z
          enddo
        elseif(mod(ilayer,3).eq.2)then
          shift=-a_sf/sqrt(3.d0)
          do ii=1,6
            iatm=iatm+1
            tau(1,iatm)=dble(ii-1)*0.5d0*sqrt(3.d0)*a_sf + shift
            tau(2,iatm)=dble(mod(ii,2)-1)*0.5d0*a_sf
            tau(3,iatm)=z
          enddo
        elseif(mod(ilayer,3).eq.0)then
          shift=+a_sf/sqrt(3.d0)
          do ii=1,5
            iatm=iatm+1
            tau(1,iatm)=dble(ii-1)*0.5d0*sqrt(3.d0)*a_sf + shift
            tau(2,iatm)=dble(mod(ii,2)-1)*0.5d0*a_sf
            tau(3,iatm)=z
          enddo
        endif
      enddo
      natm=iatm
! ... rotate around y axis
      call roty(tau,natm,theta) 
!     ==--------------------------------------------------------------==
!     do ii=1,3
!       xmat(ii,1)=a1(ii)
!       xmat(ii,2)=a2(ii)
!       xmat(ii,3)=a3(ii)
!     enddo
!     call matinv(xmat,xmati,sum)
!     do iatm=1,natm
!       do ii=1,3
!         sum=0.0d0
!         do jj=1,3
!           sum=sum+xmati(ii,jj)*tau(jj,iatm)
!         enddo
!         taus(ii,iatm)=sum
!       enddo
!     enddo
!     ==--------------------------------------------------------------==
!     == generate supercell                                           ==
!     ==--------------------------------------------------------------==
      it1=(n1-1)/2
      it2=(n2-1)/2
      is1=-it1
      is2=-it2
      ie1=is1+n1-1
      ie2=is2+n2-1
      iatmp=0
      do i1=is1,ie1
        do i2=is2,ie2
          do iatm=1,natm
            iatmp=iatmp+1
            taus(1,iatmp)=tau(1,iatm)+dble(i1)*a1(1)+dble(i2)*a2(1)
            taus(2,iatmp)=tau(2,iatm)+dble(i1)*a1(2)+dble(i2)*a2(2)
            taus(3,iatmp)=tau(3,iatm)
          enddo
        enddo
      enddo
!     ==--------------------------------------------------------------==
!     == generate XSF file                                            ==
!     ==--------------------------------------------------------------==
      if(natm*n1*n2.lt.10)then
        write(num,'(i1)')natm*n1*n2
        filen=atmnm//num(1:1)//'.xsf'
      elseif(natm*n1*n2.lt.100)then
        write(num,'(i2)')natm*n1*n2
        filen=atmnm//num(1:2)//'.xsf'
      elseif(natm*n1*n2.lt.1000)then
        write(num,'(i3)')natm*n1*n2
        filen=atmnm//num(1:3)//'.xsf'
      endif
      write(*,'(/,a,a)')'Generating ',filen
      open(ixsf,file=filen,status='unknown')
      write(ixsf,'(a)')' CRYSTAL'
      write(ixsf,'(a)')' PRIMVEC'
      write(ixsf,'(3(f15.6))')bohr*a1_sc(:)
      write(ixsf,'(3(f15.6))')bohr*a2_sc(:)
      write(ixsf,'(3(f15.6))')bohr*a3_sc(:)
      write(ixsf,'(a)')' PRIMCOORD'
      write(ixsf,'(i3,i2)') natm*n1*n2,1
      do iatm=1,natm*n1*n2
        write(ixsf,'(a,3(f15.9,2x))')atmnm&
     &  ,(taus(ii,iatm)*bohr,ii=1,3)
      enddo
      close(ixsf)
!     ==--------------------------------------------------------------==
!     == output                                                       ==
!     ==--------------------------------------------------------------==
      write(iout,*)
      write(iout,'(a,f14.8,a)')' Lattice constant = ',a_0,' (Bohr)'
      write(iout,'(a,f14.8,a)')' Lattice constant = ',a_0*bohr,&
     &                        ' (Angstrom)'
      write(iout,*)
      write(iout,'(a,f14.8,a)')&
     &' Angle between (111) and (322) plane = ',&
     &theta/pi*180.d0,' (degree)'
      write(iout,*)
      write(iout,'(a,i2.2,a,i2.2)')' Supercell dimension : ',n1,'x',n2
      write(iout,*)
      write(iout,'(a)')' Lattice vectors (Bohr)'
      write(iout,'(3f20.12)')a1_sc(:)
      write(iout,'(3f20.12)')a2_sc(:)
      write(iout,'(3f20.12)')a3_sc(:)
      write(iout,*)
      write(iout,*)' Atomic positions (Bohr)'
      do iatm=1,natm*n1*n2
        write(iout,'(3f20.12,3i5)')taus(:,iatm),1,1,1
      enddo
      write(iout,*)
!     ==--------------------------------------------------------------==
      close(iout)
!     ==--------------------------------------------------------------==
      deallocate(tau,taus,tau_new)
!     ==--------------------------------------------------------------==
      stop
      end
!     ==================================================================
      subroutine roty(cps,natm,ang)
!     ==--------------------------------------------------------------==
! ... rotate coordinates around y axis
!     ==--------------------------------------------------------------==
      implicit none
      integer::iatm,natm
      real(kind=8)::cps(3,natm)
      real(kind=8)::ang
      real(kind=8)::cps_tmp(3)
      real(kind=8)::cosa,sina
      cosa=cos(ang)
      sina=sin(ang)
      do iatm=1,natm
        cps_tmp(:)=cps(:,iatm)
        cps(3,iatm)=cosa*cps_tmp(3)-sina*cps_tmp(1)
        cps(1,iatm)=sina*cps_tmp(3)+cosa*cps_tmp(1)
      enddo
      return
      end
