c     ==================================================================
      program main
c     ==--------------------------------------------------------------==
c     == This is a utility program to generate an fcc (111)           ==
c     == sqrt(3) x sqrt(3) R30 surface                                ==
c     ==--------------------------------------------------------------==
      implicit none
      integer :: iatom, natom
      integer, parameter :: matom = 400
      integer :: ii,ilayer,nlayer,nvaclayer,i1,i2,n1,n2
      double precision :: a_0, a, a_sf, bohr,rlaydist,rvac,z,cslb(3)
      double precision :: a1(3),a2(3),a3(3),a1_sc(3),a2_sc(3),a3_sc(3)
      double precision :: tau(3,matom),taus(3,matom),tau_tmp(3)
      double precision :: tau0_tmp(3,3)
      character(len=2) :: atmnm
      character(len=3) :: num
      character(len=20) :: filen
      integer :: input,iout,stdin,stdout,stderr,ixyz,ios,len
      character(len=80) :: infile,outfile
      logical :: fexist
      integer :: idum
      integer :: zatom
c     ==--------------------------------------------------------------==
c     == set constants                                                ==
c     ==--------------------------------------------------------------==
      stdin =05
      stdout=06
      stderr=00
      input =10
      iout  =20
      bohr=0.529177
      num='   '
c     ==--------------------------------------------------------------==
      write(stdout,'(a,$)')'Enter input file> '
      read(stdin,'(a)')infile
      write(stdout,'(a,$)')'Enter output file> '
      read(stdin,'(a)')outfile
      len=index(infile,' ')
      write(stdout,'(/,/,a,a)')'Input file : ',infile(:len-1)
      len=index(outfile,' ')
      write(stdout,'(a,a)')    'Output file: ',outfile(:len-1) 
      inquire(file=infile,exist=fexist)
      if(.not.fexist)then
        len=index(infile,' ')
        write(stderr,'(/,a,a)') infile(:len-1),' does not exist.'
        stop
      endif
      open(input,file=infile,iostat=ios,status='old')
      if(ios.ne.0)then
        write(stderr,'(a)')
     &  '*** ERROR occurs while opening the input file.'
        stop
      endif
      open(iout,file=outfile,status='unknown')
c     ==--------------------------------------------------------------==
      write(iout,'(/,a,a,a,/,a,a,a,/,a,a,a,/a,a,a,/)')
     &' ***',
     &'***********************************************************',
     &'****',
     &' ***',
     &' This program generates an fcc (111) sqrt(3) x sqrt(3) R30 ',
     &' ***',
     &' ***',
     &' clean surface                                             ',
     &' ***',
     &' ***',
     &'***********************************************************',
     &'****'
c     ==--------------------------------------------------------------==
      read(input,'(a)') atmnm
      read(input,*) a_0
      read(input,*) nlayer
      read(input,*) nvaclayer
      read(input,*) n1, n2
      a_0=dble(a_0)
c     ==--------------------------------------------------------------==
c     == set lattice vectors                                          ==
c     ==--------------------------------------------------------------==
      do ii=1,3
        a1(ii)=0.0d0
        a2(ii)=0.0d0
        a3(ii)=0.0d0
      enddo
      a=a_0/sqrt(2.0d0) * sqrt(3.0d0)
      a_sf=a_0/sqrt(2.0d0)
      rlaydist=a_0/sqrt(3.0d0)
      rvac=rlaydist*dble(nvaclayer)
      a1(1)=0.50d0*sqrt(3.0d0)*a
      a1(2)=0.50d0*a
      a2(2)=a
      a3(3)=rlaydist*dble(nlayer)+rlaydist*dble(nvaclayer)
      a1_sc(1)=dble(n1)*a1(1)
      a1_sc(2)=dble(n1)*a1(2)
      a1_sc(3)=0.0d0
      a2_sc(1)=0.d0
      a2_sc(2)=dble(n2)*a2(2)
      a2_sc(3)=0.0d0
      a1_sc(3)=0.0d0
      a2_sc(3)=0.0d0
      do ii=1,3
        a3_sc(ii)=a3(ii)
      enddo
c     center of slab (cslb)
      cslb(1)=0.0d0
      cslb(2)=0.0d0
      cslb(3)=-rlaydist*dble(nlayer-1)/2.0d0 
c     ==--------------------------------------------------------------==
c     == set atomic positions                                         ==
c     ==--------------------------------------------------------------==
      iatom=0
      natom=nlayer*3*n1*n2
      if(natom.gt.matom)then
        write(stderr,'(/,a)')
     &  ' *** ERROR: natom > matom'
        write(*,'(a,i5,a,i5)')' natom= ',natom,' matom= ',matom
        stop
      endif
      do ilayer=1,nlayer
        z=(-1.0d0)*dble(ilayer-1)*rlaydist
        tau0_tmp(1,1)=0.0d0
        tau0_tmp(2,1)=0.0d0
        tau0_tmp(3,1)=z
        tau0_tmp(1,2)=0.50d0*a_sf
        tau0_tmp(2,2)=0.50d0*sqrt(3.0d0)*a_sf
        tau0_tmp(3,2)=z
        tau0_tmp(1,3)=a_sf
        tau0_tmp(2,3)=sqrt(3.0d0)*a_sf
        tau0_tmp(3,3)=z
        if(mod(ilayer,3).eq.1)then
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,1)
          tau(2,iatom)=tau0_tmp(2,1)
          tau(3,iatom)=tau0_tmp(3,1)
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,2)
          tau(2,iatom)=tau0_tmp(2,2)
          tau(3,iatom)=tau0_tmp(3,2)
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,3)
          tau(2,iatom)=tau0_tmp(2,3)
          tau(3,iatom)=tau0_tmp(3,3)
        elseif(mod(ilayer,3).eq.2)then
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,1)
          tau(2,iatom)=tau0_tmp(2,1)+a_sf/sqrt(3.0d0)
          tau(3,iatom)=tau0_tmp(3,1)
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,2)
          tau(2,iatom)=tau0_tmp(2,2)+a_sf/sqrt(3.0d0)
          tau(3,iatom)=tau0_tmp(3,2)
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,3)
          tau(2,iatom)=tau0_tmp(2,3)+a_sf/sqrt(3.0d0)
          tau(3,iatom)=tau0_tmp(3,3)
        elseif(mod(ilayer,3).eq.0)then
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,1)
          tau(2,iatom)=tau0_tmp(2,1)-a_sf/sqrt(3.0d0)
          tau(3,iatom)=tau0_tmp(3,1)
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,2)
          tau(2,iatom)=tau0_tmp(2,2)-a_sf/sqrt(3.0d0)
          tau(3,iatom)=tau0_tmp(3,2)
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,3)
          tau(2,iatom)=tau0_tmp(2,3)-a_sf/sqrt(3.0d0)
          tau(3,iatom)=tau0_tmp(3,3)
        endif
      enddo
c     ==--------------------------------------------------------------==
      write(iout,*)
      write(iout,'(a,f18.8)')' a_0= ',a_0
      write(iout,'(a,f18.8)')' Inter layer distance: ',rlaydist
      write(iout,'(/,a,i2,a)')' Slab of ',nlayer,' ML'
      write(iout,'(a,i2,a)')
     &' Vacuum region corresponds to ',nvaclayer,' ML'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac,' Bohr'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac*Bohr
     &,' Angstrom'
      write(iout,'(a,f6.2,a)')
     &' Size of the supercell along the z axis: '
     &,abs(a3(3)),' Bohr'
      write(iout,'(a,f6.2,a)')
     &' Size of the supercell along the z axis: '
     &,abs(a3(3))*Bohr,' Angstrom'
      write(iout,'(/,a)')      ' Primitive lattice vectors:'
      write(iout,'(3f20.12)')(a1(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3(ii),ii=1,3)
      write(iout,'(/,a)')      ' Supercell lattice vectors:'
      write(iout,'(3f20.12)')(a1_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3_sc(ii),ii=1,3)
      write(iout,'(a,i5)')   ' Number of atoms:',natom
      write(iout,'(a)')      ' Atoms: '
      do iatom=1,natom
        write(iout,'(3f20.12,3i5)')(tau(ii,iatom),ii=1,3),1,1,1
      enddo 
      write(iout,'(/,a)')
     &' Atoms (center of the slab is set to the origin):'
      do iatom=1,natom
        write(iout,'(3f20.12,3i5)')(tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
      enddo 
c     ==--------------------------------------------------------------==
      close(input)
      close(iout)
c     ==--------------------------------------------------------------==
c     == generate XYZ file                                            ==
c     ==--------------------------------------------------------------==
      if(natom.lt.10)then
        write(num,'(i1)')natom
        filen=atmnm//num(1:1)//'.xyz'
      elseif(natom.lt.100)then
        write(num,'(i2)')natom
        filen=atmnm//num(1:2)//'.xyz'
      elseif(natom.lt.1000)then
        write(num,'(i3)')natom
        filen=atmnm//num(1:3)//'.xyz'
      endif
      write(*,'(/,a,a)')'Generating ',filen
      open(iout,file=filen,status='unknown')
      write(iout,'(i3)') natom
      write(iout,'(a)') atmnm//num
      do iatom=1,natom
        write(iout,'(a,3f18.8)')atmnm
c    &  ,((tau(ii,iatom)-cslb(ii))*bohr,ii=1,3)
     &  ,(tau(ii,iatom)*bohr,ii=1,3)
      enddo
      close(iout)
c     ==--------------------------------------------------------------==
c     == generate XSF file                                            ==
c     ==--------------------------------------------------------------==
      if(natom.lt.10)then
        write(num,'(i1)')natom
        filen=atmnm//num(1:1)//'.xsf'
      elseif(natom.lt.100)then
        write(num,'(i2)')natom
        filen=atmnm//num(1:2)//'.xsf'
      elseif(natom.lt.1000)then
        write(num,'(i3)')natom
        filen=atmnm//num(1:3)//'.xsf'
      endif
      write(*,'(/,a,a)')'Generating ',filen
      open(ixyz,file=filen,status='unknown')
      write(ixyz,'(a)')' SLAB'
      write(ixyz,'(a)')' PRIMVEC'
      write(ixyz,'(3f15.6)')(a1_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(3f15.6)')(a2_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(3f15.6)')(a3_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(a)')' PRIMCOORD'
      idum=1
      write(ixyz,'(i5,i1)')natom,idum
      do iatom=1,natom
        idum=zatom(trim(atmnm))
        write(ixyz,'(i5,3(f15.9,2x))')idum
     &  ,(tau(ii,iatom)*bohr,ii=1,3)
      enddo
      close(ixyz)
c     ==--------------------------------------------------------------==
      stop
      end
c     ==================================================================
