!     ==================================================================
      program main
!     ==--------------------------------------------------------------==
!     == This is a utility program to generate a (100) surface of the ==
!     == crystal with the diamond structure                           ==
!     ==--------------------------------------------------------------==
      implicit none
      integer :: iatom, natom, nhatom
      integer, parameter :: matom = 400
      integer :: ii, idum, ilayer, nlayer, nvaclayer, i1, i2, n1, n2
      real(kind=8) :: a0, a, bohr, rlaydist, rvac, z, dz, cslb(3)
      real(kind=8) :: dsih, xsih, ysih, zsih
      real(kind=8) :: a1(3), a2(3), a3(3), &
     &                a1_sc(3), a2_sc(3), a3_sc(3)
      real(kind=8) :: tau(3,matom), taus(3,matom), &
     &                tau0_tmp(3), tau_tmp(3)
      character(len=2) :: atmnm
      character(len=3) :: num, numh
      character(len=80) :: filen
      integer :: input, iout, ixyz, iatps, stdin, stdout, stderr
      integer :: ios, len
      character(len=80) ::  infile,outfile
      logical :: fexist
      integer :: zatom
! ... arguments
      integer :: iarg, iargc, narg, marg
      character(len=80) :: arg
! ... options
      integer :: ipw=0, istate=0
      integer :: ihter=0
!     ==--------------------------------------------------------------==
!     == set constants                                                ==
!     ==--------------------------------------------------------------==
      stdin  = 05
      stdout = 06
      stderr = 00
      input  = 10
      iout   = 20
      ixyz   = 20
      bohr = 0.529177
!     Distance between Si and H
      dsih = 1.5 / bohr
      xsih = dsih / sqrt(2.d0)
      ysih = xsih
      zsih = xsih
      num = '   '
!     ==--------------------------------------------------------------==
! ... arguments
      narg=iargc()
      iarg = 0
      if(narg > 0)then
        do while(iarg < narg)
          iarg = iarg + 1
          call getarg(iarg,arg)
          select case(trim(arg))
            case('-pw') 
              ipw=1
            case('-state')
              istate=1
            case('-h-term','-hterm')
              ihter=1
          end select 
        enddo
      endif
!     ==--------------------------------------------------------------==
! ... read input/output file names
      write(stdout,'(a,$)')'Enter input file> '
      read(stdin,'(a)')infile
      write(stdout,'(a,$)')'Enter output file> '
      read(stdin,'(a)')outfile
      inquire(file=infile,exist=fexist)
      if(.not.fexist)then
        len=index(infile,' ')
        write(stderr,'(/,a,a)') infile(:len-1),' does not exist.'
        stop
      endif
      len=index(infile,' ')
      write(stdout,'(/,/,a,a)')'Input file : ',infile(:len-1)
      len=index(outfile,' ')
      write(stdout,'(a,a)')  'Output file: ',outfile(:len-1)  
      open(input,file=infile,iostat=ios,status='old')
      if(ios.ne.0)then
        write(stderr,'(a)')&
     &  '*** ERROR occurs while opening the input file.'
        stop
      endif
      open(iout,file=outfile,status='unknown')
!     ==--------------------------------------------------------------==
      write(iout,'(/,a,a,a,/,a,a,a/,a,a,a,/)')&
     &' ***',&
     &'***********************************************************',&
     &'****',&
     &' ***',&
     &' This program generates a diamond (100) clean surface      ',&
     &' ***',&
     &' ***',&
     &'***********************************************************',&
     &'****'
!     ==--------------------------------------------------------------==
      read(input,'(a)') atmnm
      read(input,*) a0
      read(input,*) nlayer
      read(input,*) nvaclayer
      read(input,*) n1, n2
      a0=dble(a0)
!     ==--------------------------------------------------------------==
!     == set lattice vectors                                          ==
!     ==--------------------------------------------------------------==
! ... inter-layer distance
      rlaydist = 0.5d0 * a0
! ... vacuum thickness
      rvac     = rlaydist * dble(nvaclayer)
      a1(:) = 0.d0; a2(:) = 0.d0; a3(:) = 0.d0
      a1_sc(:) = 0.d0; a2_sc(:) = 0.d0; a3_sc(:) = 0.d0
! ... lattice vectors of the primitive cell
      a1(1) = a0 / sqrt(2.0d0)
      a2(2) = a0 / sqrt(2.0d0)
      a3(3) = rlaydist * dble(nlayer) + rlaydist * dble(nvaclayer)
! ... lattice vectors of the supercell
      a1_sc(1) = dble(n1) * a1(1)
      a1_sc(2) = 0.0d0
      a1_sc(3) = 0.0d0
      a2_sc(1) = 0.0d0
      a2_sc(2) = dble(n2) * a2(2)
      a2_sc(3) = 0.0d0
      do ii = 1, 3
        a3_sc(ii)=a3(ii)
      enddo
!     center of slab (cslb)
      cslb(1) = 0.0d0
      cslb(2) = 0.0d0
      cslb(3) = - rlaydist * dble(nlayer - 1) / 2.0d0 -rlaydist * 0.25d0
!     ==--------------------------------------------------------------==
!     == set atomic positions                                         ==
!     ==--------------------------------------------------------------==
      iatom = 0
      nhatom = 0
      if(ihter == 0)then
        natom = nlayer * n1 * n2 * 2
      else
        natom = nlayer * n1 * n2 * 2 + n1 * n2 * 2
        nhatom = n1 * n2 * 2
      endif
      if(natom > matom)then
        write(stderr,'(/,a)')&
     &  ' *** ERROR: natom > matom'
        write(*,'(a,i5,a,i5)')' natom= ',natom,' matom= ',matom
        stop
      endif
      do ilayer = 1, nlayer
        z = (-1.0d0) * dble(ilayer-1) * rlaydist
!       ... rlaydist is the distance between the layer for the bravis lattice (fcc)
!       ... dz is the distance between the layers for sublattice 1 and 2
        dz = rlaydist * 0.5d0 
!       ... sublattice (2) ...
        if(mod(ilayer,2) == 1)then
          tau0_tmp(1) = 0.0d0
          tau0_tmp(2) = 0.5d0 * a2(2)
          tau0_tmp(3) = z
          do i2 = 1, n2
            do i1 = 1, n1
              iatom = iatom + 1
              tau(1,iatom) = tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
              tau(2,iatom) = tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
              tau(3,iatom) = tau0_tmp(3)
            enddo
          enddo
        elseif(mod(ilayer,2) == 0)then
          tau0_tmp(1) = -0.50d0 * a1(1)
          tau0_tmp(2) = 0.d0          ! -0.50d0 * a2(2) + 0.5d0 * a2(2)
          tau0_tmp(3) = z
          do i2=1,n2
            do i1=1,n1
              iatom=iatom+1
              tau(1,iatom)=tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
              tau(2,iatom)=tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
              tau(3,iatom)=tau0_tmp(3)
            enddo
          enddo
        endif
!       ... sublattice (1) ...
        if(mod(ilayer,2) == 1)then
          tau0_tmp(1) = 0.0d0
          tau0_tmp(2) = 0.0d0
          tau0_tmp(3) = z - dz
          do i2 = 1, n2
            do i1 = 1, n1
              iatom = iatom + 1
              tau(1,iatom) = tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
              tau(2,iatom) = tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
              tau(3,iatom) = tau0_tmp(3)
            enddo
          enddo
        elseif(mod(ilayer,2) == 0)then
          tau0_tmp(1) = -0.50d0*a1(1)
          tau0_tmp(2) = -0.50d0*a2(2)
          tau0_tmp(3) = z - dz
          do i2=1,n2
            do i1=1,n1
              iatom=iatom+1
              tau(1,iatom)=tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
              tau(2,iatom)=tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
              tau(3,iatom)=tau0_tmp(3)
            enddo
          enddo
        endif
      enddo
      if(ihter == 1)then
        if(mod(nlayer,2) == 1)then
          tau0_tmp(1) = 0.0d0 + xsih
          tau0_tmp(2) = 0.0d0
          tau0_tmp(3) = z - dz - zsih
          do i2 = 1, n2
            do i1 = 1, n1
              iatom = iatom + 1
              tau(1,iatom) = tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
              tau(2,iatom) = tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
              tau(3,iatom) = tau0_tmp(3)
            enddo
          enddo
          tau0_tmp(1) = 0.0d0 - xsih
          tau0_tmp(2) = 0.0d0
          tau0_tmp(3) = z - dz - zsih
          do i2 = 1, n2
            do i1 = 1, n1
              iatom = iatom + 1
              tau(1,iatom) = tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
              tau(2,iatom) = tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
              tau(3,iatom) = tau0_tmp(3)
            enddo
          enddo
        elseif(mod(nlayer,2) == 0)then
          tau0_tmp(1) = -0.50d0*a1(1) + xsih
          tau0_tmp(2) = -0.50d0*a2(2)
          tau0_tmp(3) = z - dz - zsih
          do i2=1,n2
            do i1=1,n1
              iatom=iatom+1
              tau(1,iatom)=tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
              tau(2,iatom)=tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
              tau(3,iatom)=tau0_tmp(3)
            enddo
          enddo
          tau0_tmp(1) = -0.50d0*a1(1) - xsih
          tau0_tmp(2) = -0.50d0*a2(2)
          tau0_tmp(3) = z - dz - zsih
          do i2=1,n2
            do i1=1,n1
              iatom=iatom+1
              tau(1,iatom)=tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
              tau(2,iatom)=tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
              tau(3,iatom)=tau0_tmp(3)
            enddo
          enddo
        endif
      endif
!     ==--------------------------------------------------------------==
      write(iout,*)
      write(iout,'(a,f18.8)')' a_0 = ',a0
      write(iout,'(a,f18.8)')&
     &' Inter layer (bravis lattice) distance : ',rlaydist
      write(iout,'(a,f18.8)')&
     &' Inter layer distance                  : ',rlaydist * 0.5d0
      write(iout,'(/,a,i2,a)')' Slab of ',nlayer * 2,' ML'
      write(iout,'(a,i2,a)')&
     &' Vacuum region corresponds to ',nvaclayer * 2,' ML'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac,' Bohr'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac * Bohr,&
     &' Angstrom'
      write(iout,'(/,a)')      ' Primitive lattice vectors:'
      write(iout,'(3f20.12)')(a1(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3(ii),ii=1,3)
      write(iout,'(/,a)')      ' Supercell lattice vectors:'
      write(iout,'(3f20.12)')(a1_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3_sc(ii),ii=1,3)
      write(iout,'(a,i5)')   ' Number of atoms:',natom
      if(ihter == 1)then
        write(iout,'(a,i5)')   ' Number of H atoms:',nhatom
      endif
      write(iout,'(a)')      ' Atoms: '
      if(ihter == 0)then
        do iatom = 1, natom
          write(iout,'(3f20.12,3i5)')(tau(ii,iatom),ii=1,3),1,1,1
        enddo 
        write(iout,'(/,a)')&
     &  ' Atoms (center of the slab is set to the origin):'
        do iatom = 1 , natom
          write(iout,'(3f20.12,3i5)')&
     &    (tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
        enddo
      else
        do iatom = 1, natom - nhatom
          write(iout,'(3f20.12,3i5)')(tau(ii,iatom),ii=1,3),1,1,1
        enddo 
        do iatom = natom - nhatom + 1, natom
          write(iout,'(3f20.12,3i5)')(tau(ii,iatom),ii=1,3),1,1,2
        enddo 
        write(iout,'(/,a)')&
     &  ' Atoms (center of the slab is set to the origin):'
        do iatom = 1, natom - nhatom
          write(iout,'(3f20.12,3i5)')&
     &    (tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
        enddo
        do iatom = natom - nhatom + 1, natom
          write(iout,'(3f20.12,3i5)')&
     &    (tau(ii,iatom)-cslb(ii),ii=1,3),1,1,2
        enddo
      endif
!     ==--------------------------------------------------------------==
      close(input)
      close(iout)
!     ==--------------------------------------------------------------==
      if(ipw == 1)then
! ... PW output
        filen = 'atps'
        open(iatps,file=filen,status='unknown')
        write(iatps,'(a)')       ' ibrav = 4'
        write(iatps,'(a,i3)')    ' nat   =',natom
        if(ihter == 0)then
          write(iatps,'(a,i1)')    ' ntyp  =',1
        else
          write(iatps,'(a,i1)')    ' ntyp  =',2
        endif
        write(iatps,'(a,f20.12)')' celldm(1) = ',a1_sc(1)
        write(iatps,'(a,f20.12)')' celldm(3) = ',a3_sc(3)/a1_sc(1)
        write(iatps,'(a)')'CELL_PARAMETERS (bohr)'
        write(iatps,'(3f20.12)')(a1_sc(ii),ii=1,3)
        write(iatps,'(3f20.12)')(a2_sc(ii),ii=1,3)
        write(iatps,'(3f20.12)')(a3_sc(ii),ii=1,3)
        write(iatps,'(a)')'ATOMIC_POSITIONS (bohr)'
        if(ihter == 0)then
          do iatom = 1, natom
            write(iatps,'(a,3f20.12,3i5)')&
     &      trim(atmnm),(tau(ii,iatom),ii=1,3),1,1,1
          enddo 
        else
          do iatom = 1, natom - nhatom
            write(iatps,'(a,3f20.12,3i5)')&
     &      trim(atmnm),(tau(ii,iatom),ii=1,3),1,1,1
          enddo 
          do iatom = natom - nhatom + 1, natom
            write(iatps,'(a,3f20.12,3i5)')&
     &      'H ',(tau(ii,iatom),ii=1,3),1,1,1
          enddo 
        endif
        write(iatps,'(a)')''
        write(iatps,'(a)')'# Atomic positions centered at z=0'
        write(iatps,'(a)')'ATOMIC_POSITIONS (bohr)'
        if(ihter == 0)then
          do iatom = 1, natom
            write(iatps,'(a,3f20.12,3i5)')&
     &      trim(atmnm),(tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
          enddo 
        else
          do iatom = 1, natom - nhatom
            write(iatps,'(a,3f20.12,3i5)')&
     &      trim(atmnm),(tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
          enddo 
          do iatom = natom - nhatom + 1, natom
            write(iatps,'(a,3f20.12,3i5)')&
     &      'H ',(tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
          enddo 
        endif
        close(iatps) 
! ... PW output in Angstrom
        filen = 'atps_angstrom'
        open(iatps,file=filen,status='unknown')
        write(iatps,'(a)')       ' ibrav = 4'
        write(iatps,'(a,i3)')    ' nat   =',natom
        if(ihter == 0)then
          write(iatps,'(a,i1)')    ' ntyp  =',1
        else
          write(iatps,'(a,i1)')    ' ntyp  =',2
        endif
        write(iatps,'(a,f20.12)')' A = ',a1_sc(1)*bohr
        write(iatps,'(a,f20.12)')' C = ',a3_sc(3)*bohr
        write(iatps,'(a)')'CELL_PARAMETERS (angstrom)'
        write(iatps,'(3f20.12)')(a1_sc(ii)*bohr,ii=1,3)
        write(iatps,'(3f20.12)')(a2_sc(ii)*bohr,ii=1,3)
        write(iatps,'(3f20.12)')(a3_sc(ii)*bohr,ii=1,3)
        write(iatps,'(a)')'ATOMIC_POSITIONS (angstrom)'
        if(ihter == 0)then
          do iatom = 1, natom
            write(iatps,'(a,3f20.12,3i5)')&
     &      trim(atmnm),(tau(ii,iatom)*bohr,ii=1,3),1,1,1
          enddo 
        else
          do iatom = 1, natom - nhatom
            write(iatps,'(a,3f20.12,3i5)')&
     &      trim(atmnm),(tau(ii,iatom)*bohr,ii=1,3),1,1,1
          enddo 
          do iatom = natom - nhatom + 1, natom
            write(iatps,'(a,3f20.12,3i5)')&
     &      'H ',(tau(ii,iatom)*bohr,ii=1,3),1,1,1
          enddo 
        endif
        write(iatps,'(a)')''
        write(iatps,'(a)')'# Atomic positions centered at z=0'
        write(iatps,'(a)')'ATOMIC_POSITIONS (angstrom)'
        if(ihter == 0)then
          do iatom = 1, natom
            write(iatps,'(a,3f20.12,3i5)')&
     &      trim(atmnm),((tau(ii,iatom)-cslb(ii))*bohr,ii=1,3),1,1,1
          enddo 
        else
          do iatom = 1, natom - nhatom
            write(iatps,'(a,3f20.12,3i5)')&
     &      trim(atmnm),((tau(ii,iatom)-cslb(ii))*bohr,ii=1,3),1,1,1
          enddo 
          do iatom = natom - nhatom + 1, natom
            write(iatps,'(a,3f20.12,3i5)')&
     &      'H ',((tau(ii,iatom)-cslb(ii))*bohr,ii=1,3),1,1,1
          enddo 
        endif
        close(iatps) 
      endif
!     ==--------------------------------------------------------------==
!     == generate XYZ file                                            ==
!     ==--------------------------------------------------------------==
      if(ihter == 0)then
        if(natom < 10)then
          write(num,'(i1)')natom
          filen=atmnm//num(1:1)//'.xyz'
        elseif(natom < 100)then
          write(num,'(i2)')natom
          filen=atmnm//num(1:2)//'.xyz'
        elseif(natom < 1000)then
          write(num,'(i3)')natom
          filen=atmnm//num(1:3)//'.xyz'
        endif
      else
        write(num,'(i0)')natom-nhatom
        write(numh,'(i0)')nhatom
        filen=atmnm//trim(num)//'H'//trim(numh)//'.xyz'
      endif
      write(*,'(/,a,a)')'Generating ',filen
      open(iout,file=filen,status='unknown')
      write(iout,'(i3)') natom
      write(iout,'(a)') atmnm//trim(num)//'H'//trim(numh)
      if(ihter == 0)then
        do iatom = 1, natom
          write(iout,'(a,3f18.8)')atmnm &
     &    ,((tau(ii,iatom)-cslb(ii))*bohr,ii=1,3)
        enddo
      else
        do iatom = 1, natom - nhatom
          write(iout,'(a,3f18.8)')atmnm &
     &    ,((tau(ii,iatom)-cslb(ii))*bohr,ii=1,3)
        enddo
        do iatom = natom - nhatom + 1, natom
          write(iout,'(a,3f18.8)')'H ' &
     &    ,((tau(ii,iatom)-cslb(ii))*bohr,ii=1,3)
        enddo
      endif
      close(iout)
!     ==--------------------------------------------------------------==
!     == generate XSF file                                            ==
!     ==--------------------------------------------------------------==
      if(ihter == 0)then
        write(num,'(i0)')natom
        filen=atmnm//trim(num)//'.xsf'
      else
        write(num,'(i0)')natom-nhatom
        write(numh,'(i0)')nhatom
        filen=atmnm//trim(num)//'H'//trim(numh)//'.xsf'
      endif
      write(*,'(/,a,a)')'Generating ',filen
      open(ixyz,file=filen,status='unknown')
      write(ixyz,'(a)')' SLAB'
      write(ixyz,'(a)')' PRIMVEC'
      write(ixyz,'(3f15.6)')(a1_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(3f15.6)')(a2_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(3f15.6)')(a3_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(a)')' PRIMCOORD'
      idum=1
      write(ixyz,'(i5,i1)')natom,idum
      if(ihter == 0)then
        do iatom = 1, natom
          idum = zatom( trim(atmnm) )
          write(ixyz,'(i5,3(f15.9,2x))')idum&
     &    ,(tau(ii,iatom)*bohr,ii=1,3)
        enddo
      else
        do iatom = 1, natom - nhatom
          idum = zatom( trim(atmnm) )
          write(ixyz,'(i5,3(f15.9,2x))')idum&
     &    ,(tau(ii,iatom)*bohr,ii=1,3)
        enddo
        do iatom = natom - nhatom + 1, natom
          write(ixyz,'(i5,3(f15.9,2x))')1 &
     &    ,(tau(ii,iatom)*bohr,ii=1,3)
        enddo
      endif
      close(ixyz)
!     ==--------------------------------------------------------------==
      stop
      end
!     ==================================================================
