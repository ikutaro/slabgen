!     ==================================================================
      program main
!     ==--------------------------------------------------------------==
!     == This is a utility program to generate a graphene structure   ==
!     ==--------------------------------------------------------------==
      implicit none
      integer :: iatom, natom, natom_c, natom_h
      integer :: zatom
      integer :: ilayer, nlayer, nvaclayer
      integer :: i1, i2, n1, n2, ix, iy, nx, ny
      integer :: ndim
      integer :: ndim_sc(2)
      integer :: ivac
      real(kind=8) :: a_0, c_0, a, a_sf, bohr,rlaydist,rvac,z
      real(kind=8) :: d_ch
      real(kind=8) :: cslb(3)
      real(kind=8) :: a1(3),a2(3),a3(3),a1_sc(3),a2_sc(3),a3_sc(3)
      real(kind=8), allocatable :: tau(:,:),taus(:,:),tau_tmp(:,:)
      real(kind=8) :: sum
      real(kind=8) :: xmat(3,3),xmati(3,3)
      real(kind=8) :: vac
      character(len=2) :: atmnm
      character(len=3) :: num, num_c, num_h
      character(len=80) :: filen
      character(len=256) :: str
! ... fortran i/o
      integer :: input,iout,stdin,stdout,stderr, ios, ios2, len
      integer :: ixyz,iatps
      character(len=80) :: infile, outfile
      logical :: fexist
! ... arguments
      integer :: iarg, iargc, narg, marg
      character(len=80) :: arg
! ... options
      integer :: ipw = 0, istate = 0
      integer :: ihter = 0
! ... others
      integer :: idum
! ... counters
      integer :: ii, jj, kk
!     ==--------------------------------------------------------------==
!     == set constants                                                ==
!     ==--------------------------------------------------------------==
      stdin  = 05
      stdout = 06
      stderr = 00
      input  = 10
      iout   = 20
      ixyz   = 20
      bohr = 1.D0/1.8897265D0
      ndim = 2
      num = '   '
      d_ch = 1.0855d0 / bohr
!     ==--------------------------------------------------------------==
! ... arguments
      narg = iargc()
      IF(narg > 0)THEN
        iarg = 0
        DO WHILE(iarg < narg)
          iarg = iarg + 1
          CALL getarg(iarg,arg)
          SELECT CASE(trim(arg))
            CASE('-pw','--pw','-qe','--qe') 
              ipw=1
            CASE('-pw-crystal') 
              ipw=2
            CASE('-state','--state')
              istate=1
            CASE('-h-term','-hterm')
                ihter=1
          END SELECT
        END DO
      END IF
!     ==--------------------------------------------------------------==
      WRITE(stdout,'(a,$)')'Enter input file> '
      READ(stdin,'(a)')infile
      WRITE(stdout,'(a,$)')'Enter output file> '
      READ(stdin,'(a)')outfile
      len=index(infile,' ')
      WRITE(stdout,'(/,/,a,a)')'Input file : ',infile(:len-1)
      len=index(outfile,' ')
      WRITE(stdout,'(a,a)')    'Output file: ',outfile(:len-1) 
      INQUIRE(file=infile,exist=fexist)
      IF(.not.fexist)THEN
        len=index(infile,' ')
        WRITE(stderr,'(/,a,a)') infile(:len-1),' does not exist.'
        STOP
      END IF
      OPEN(input,file=infile,iostat=ios,status='old')
      IF(ios /= 0)then
        WRITE(stderr,'(a)')&
     &  '*** ERROR occurs while opening the input file.'
        STOP
      END IF
      OPEN(iout,file=outfile,status='unknown')
!     ==--------------------------------------------------------------==
      WRITE(iout,'(/,a,a,a,/,a,a,a,/,a,a,a,/)')&
     &' ***',&
     &'***********************************************************',&
     &'****',&
     &' ***',&
     &' This program generates an armchair graphene nanoribbon    ',&
     &' ***',&
     &' ***',&
     &'***********************************************************',&
     &'****'
!     ==--------------------------------------------------------------==
      READ(input,'(a)') atmnm
      READ(input,*) a_0, c_0
      READ(input,'(a)') str
      READ(str,*,iostat=ios) n1, n2
      IF(ios /= 0)THEN
        READ(str,*,iostat=ios2) n1
        IF(ios2 == 0)THEN
          n2 = 1
        ELSE
          WRITE(stderr,'(a)')&
     &    ' *** ERROR occurs while reading the input file.'
          STOP
        END IF
      END IF 
!     READ(input,*) n1, n2
      a_0 = dble(a_0)
      c_0 = dble(c_0)
      nlayer = 1
      nvaclayer = 1
      nx = n1
      ny = n2
!     ==--------------------------------------------------------------==
!     == set lattice vectors                                          ==
!     ==--------------------------------------------------------------==
      DO ii = 1, 3
        a1(ii) = 0.d0
        a2(ii) = 0.d0
        a3(ii) = 0.d0
      END DO
      a = a_0
      a_sf = a_0 / SQRT(3.D0) ! C-C bond length
      rlaydist = c_0
      rvac     = c_0
      a1(1) = 3.D0 * a_sf
      a2(2) = ( 0.5D0 * a_0 ) * DBLE( ny - 1 ) + c_0
      a3(3) = c_0
      a1_sc(:) = a1(:) * DBLE( nx )
      a2_sc(:) = a2(:)
      a3_sc(:) = a3(:)
!     center of ribbon
      cslb(1) = 0.D0
      cslb(2) = -0.25D0 * a_0 * DBLE( ny -1 )
      cslb(3) = 0.D0
!     ==--------------------------------------------------------------==
      natom = 2 * nx * ny
      natom_c = 2 * nx * ny
      natom_h = 0
      IF(ihter == 1)THEN
        natom = natom + 4 * nx
        natom_h = natom - natom_c
      ENDIF
      ALLOCATE(tau_tmp(3,natom), tau(3,natom), taus(3,natom))
      iatom = 0
      DO ix = 1, nx
        DO iy = 1, ny
          tau_tmp(3,1) = 0.D0
          tau_tmp(3,2) = 0.D0
          IF( (iy/2)*2 /= iy )THEN
            tau_tmp(1,1) = 0.D0
            tau_tmp(2,1) = -( 0.5D0 * a_0 ) * DBLE( iy - 1 )
            tau_tmp(1,2) = a_sf
            tau_tmp(2,2) = -( 0.5D0 * a_0 ) * DBLE( iy - 1 )
          ELSE
            tau_tmp(1,1) = 1.5D0 * a_sf
            tau_tmp(2,1) = -( 0.5D0 * a_0 ) * DBLE( iy - 1 )
            tau_tmp(1,2) = 2.5D0 * a_sf
            tau_tmp(2,2) = -( 0.5d0 * a_0 ) * DBLE( iy - 1 )
          END IF
          DO ii = 1, 2
            iatom = iatom + 1
            IF( iatom > natom )THEN
              WRITE(stderr,'(a)')' *** ERROR: iatom > natom'
              STOP
            END IF
            tau(1,iatom) = tau_tmp(1,ii) + DBLE( ix - 1 ) * a1(1) 
            tau(2,iatom) = tau_tmp(2,ii) + DBLE( ix - 1 ) * a1(2)
            tau(3,iatom) = tau_tmp(3,ii) + DBLE( ix - 1 ) * a1(3)
          END DO
        END DO
      END DO
      IF(ihter == 1)THEN
        DO ix = 1, nx
          tau_tmp(1,1) = 0.D0
          tau_tmp(2,1) = 0.D0
          tau_tmp(1,2) = a_sf
          tau_tmp(2,2) = 0.D0
          tau_tmp(1,1) = tau_tmp(1,1) - 0.5D0 * d_ch 
          tau_tmp(2,1) = tau_tmp(2,1) + 0.5D0 * SQRT(3.D0) * d_ch 
          tau_tmp(1,2) = tau_tmp(1,2) + 0.5D0 * d_ch 
          tau_tmp(2,2) = tau_tmp(2,2) + 0.5D0 * SQRT(3.D0) * d_ch 
          IF( (ny/2)*2 /= ny )THEN
            tau_tmp(1,3) = 0.D0
            tau_tmp(2,3) = -( 0.5D0 * a_0 ) * DBLE( ny - 1 )
            tau_tmp(1,4) = a_sf
            tau_tmp(2,4) = -( 0.5D0 * a_0 ) * DBLE( ny - 1 )
          ELSE
            tau_tmp(1,3) = 1.5D0 * a_sf
            tau_tmp(2,3) = -( 0.5D0 * a_0 ) * DBLE( ny - 1 )
            tau_tmp(1,4) = 2.5D0 * a_sf
            tau_tmp(2,4) = -( 0.5d0 * a_0 ) * DBLE( ny - 1 )
          END IF
          tau_tmp(1,3) = tau_tmp(1,3) - 0.5D0 * d_ch 
          tau_tmp(2,3) = tau_tmp(2,3) - 0.5D0 * SQRT(3.D0) * d_ch 
          tau_tmp(1,4) = tau_tmp(1,4) + 0.5D0 * d_ch 
          tau_tmp(2,4) = tau_tmp(2,4) - 0.5D0 * SQRT(3.D0) * d_ch 
          DO ii = 1, 4
            iatom = iatom + 1
            IF( iatom > natom )THEN
              WRITE(stderr,'(a)')' *** ERROR: iatom > natom'
              STOP
            END IF
            tau(1,iatom) = tau_tmp(1,ii) + DBLE( ix - 1 ) * a1(1) 
            tau(2,iatom) = tau_tmp(2,ii) + DBLE( ix - 1 ) * a1(2)
            tau(3,iatom) = tau_tmp(3,ii) + DBLE( ix - 1 ) * a1(3)
          END DO
        END DO
      ENDIF
!     ==--------------------------------------------------------------==
      DO ii = 1, 3
        xmat(ii,1)=a1_sc(ii)
        xmat(ii,2)=a2_sc(ii)
        xmat(ii,3)=a3_sc(ii)
      END DO
      CALL matinv(xmat,xmati,sum)
      DO iatom = 1, natom
        DO ii = 1, 3
          sum = 0.D0
          DO jj = 1, 3
            sum = sum + xmati(ii,jj) * tau(jj,iatom)
          ENDDO
          taus(ii,iatom) = sum
        END DO
      END DO
!     ==--------------------------------------------------------------==
      WRITE(iout,'(a,f18.8)')' Lattice constant of graphene (a_0): ',a_0
      WRITE(iout,'(a,f18.8)')' Vacuum thickness (c_0): ',c_0
      WRITE(iout,'(a,f18.8)')' Inter layer distance: ',rlaydist
      WRITE(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac,' Bohr'
      WRITE(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac*Bohr&
     &,' Angstrom'
      WRITE(iout,'(/,a)')      ' Primitive lattice vectors:'
      WRITE(iout,'(3f20.12)')(a1(ii),ii=1,3)
      WRITE(iout,'(3f20.12)')(a2(ii),ii=1,3)
      WRITE(iout,'(3f20.12)')(a3(ii),ii=1,3)
      WRITE(iout,'(/,a)')      ' Supercell lattice vectors:'
      WRITE(iout,'(3f20.12)')(a1_sc(ii),ii=1,3)
      WRITE(iout,'(3f20.12)')(a2_sc(ii),ii=1,3)
      WRITE(iout,'(3f20.12)')(a3_sc(ii),ii=1,3)
      WRITE(iout,'(a,i5)')' Number of units (in y-direction):',n2
      WRITE(iout,'(a,i5)')' Supercell dimension (in the x-direction):',n2
      WRITE(iout,'(a,i5)')' Number of atoms:',natom
      WRITE(iout,'(a)')   ' Atoms: '
      if(ihter == 0)then
        DO iatom = 1, natom
          WRITE(iout,'(3f20.12,3i5)')(tau(ii,iatom),ii=1,3), 1, 1, 1
        END DO
        WRITE(iout,'(/,a)')&
     &  ' Atoms (center of the slab is set to the origin):'
        DO iatom = 1, natom
          WRITE(iout,'(3f20.12,3i5)')(tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
        END DO
      ELSE
        DO iatom = 1, natom_c
          WRITE(iout,'(3f20.12,3i5)')(tau(ii,iatom),ii=1,3), 1, 1, 1
        END DO
        DO iatom = natom_c + 1, natom
          WRITE(iout,'(3f20.12,3i5)')(tau(ii,iatom),ii=1,3), 1, 1, 2
        END DO
        WRITE(iout,'(/,a)')&
     &  ' Atoms (center of the slab is set to the origin):'
        DO iatom = 1, natom_c
          WRITE(iout,'(3f20.12,3i5)')(tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
        END DO
        DO iatom = natom_c + 1, natom
          WRITE(iout,'(3f20.12,3i5)')(tau(ii,iatom)-cslb(ii),ii=1,3),1,1,2
        END DO
      END IF
      IF(ipw == 1)THEN
! ... PW output
        filen = 'atps'
        OPEN(iatps,file=filen,status='unknown')
        WRITE(iatps,'(a)')       ' ibrav = 8'
        WRITE(iatps,'(a,i3)')    ' nat   =',natom
        IF(ihter == 0)THEN
          WRITE(iatps,'(a,i1)')    ' ntyp  =',1
        ELSE IF(ihter == 1)THEN
          WRITE(iatps,'(a,i1)')    ' ntyp  =',2
        END IF
        WRITE(iatps,'(a,f20.12)')' celldm(1) = ',a1_sc(1)
        WRITE(iatps,'(a,f20.12)')' celldm(2) = ',a2_sc(2)/a1_sc(1)
        WRITE(iatps,'(a,f20.12)')' celldm(3) = ',a3_sc(3)/a1_sc(1)
        WRITE(iatps,'(a)')'CELL_PARAMETERS (bohr)'
        WRITE(iatps,'(3f20.12)')(a1_sc(ii),ii=1,3)
        WRITE(iatps,'(3f20.12)')(a2_sc(ii),ii=1,3)
        WRITE(iatps,'(3f20.12)')(a3_sc(ii),ii=1,3)
        WRITE(iatps,'(a)')'ATOMIC_POSITIONS (bohr)'
        IF(ihter == 0)THEN
          DO iatom = 1, natom
            WRITE(iatps,'(a,3f20.12,3i5)')&
     &      trim(atmnm),(tau(ii,iatom),ii=1,3),1,1,1
          END DO
        ELSE IF(ihter == 1)THEN
          DO iatom = 1, natom_c
            WRITE(iatps,'(a,3f20.12,3i5)')&
     &      trim(atmnm),(tau(ii,iatom),ii=1,3),1,1,1
          END DO
          DO iatom = natom_c + 1, natom
            WRITE(iatps,'(a,3f20.12,3i5)')&
     &      'H',(tau(ii,iatom),ii=1,3),1,1,1
          END DO
        ENDIF
        CLOSE(iatps) 
! ... PW output (Angstrom)
        filen='atps_angstrom'
        OPEN(iatps,file=filen,status='unknown')
        WRITE(iatps,'(a)')       ' ibrav = 8'
        WRITE(iatps,'(a,i3)')    ' nat   =',natom
        IF(ihter == 0)THEN
          WRITE(iatps,'(a,i1)')    ' ntyp  =',1
        ELSE
          WRITE(iatps,'(a,i1)')    ' ntyp  =',2
        END IF
        WRITE(iatps,'(a,f20.12)')' A = ',a1_sc(1)*bohr
        WRITE(iatps,'(a,f20.12)')' B = ',a2_sc(2)*bohr
        WRITE(iatps,'(a,f20.12)')' C = ',a3_sc(3)*bohr
        WRITE(iatps,'(a)')'CELL_PARAMETERS (angstrom)'
        WRITE(iatps,'(3f20.12)')(a1_sc(ii)*bohr,ii=1,3)
        WRITE(iatps,'(3f20.12)')(a2_sc(ii)*bohr,ii=1,3)
        WRITE(iatps,'(3f20.12)')(a3_sc(ii)*bohr,ii=1,3)
        WRITE(iatps,'(a)')'ATOMIC_POSITIONS (angstrom)'
        IF(ihter == 0)THEN
          DO iatom = 1, natom
            WRITE(iatps,'(a,3f20.12,3i5)')&
     &      trim(atmnm),(tau(ii,iatom)*bohr,ii=1,3),1,1,1
          END DO
        ELSE
          DO iatom = 1, natom_c
            WRITE(iatps,'(a,3f20.12,3i5)')&
     &      trim(atmnm),(tau(ii,iatom)*bohr,ii=1,3),1,1,1
          END DO
          DO iatom = natom_c + 1, natom
            WRITE(iatps,'(a,3f20.12,3i5)')&
     &      'H',(tau(ii,iatom)*bohr,ii=1,3),1,1,2
          END DO
        END IF
        CLOSE(iatps) 
      ELSE IF(ipw == 2)THEN
! ... PW output (Angstrom and crystal)
        filen='atps_angstrom_crystal'
        open(iatps,file=filen,status='unknown')
        WRITE(iatps,'(a)')       ' ibrav = 0'
        WRITE(iatps,'(a,i3)')    ' nat   =',natom
        IF(ihter == 0)THEN
          WRITE(iatps,'(a,i1)')    ' ntyp  =',1
        ELSE
          WRITE(iatps,'(a,i1)')    ' ntyp  =',2
        END IF
        WRITE(iatps,'(a)')'CELL_PARAMETERS angstrom'
        WRITE(iatps,'(3f20.12)')(a1_sc(ii)*bohr,ii=1,3)
        WRITE(iatps,'(3f20.12)')(a2_sc(ii)*bohr,ii=1,3)
        WRITE(iatps,'(3f20.12)')(a3_sc(ii)*bohr,ii=1,3)
        WRITE(iatps,'(a)')'ATOMIC_POSITIONS crystal'
        IF(ihter == 0)THEN
          DO iatom = 1, natom
            WRITE(iatps,'(a,3f20.12,3i5)')&
     &      trim(atmnm),(taus(ii,iatom),ii=1,3),1,1,1
          END DO
        ELSE
          DO iatom = 1, natom_c
            WRITE(iatps,'(a,3f20.12,3i5)')&
     &      trim(atmnm),(taus(ii,iatom),ii=1,3),1,1,1
          END DO
          DO iatom = natom_c + 1, natom
            WRITE(iatps,'(a,3f20.12,3i5)')&
     &      'H',(taus(ii,iatom),ii=1,3),1,1,1
          END DO
        END IF
        CLOSE(iatps) 
      END IF
!     ==--------------------------------------------------------------==
!     == generate XSF file                                            ==
!     ==--------------------------------------------------------------==
      IF(ihter == 0)THEN
        WRITE(num,'(i0)')natom
        filen = TRIM(atmnm)//TRIM(num)//'.xsf'
      ELSE
        WRITE(num_c,'(i0)')natom_c
        WRITE(num_h,'(i0)')natom_h
        filen = TRIM(atmnm)//TRIM(num_c)//'H'//TRIM(num_h)//'.xsf'
      END IF
      WRITE(*,'(/,a,a)')'Generating ',filen
      OPEN(ixyz,file=filen,status='unknown')
      WRITE(ixyz,'(a)')' SLAB'
      WRITE(ixyz,'(a)')' PRIMVEC'
      WRITE(ixyz,'(3f15.6)')(a1_sc(ii)*bohr,ii=1,3)
      WRITE(ixyz,'(3f15.6)')(a2_sc(ii)*bohr,ii=1,3)
      WRITE(ixyz,'(3f15.6)')(a3_sc(ii)*bohr,ii=1,3)
      WRITE(ixyz,'(a)')' PRIMCOORD'
      idum = 1
      WRITE(ixyz,'(i5,i1)')natom,idum
      IF(ihter == 0)THEN
        DO iatom = 1, natom
          idum = zatom(trim(atmnm))
          WRITE(ixyz,'(i5,3(f15.9,2x))')idum, &
     &    (tau(ii,iatom)*bohr,ii=1,3)
        END DO
      ELSE
        DO iatom = 1, natom_c
          idum = zatom(trim(atmnm))
          WRITE(ixyz,'(i5,3(f15.9,2x))')idum, &
     &    (tau(ii,iatom)*bohr,ii=1,3)
        END DO
        DO iatom = natom_c + 1, natom
          idum = 1
          WRITE(ixyz,'(i5,3(f15.9,2x))')idum, &
     &    (tau(ii,iatom)*bohr,ii=1,3)
        END DO
      END IF
      CLOSE(ixyz)
!     ==--------------------------------------------------------------==
      DEALLOCATE(tau,taus,tau_tmp)
!     ==--------------------------------------------------------------==
      END PROGRAM
!     ==================================================================

