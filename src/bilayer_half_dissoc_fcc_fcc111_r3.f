c     ==================================================================
      program main
c     ==--------------------------------------------------------------==
c     == This is a utility program to generate an fcc (111)           ==
c     == Water bylayer("half-disscociate")/fcc(111)                   ==
c     == (sqrt(3) x sqrt(3)) surface                                  ==
c     == See Michaelides et al, PRL 90 216102 (2003)                  ==
c     == and Vasilev et al, JCP 122 054701 (2005)                     ==
c     ==--------------------------------------------------------------==
      implicit none
      integer iatom,natom,matom
      parameter(matom=400)
      integer ii,ilayer,nlayer,nvaclayer,i1,i2,n1,n2
      double precision a_0,a,a_sf, bohr,pi,tpi,rlaydist,rvac,z,cslb(3)
     &                ,arg
      double precision a1(3),a2(3),a3(3),a1_sc(3),a2_sc(3),a3_sc(3)
      double precision tau(3,matom),taus(3,matom),tau_tmp(3)
      double precision tau0_tmp(3,3)
      integer imetal
      double precision d_om(2),d_oh(2),alpha(2),theta(2),d_hm
      double precision tau_h1(3,2),tau_h2(3,2),tau_o(3,2)
      character*2 atmnm
      character*3 num
      character*20 filen
      integer input,iout,stdin,stdout,stderr, ios,len
      character*80 infile,outfile
      logical fexist
c     ==--------------------------------------------------------------==
c     == set constants                                                ==
c     ==--------------------------------------------------------------==
      stdin =05
      stdout=06
      stderr=00
      input =10
      iout  =20
      pi=4.0d0*atan(1.0d0)
      tpi=8.0d0*atan(1.0d0)
      bohr=0.529177
      num='   '
c     ==--------------------------------------------------------------==
      write(stdout,'(a,$)')'Enter input file> '
      read(stdin,'(a)')infile
      write(stdout,'(a,$)')'Enter output file> '
      read(stdin,'(a)')outfile
      len=index(infile,' ')
      write(stdout,'(/,/,a,a)')'Input file : ',infile(:len-1)
      len=index(outfile,' ')
      write(stdout,'(a,a)')    'Output file: ',outfile(:len-1) 
      inquire(file=infile,exist=fexist)
      if(.not.fexist)then
        len=index(infile,' ')
        write(stderr,'(/,a,a)') infile(:len-1),' does not exist.'
        stop
      endif
      open(input,file=infile,iostat=ios,status='old')
      if(ios.ne.0)then
        write(stderr,'(a)')
     &  '*** ERROR occurs while opening the input file.'
        stop
      endif
      open(iout,file=outfile,status='unknown')
c     ==--------------------------------------------------------------==
      write(iout,'(/,a,a,a,/,a,a,a,/,a,a,a,/a,a,a,/)')
     &' ***',
     &'***********************************************************',
     &'****',
     &' ***',
     &' This program generates Water/fcc(111) (sqrt(3) x sqrt(3)) ',
     &' ***',
     &' ***',
     &' surface                                                   ',
     &' ***',
     &' ***',
     &'***********************************************************',
     &'****'
c     ==--------------------------------------------------------------==
c     Slab
      read(input,'(a)') atmnm
      read(input,*) a_0
      read(input,*) nlayer
      read(input,*) nvaclayer
c     Water(1)
      read(input,*) d_oh(1),theta(1)
      read(input,*) d_om(1),alpha(1)
c     Water(2)/Metal
      read(input,*) d_oh(2),theta(2)
      read(input,*) d_om(2),alpha(2)
c     ==--------------------------------------------------------------==
c     == set lattice vectors                                          ==
c     ==--------------------------------------------------------------==
      do ii=1,3
        a1(ii)=0.0d0
        a2(ii)=0.0d0
        a3(ii)=0.0d0
      enddo
      a=a_0/sqrt(2.0d0) * sqrt(3.0d0)
      a_sf=a_0/sqrt(2.0d0)
      rlaydist=a_0/sqrt(3.0d0)
      rvac=rlaydist*dble(nvaclayer)
      a1(1)=0.50d0*sqrt(3.0d0)*a
      a1(2)=0.50d0*a
      a2(2)=a
      a3(3)=rlaydist*dble(nlayer)+rlaydist*dble(nvaclayer)
      a1_sc(1)=dble(n1)*a1(1)
      a1_sc(2)=0.0d0
      a1_sc(3)=0.0d0
      a2_sc(1)=0.0d0
      a2_sc(2)=dble(n2)*a2(2)
      a2_sc(3)=0.0d0
      a1_sc(3)=0.0d0
      a2_sc(3)=0.0d0
      do ii=1,3
        a3_sc(ii)=a3(ii)
      enddo
c     center of slab (cslb)
      cslb(1)=0.0d0
      cslb(2)=0.0d0
      cslb(3)=-rlaydist*dble(nlayer-1)/2.0d0 
c     ==--------------------------------------------------------------==
c     == set up the position of H2O molecule                          ==
c     ==--------------------------------------------------------------==
c     Water (1) "Down"
      theta(1)=theta(1)/180.0d0*pi
      alpha(1)=alpha(1)/180.0d0*pi
      tau_o(1,1)=0.50d0*a_sf
      tau_o(2,1)=0.50d0*sqrt(3.0d0)*a_sf
      tau_o(3,1)=d_om(1)  
      tau_h1(1,1)=tau_o(1,1)
      tau_h1(2,1)=tau_o(2,1)
      tau_h1(3,1)=tau_o(3,1)-d_oh(1)
      arg=pi/2.0d0-theta(1)
c     tau_h2(1,1)=tau_o(1,1)+0.50d0*d_oh(1)*cos(arg)
c     tau_h2(2,1)=tau_o(2,1)+0.50d0*sqrt(3.0d0)*d_oh(1)*cos(arg)
c     tau_h2(3,1)=tau_o(3,1)-d_oh(1)*sin(arg)

c     Water (2) "Parallel"
      theta(2)=theta(2)/180.0d0*pi
      alpha(2)=alpha(2)/180.0d0*pi
      tau_o(1,2)=0.0d0
      tau_o(2,2)=0.0d0
      tau_o(3,2)=d_om(2)
      arg=pi/3.0d0-theta(2)/2.0d0
      tau_h1(1,2)=d_oh(2)*cos(arg)
      tau_h1(2,2)=d_oh(2)*sin(arg)
      tau_h1(3,2)=d_om(2)+(d_oh(2)*sin(theta(2)/2.0d0))*sin(alpha(2))
      arg=pi/3.0d0+theta(2)/2.0d0
      tau_h2(1,2)=d_oh(2)*cos(arg)
      tau_h2(2,2)=d_oh(2)*sin(arg)
      tau_h2(3,2)=d_om(2)+(d_oh(2)*sin(theta(2)/2.0d0))*sin(alpha(2))
      tau_o(1,2)=tau_o(1,2)+a_sf
      tau_o(2,2)=tau_o(2,2)+sqrt(3.0d0)*a_sf
      tau_h1(1,2)=tau_h1(1,2)+a_sf
      tau_h1(2,2)=tau_h1(2,2)+sqrt(3.0d0)*a_sf
      tau_h2(1,2)=tau_h2(1,2)+a_sf
      tau_h2(2,2)=tau_h2(2,2)+sqrt(3.0d0)*a_sf
c     ==--------------------------------------------------------------==
c     == set atomic positions                                         ==
c     ==--------------------------------------------------------------==
      iatom=0
      natom=nlayer*3
      if(natom.gt.matom)then
        write(stderr,'(/,a)')
     &  ' *** ERROR: natom > matom'
        write(*,'(a,i5,a,i5)')' natom= ',natom,' matom= ',matom
        stop
      endif
      do ilayer=1,nlayer
        z=(-1.0d0)*dble(ilayer-1)*rlaydist
        tau0_tmp(1,1)=0.0d0
        tau0_tmp(2,1)=0.0d0
        tau0_tmp(3,1)=z
        tau0_tmp(1,2)=0.50d0*a_sf
        tau0_tmp(2,2)=0.50d0*sqrt(3.0d0)*a_sf
        tau0_tmp(3,2)=z
        tau0_tmp(1,3)=a_sf
        tau0_tmp(2,3)=sqrt(3.0d0)*a_sf
        tau0_tmp(3,3)=z
        if(mod(ilayer,3).eq.1)then
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,1)
          tau(2,iatom)=tau0_tmp(2,1)
          tau(3,iatom)=tau0_tmp(3,1)
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,2)
          tau(2,iatom)=tau0_tmp(2,2)
          tau(3,iatom)=tau0_tmp(3,2)
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,3)
          tau(2,iatom)=tau0_tmp(2,3)
          tau(3,iatom)=tau0_tmp(3,3)
        elseif(mod(ilayer,3).eq.2)then
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,1)
          tau(2,iatom)=tau0_tmp(2,1)+a_sf/sqrt(3.0d0)
          tau(3,iatom)=tau0_tmp(3,1)
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,2)
          tau(2,iatom)=tau0_tmp(2,2)+a_sf/sqrt(3.0d0)
          tau(3,iatom)=tau0_tmp(3,2)
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,3)
          tau(2,iatom)=tau0_tmp(2,3)+a_sf/sqrt(3.0d0)
          tau(3,iatom)=tau0_tmp(3,3)
        elseif(mod(ilayer,3).eq.0)then
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,1)
          tau(2,iatom)=tau0_tmp(2,1)-a_sf/sqrt(3.0d0)
          tau(3,iatom)=tau0_tmp(3,1)
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,2)
          tau(2,iatom)=tau0_tmp(2,2)-a_sf/sqrt(3.0d0)
          tau(3,iatom)=tau0_tmp(3,2)
          iatom=iatom+1
          tau(1,iatom)=tau0_tmp(1,3)
          tau(2,iatom)=tau0_tmp(2,3)-a_sf/sqrt(3.0d0)
          tau(3,iatom)=tau0_tmp(3,3)
        endif
      enddo
c     ==--------------------------------------------------------------==
      write(iout,*)
      write(iout,'(a,f18.8)')' a_0= ',a_0
      write(iout,'(/,a,i2,a)')' Slab of ',nlayer,' ML'
      write(iout,'(a,i2,a)')
     &' Vacuum region corresponds to ',nvaclayer,' ML'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac,' Bohr'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac*Bohr
     &,' Angstrom'
      write(iout,'(a,f6.2,a)')
     &' Size of the supercell along the z axis: '
     &,abs(a3(3)),' Bohr'
      write(iout,'(a,f6.2,a)')
     &' Size of the supercell along the z axis: '
     &,abs(a3(3))*Bohr,' Angstrom'
      write(iout,'(/,a)')      ' Primitive lattice vectors:'
      write(iout,'(3f18.8)')(a1(ii),ii=1,3)
      write(iout,'(3f18.8)')(a2(ii),ii=1,3)
      write(iout,'(3f18.8)')(a3(ii),ii=1,3)
      write(iout,'(/,a)')      ' Supercell lattice vectors:'
      write(iout,'(3f18.8)')(a1_sc(ii),ii=1,3)
      write(iout,'(3f18.8)')(a2_sc(ii),ii=1,3)
      write(iout,'(3f18.8)')(a3_sc(ii),ii=1,3)
      write(iout,'(a,i5)')   ' Number of atoms:',natom
      write(iout,'(a)')      ' Atoms: '
      do iatom=1,natom
        write(iout,'(3f18.8)')(tau(ii,iatom),ii=1,3)
      enddo 
      write(iout,'(a)')      ' Water(1):'
      write(iout,'(a)')      ' Hydrogen:'
      write(iout,'(3f18.8)')(tau_h1(ii,1),ii=1,3)
      write(iout,'(3f18.8)')(tau_h2(ii,1),ii=1,3)
      write(iout,'(a)')      ' Oxygen:'
      write(iout,'(3f18.8)')(tau_o(ii,1),ii=1,3)
      write(iout,'(a)')      ' Water(2):'
      write(iout,'(a)')      ' Hydrogen:'
      write(iout,'(3f18.8)')(tau_h1(ii,2),ii=1,3)
      write(iout,'(3f18.8)')(tau_h2(ii,2),ii=1,3)
      write(iout,'(a)')      ' Oxygen:'
      write(iout,'(3f18.8)')(tau_o(ii,2),ii=1,3)
c     ==--------------------------------------------------------------==
      close(input)
      close(iout)
c     ==--------------------------------------------------------------==
c     == generate XYZ file                                            ==
c     ==--------------------------------------------------------------==
      if(natom.lt.10)then
        write(num,'(i1)')natom
        filen='H4O2:'//atmnm//num(1:1)//'.xyz'
      elseif(natom.lt.100)then
        write(num,'(i2)')natom
        filen='H4O2:'//atmnm//num(1:2)//'.xyz'
      elseif(natom.lt.1000)then
        write(num,'(i3)')natom
        filen='H4O2:'//atmnm//num(1:3)//'.xyz'
      endif
      write(*,'(/,a,a)')'Generating ',filen
      open(iout,file=filen,status='unknown')
      write(iout,'(i3)') natom+6
      write(iout,'(a)') 'H up water bylayer on '//atmnm//num
      write(iout,'(a,3f18.8)')'H '
     &,(tau_h1(ii,1)*bohr,ii=1,3)
      write(iout,'(a,3f18.8)')'H '
     &,(tau_h2(ii,1)*bohr,ii=1,3)
      write(iout,'(a,3f18.8)')'O '
     &,(tau_o(ii,1)*bohr,ii=1,3)
      write(iout,'(a,3f18.8)')'H '
     &,(tau_h1(ii,2)*bohr,ii=1,3)
      write(iout,'(a,3f18.8)')'H '
     &,(tau_h2(ii,2)*bohr,ii=1,3)
      write(iout,'(a,3f18.8)')'O '
     &,(tau_o(ii,2)*bohr,ii=1,3)
      do iatom=1,natom
        write(iout,'(a,3f18.8)')atmnm
     &  ,(tau(ii,iatom)*bohr,ii=1,3)
      enddo
      close(iout)
c     ==--------------------------------------------------------------==
      stop
      end
c     ==================================================================
