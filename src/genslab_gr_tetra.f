c     ==================================================================
      program main
c     ==--------------------------------------------------------------==
c     == This is a utility program to generate a graphene structure   ==
c     == an orthorhombic cell is generated, instead of hexagonal one  ==
c     ==--------------------------------------------------------------==
      implicit none
      integer :: iatom, natom
      integer, parameter :: matom = 400
      integer :: zatom
      integer :: ilayer,nlayer,nvaclayer,i1,i2,n1,n2
      integer :: ivac
      double precision :: a_0, b_0, c_0, a, b, a_sf
      double precision :: bohr, rlaydist, rvac,z
      double precision :: cslb(3)
      double precision :: a1(3),a2(3),a3(3),a1_sc(3),a2_sc(3),a3_sc(3)
      double precision :: tau(3,matom),taus(3,matom),tau_tmp(3)
      double precision :: tau0_tmp(3,4)
      double precision :: sum
      double precision :: xmat(3,3),xmati(3,3)
      double precision :: vac
      character(len=2) :: atmnm
      character(len=3) :: num
      character(len=80) :: filen
      character(len=256) :: str
      integer :: input,iout,stdin,stdout,stderr, ios,len
      integer :: ixyz,iatps
      character(len=80) :: infile,outfile
      logical :: fexist
c ... arguments
      integer :: iarg, iargc, narg, marg
      character(len=80) :: arg
c ... options
      integer :: ipw=0, istate=0
c ... others
      integer :: idum
c ... counter
      integer :: ii, jj, kk
c     ==--------------------------------------------------------------==
c     == set constants                                                ==
c     ==--------------------------------------------------------------==
      stdin  = 05
      stdout = 06
      stderr = 00
      input  = 10
      iout   = 20
      ixyz   = 20
      bohr = 0.529177
      num = '   '
c     ==--------------------------------------------------------------==
c ... arguments
      narg=iargc()
      if(narg.gt.0)then
        call getarg(1,arg)
        select case(trim(arg))
          case('-pw') 
            ipw=1
          case('-pw-crystal')
            ipw=2
          case('-state')
            istate=1
        end select 
      endif
c     ==--------------------------------------------------------------==
      write(stdout,'(a,$)')'Enter input file> '
      read(stdin,'(a)')infile
      write(stdout,'(a,$)')'Enter output file> '
      read(stdin,'(a)')outfile
      len=index(infile,' ')
      write(stdout,'(/,/,a,a)')'Input file : ',infile(:len-1)
      len=index(outfile,' ')
      write(stdout,'(a,a)')    'Output file: ',outfile(:len-1) 
      inquire(file=infile,exist=fexist)
      if(.not.fexist)then
        len=index(infile,' ')
        write(stderr,'(/,a,a)') infile(:len-1),' does not exist.'
        stop
      endif
      open(input,file=infile,iostat=ios,status='old')
      if(ios.ne.0)then
        write(stderr,'(a)')
     &  '*** ERROR occurs while opening the input file.'
        stop
      endif
      open(iout,file=outfile,status='unknown')
c     ==--------------------------------------------------------------==
      write(iout,'(/,a,a,a,/,a,a,a,/,a,a,a,/,a,a,a,/)')
     &' ***',
     &'***********************************************************',
     &'****',
     &' ***',
     &' This program generates a graphene structure               ',
     &' ***',
     &' An orthorhombic unit cell is used                         ',
     &' ***',
     &' ***',
     &'***********************************************************',
     &'****'
c     ==--------------------------------------------------------------==
      read(input,'(a)') atmnm
      read(input,*) a_0, c_0
      read(input,*) n1, n2
      a_0 = dble(a_0)
      b_0 = a_0 * sqrt(3.d0)
      c_0 = dble(c_0)
      nlayer = 1
      nvaclayer = 1
c     ==--------------------------------------------------------------==
c     == set lattice vectors                                          ==
c     ==--------------------------------------------------------------==
      do ii = 1, 3
        a1(ii) = 0.0d0
        a2(ii) = 0.0d0
        a3(ii) = 0.0d0
      enddo
      a = a_0
      a_sf = a_0
      rlaydist = c_0 * 0.5d0
      rvac = c_0
      a1(1) = a
      a2(2) = sqrt(3.d0) * a
      a3(3) = c_0
      a1_sc(:) = a1(:) * dble(n1)
      a2_sc(:) = a2(:) * dble(n2)
      a3_sc(:) = a3(:)
c     center of slab (cslb)
      cslb(1) = 0.0d0
      cslb(2) = 0.0d0
      cslb(3) = 0.0d0
c     ==--------------------------------------------------------------==
c
c     atomic position in the crystal coordinate of the primitive cell
c
      tau0_tmp(1,1) = 0.0d0
      tau0_tmp(2,1) = 0.0d0
      tau0_tmp(3,1) = 0.0d0
c
      tau0_tmp(1,2) = 0.5d0
      tau0_tmp(2,2) = 1.d0/6.d0
      tau0_tmp(3,2) = 0.0d0
c
      tau0_tmp(1,3) = 0.5d0
      tau0_tmp(2,3) = 1.d0/2.d0
      tau0_tmp(3,3) = 0.0d0
c
      tau0_tmp(1,4) = 0.0d0
      tau0_tmp(2,4) = 2.d0/3.d0
      tau0_tmp(3,4) = 0.0d0
c
      natom = 4 * n1 * n2
      iatom = 0
      taus(:,:) = 0.d0
      do i2 = 1, n2
        do i1 = 1, n1
          do ii = 1, 4
            iatom = iatom + 1
            tau_tmp(1) = tau0_tmp(1,ii) * a1(1) + 
     &                   tau0_tmp(2,ii) * a2(1)
            tau_tmp(2) = tau0_tmp(2,ii) * a1(2) + 
     &                   tau0_tmp(2,ii) * a2(2)
            tau_tmp(3) = tau0_tmp(3,ii)
            tau(1,iatom) = tau_tmp(1) 
     &                    + dble(i1-1) * a1(1) + dble(i2-1) * a2(1)
            tau(2,iatom) = tau_tmp(2) 
     &                    + dble(i1-1) * a1(2) + dble(i2-1) * a2(2)
            tau(3,iatom) = tau_tmp(3)
          enddo 
        enddo
      enddo
c     ==--------------------------------------------------------------==
      do ii=1,3
        xmat(ii,1)=a1_sc(ii)
        xmat(ii,2)=a2_sc(ii)
        xmat(ii,3)=a3_sc(ii)
      enddo
      call matinv(xmat,xmati,sum)
      do iatom=1,natom
        do ii=1,3
          sum=0.0d0
          do jj=1,3
            sum=sum+xmati(ii,jj)*tau(jj,iatom)
          enddo
          taus(ii,iatom)=sum
        enddo
      enddo
c     ==--------------------------------------------------------------==
      write(iout,*)
      write(iout,'(a,f18.8)')' a_0= ',a_0
      write(iout,'(a,f18.8)')' b_0= ',b_0
      write(iout,'(a,f18.8)')' c_0= ',c_0
      write(iout,'(a,f18.8)')' Inter layer distance: ',rlaydist
      write(iout,'(/,a,i2,a)')' Slab of ',nlayer,' ML'
      write(iout,'(a,i2,a)')
     &' Vacuum region corresponds to ',nvaclayer,' ML'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac,' Bohr'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac*Bohr
     &,' Angstrom'
      write(iout,'(a,f6.2,a)')
     &' Size of the supercell along the z axis: '
     &,abs(a3(3)),' Bohr'
      write(iout,'(a,f6.2,a)')
     &' Size of the supercell along the z axis: '
     &,abs(a3(3))*Bohr,' Angstrom'
      write(iout,'(/,a)')      ' Primitive lattice vectors:'
      write(iout,'(3f20.12)')(a1(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3(ii),ii=1,3)
      write(iout,'(/,a)')      ' Supercell lattice vectors:'
      write(iout,'(3f20.12)')(a1_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3_sc(ii),ii=1,3)
      write(iout,'(a,i5)')   ' Number of atoms:',natom
      write(iout,'(a)')      ' Atoms: '
      do iatom=1,natom
        write(iout,'(3f20.12,3i5)')(tau(ii,iatom),ii=1,3),1,1,1
      enddo 
!     write(iout,'(/,a)')
!    &' Atoms (center of the slab is set to the origin):'
!     do iatom=1,natom
!       write(iout,'(3f20.12,3i5)')(tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
!     enddo 
      if(ipw .eq. 1)then
c ... PW output
        filen='atps'
        open(iatps,file=filen,status='unknown')
        write(iatps,'(a)')       ' ibrav = 8'
        write(iatps,'(a,i0)')    ' nat   = ',natom
        write(iatps,'(a,i1)')    ' ntyp  = ',1
        write(iatps,'(a,f20.12)')' celldm(1) = ',a1_sc(1)
        write(iatps,'(a,f20.12)')' celldm(2) = ',a1_sc(2)/a1_sc(1)
        write(iatps,'(a,f20.12)')' celldm(3) = ',a3_sc(3)/a1_sc(1)
        write(iatps,'(a)')'CELL_PARAMETERS (bohr)'
        write(iatps,'(3f20.12)')(a1_sc(ii),ii=1,3)
        write(iatps,'(3f20.12)')(a2_sc(ii),ii=1,3)
        write(iatps,'(3f20.12)')(a3_sc(ii),ii=1,3)
        write(iatps,'(a)')'ATOMIC_POSITIONS (bohr)'
        do iatom=1,natom
          write(iatps,'(a,3f20.12,3i5)')
     &    trim(atmnm),(tau(ii,iatom),ii=1,3),1,1,1
        enddo 
        close(iatps) 
c ... PW output (Angstrom)
        filen='atps_angstrom'
        open(iatps,file=filen,status='unknown')
        write(iatps,'(a)')       ' ibrav = 8'
        write(iatps,'(a,i0)')    ' nat   = ',natom
        write(iatps,'(a,i1)')    ' ntyp  = ',1
        write(iatps,'(a,f20.12)')' A = ',a1_sc(1)*bohr
        write(iatps,'(a,f20.12)')' B = ',a2_sc(2)*bohr
        write(iatps,'(a,f20.12)')' C = ',a3_sc(3)*bohr
        write(iatps,'(a)')'CELL_PARAMETERS (angstrom)'
        write(iatps,'(3f20.12)')(a1_sc(ii)*bohr,ii=1,3)
        write(iatps,'(3f20.12)')(a2_sc(ii)*bohr,ii=1,3)
        write(iatps,'(3f20.12)')(a3_sc(ii)*bohr,ii=1,3)
        write(iatps,'(a)')'ATOMIC_POSITIONS (angstrom)'
        do iatom=1,natom
          write(iatps,'(a,3f20.12,3i5)')
     &    trim(atmnm),(tau(ii,iatom)*bohr,ii=1,3),1,1,1
        enddo 
        close(iatps) 
      elseif(ipw .eq. 2)then
c ... PW output (Angstrom and crystal)
        filen='atps_angstrom_crystal'
        open(iatps,file=filen,status='unknown')
        write(iatps,'(a)')       ' ibrav = 0'
        write(iatps,'(a,i3)')    ' nat   =',natom
        write(iatps,'(a,i1)')    ' ntyp  =',1
        write(iatps,'(a)')'CELL_PARAMETERS angstrom'
        write(iatps,'(3f20.12)')(a1_sc(ii)*bohr,ii=1,3)
        write(iatps,'(3f20.12)')(a2_sc(ii)*bohr,ii=1,3)
        write(iatps,'(3f20.12)')(a3_sc(ii)*bohr,ii=1,3)
        write(iatps,'(a)')'ATOMIC_POSITIONS crystal'
        do iatom=1,natom
          write(iatps,'(a,3f20.12,3i5)')
     &    trim(atmnm),(taus(ii,iatom),ii=1,3),1,1,1
        enddo 
        close(iatps) 
      endif
c     ==--------------------------------------------------------------==
c     == generate XSF file                                            ==
c     ==--------------------------------------------------------------==
      if(natom.lt.10)then
        write(num,'(i1)')natom
        filen=trim(atmnm)//num(1:1)//'.xsf'
      elseif(natom.lt.100)then
        write(num,'(i2)')natom
        filen=trim(atmnm)//num(1:2)//'.xsf'
      elseif(natom.lt.1000)then
        write(num,'(i3)')natom
        filen=trim(atmnm)//num(1:3)//'.xsf'
      endif
      write(*,'(/,a,a)')'Generating ',filen
      open(ixyz,file=filen,status='unknown')
      write(ixyz,'(a)')' SLAB'
      write(ixyz,'(a)')' PRIMVEC'
      write(ixyz,'(3f15.6)')(a1_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(3f15.6)')(a2_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(3f15.6)')(a3_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(a)')' PRIMCOORD'
      idum=1
      write(ixyz,'(i5,i1)')natom,idum
      do iatom=1,natom
        idum=zatom(trim(atmnm))
        write(ixyz,'(i5,3(f15.9,2x))')idum
     &  ,(tau(ii,iatom)*bohr,ii=1,3)
      enddo
      close(ixyz)
c     ==--------------------------------------------------------------==
      stop
      end
c     ==================================================================
