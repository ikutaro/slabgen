c     ==================================================================
      subroutine pbc(tau,taunew,a1,a2,a3)
c     ==--------------------------------------------------------------==
      real*8 tau(3),taunew(3),a1(3),a2(3),a3(3)
      real*8 taus(3),xmat(3,3),xmati(3,3),sum
      integer ii,jj
c     ==--------------------------------------------------------------==
      do ii=1,3
        xmat(ii,1)=a1(ii)
        xmat(ii,2)=a2(ii)
        xmat(ii,3)=a3(ii)
      enddo 
      call matinv(xmat,xmati,sum)
      do ii=1,3
        sum=0.0d0
        do jj=1,3
          sum=sum+xmati(ii,jj)*tau(jj)
        enddo
        taus(ii)=sum
      enddo
      do ii=1,3
        taus(ii)=taus(ii)-int(taus(ii))
      enddo
      do ii=1,3
        taunew(ii)=taus(1)*a1(ii)+taus(2)*a2(ii)+taus(3)*a3(ii)
      enddo
c     ==--------------------------------------------------------------==
      return
      end
c     ==================================================================
