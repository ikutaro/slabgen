!     ==================================================================
      PROGRAM main
!     ==--------------------------------------------------------------==
!     == This is a utility program to generate an Rutile (110)        ==
!     ==--------------------------------------------------------------==
      IMPLICIT NONE
      INTEGER :: iatom, natom
      INTEGER, PARAMETER :: matom = 1000
      INTEGER :: ilayer, nlayer, nvaclayer, n1, n2
      REAL(KIND=8) :: a_0, b_0, c_0, a, b, c, a_sf, b_sf, c_sf
      REAL(KIND=8) :: u_0
      REAL(KIND=8) :: rlaydist, rvac, z, area, vol
      REAL(KIND=8) :: bohr
      REAL(KIND=8) :: a1(3), a2(3), a3(3), a1_sc(3), a2_sc(3), a3_sc(3)
      REAL(KIND=8) :: dtau(3)
      REAL(KIND=8) :: tau(3,matom),taus(3,matom),tau0_tmp(3),cslb(3)
      REAL(KIND=8) :: tau_new(3,matom)
      INTEGER      :: ityp(matom)
      CHARACTER(LEN=2) :: atmnm, atmA, atmB
      CHARACTER(LEN=2) :: at(matom)
      CHARACTER(LEN=5) :: num, n_m, n_o
      CHARACTER(LEN=80) :: str, str_opt
      CHARACTER(LEN=20) filen
      INTEGER :: input,iout,ixyz,iatps,stdin,stdout,stderr,ios,len
      INTEGER :: lenH, lenO
      CHARACTER(LEN=80) :: infile,outfile
      LOGICAL :: fexist
! ... Variable
      INTEGER      :: ii, jj, kk, i1, i2, i3
      REAL(KIND=8) :: sum
      REAL(KIND=8) :: xmat(3,3), xmati(3,3)
      REAL(KIND=8) :: ab(3), bc(3), ca(3)
      INTEGER :: zatom
! ... arguments
      INTEGER :: iarg, iargc, narg, marg
      CHARACTER(LEN=80) :: arg
! ... options
      INTEGER :: ipw = 0, istate = 0
      INTEGER :: iterm = 0
! ... others
      INTEGER :: idum, ia_m, ia_o
!     ==--------------------------------------------------------------==
!     == set constants                                                ==
!     ==--------------------------------------------------------------==
      stdin = 05
      stdout= 06
      stderr= 00
      input = 10
      iout  = 20
      ixyz  = 25
      iatps = 26
      bohr = 0.529177210903D0
!     ==--------------------------------------------------------------==
! ... arguments
      narg = iargc()
      if(narg > 0)then
        call getarg(1,arg)
        select case(trim(arg))
          case('-pw') 
            ipw = 1
          case('-state')
            istate = 1
        end select 
      endif
!     ==--------------------------------------------------------------==
      WRITE(stdout,'(a,$)') 'Enter input file> '
      READ(stdin,'(a)') infile
      WRITE(stdout,'(a,$)') 'Enter output file> '
      READ(stdin,'(a)') outfile
      WRITE(stdout,'(/,a,a,a)')'Input file is ', TRIM(infile),'.'
      WRITE(stdout,'(/,a,a,a)')'Output file is ',TRIM(outfile),'.'
      inquire(file=infile,exist=fexist)
      if(.not. fexist)then
        len=index(infile,' ')
        WRITE(stderr,'(/,a,a)') infile(:len-1),' does not exist.'
        stop
      endif
      open(input,file=infile,iostat=ios,status='old')
      if(ios /= 0)then
        WRITE(stderr,'(a)')&
     &  '*** ERROR occurs while opening the input file.'
        stop
      endif
      open(iout,file=outfile,status='unknown')
!     ==--------------------------------------------------------------==
      WRITE(iout,'(/,a,a,a,/,a,a,a/,a,a,a,/)') &
     &' ***', &
     &'***********************************************************', &
     &'****', &
     &' ***', &
     &' This program generates a rutile (MO2) (110) clean surface ', &
     &' ***', &
     &' ***', &
     &'***********************************************************', &
     &'****'
!     ==--------------------------------------------------------------==
      READ(input,'(a)')str
      READ(input,*) a_0, c_0
      READ(input,*) u_0
      READ(input,*) nlayer
      READ(input,*) nvaclayer
      READ(input,*, IOSTAT=ios) n1, n2
      IF(ios /= 0)THEN
        n1 = 1; n2 = 1
      ENDIF
      READ(input,'(A)',IOSTAT=ios) str_opt
      IF(ios == 0)THEN
        SELECT CASE(TRIM(str_opt))
        CASE('OBRI','O_BRI','O_bri','obri','o_bri')
          iterm = 0
        CASE('MCUS','M_CUS','M_cus','M','m')
          iterm = 1
        CASE('OCUS','O_CUS','O_cus','ocus','o_cus','O','o')
          iterm = 2
        CASE DEFAULT
          iterm = 0
        END SELECT
      ENDIF
!     ==--------------------------------------------------------------==
!     == set the cation name                                          ==
!     ==--------------------------------------------------------------==
! === original ===
!     str = adjustl(str)
!     atmnm = str(1:2)
! === new ===
      lenH = index(TRIM(str),'H')
      lenO = index(TRIM(str),'O')
      IF (lenH > 0 .OR. lenO > 0) THEN
        IF(lenH > 0)THEN
          atmA = str(1:lenH-1)
          atmB = str(lenH:lenH)
        ELSE IF(lenO > 0)THEN
          atmA = str(1:lenO-1)
          atmB = str(lenO:lenO)
        END IF
      ELSE
        ! Assuming that A has 2 characters and B has 1 character
        atmA = str(1:2)
        atmB = str(3:3)
      END IF
!     ==--------------------------------------------------------------==
!     == set lattice vectors                                          ==
!     ==--------------------------------------------------------------==
      a_0 = DBLE(a_0)
      c_0 = DBLE(c_0)
      u_0 = DBLE(u_0)
      a_sf = SQRT(2.d0) * a_0
      b_sf = c_0
      rlaydist = a_0 / SQRT(2.D0)
      rvac = rlaydist * DBLE(nvaclayer+1)
      a1(:) = 0.D0; a2(:) = 0.D0; a3(:) = 0.D0
      a1(1) = SQRT(2.D0) * a_0
      a2(2) = c_0
      a3(3) = rlaydist * DBLE(nlayer) + rlaydist * DBLE(nvaclayer)
      DO ii = 1, 2
        a1_sc(ii) = DBLE(n1) * a1(ii)
        a2_sc(ii) = DBLE(n2) * a2(ii)
        a3_sc(ii) = a3(ii)
      END DO
      a1_sc(3) = a1(3)
      a2_sc(3) = a2(3)
      a3_sc(3) = a3(3)
      ab(1) = a1_sc(2)*a2_sc(3)-a1_sc(3)*a2_sc(2)
      ab(2) = a1_sc(3)*a2_sc(1)-a1_sc(1)*a2_sc(3)
      ab(3) = a1_sc(1)*a2_sc(2)-a1_sc(2)*a2_sc(1)
      area = 0.0D0
      DO ii = 1, 3
        area = area + ab(ii) * ab(ii)
      END DO
      area = ABS(area)
      area = SQRT(area)
      vol = 0.0d0
      DO ii = 1, 3
        vol = vol + ab(ii) * a3_sc(ii)
      END DO
      vol = ABS(vol)
      cslb(1) = 0.D0
      cslb(2) = 0.D0
      cslb(3) = -rlaydist * DBLE(nlayer-1) * 0.5d0
!     ==--------------------------------------------------------------==
!     == Set atomic positions in the cartesian coordinate             ==
!     ==--------------------------------------------------------------==
      iatom = 0
      ia_m = 0
      ia_o = 0
      ! Assuming the stoichometric composition (MO2 *2 = 6 per layer)
      natom = nlayer * 6 * n1 * n2
      IF(iterm == 1)THEN
        natom = natom - (2 * n1 * n2)
      ELSE IF(iterm == 2)THEN
        natom = natom + (2 * n1 * n2)
      END IF
      IF(natom > matom)THEN
        WRITE(stderr,'(/,A)') &
     &  ' *** ERROR: natom > matom'
        WRITE(*,'(A,I5,A,I5)')' natom= ',natom,' matom= ',matom
        STOP
      END IF
      LAYER_LOOP: DO ilayer = 1, nlayer
        z = (-1.0d0) * DBLE(ilayer - 1) * rlaydist
        dtau(:) = 0.D0
        IF(MOD(ilayer,2) == 0)THEN
          dtau(1) = 0.5D0 * SQRT(2.D0) * a_0
        END IF
        !
        ! ... metal #1
        !
        tau0_tmp(1) = 0.0d0 + dtau(1)
        tau0_tmp(2) = 0.0d0 + dtau(2)
        tau0_tmp(3) = z
        DO i2 = 1, n2
          DO i1 = 1, n1
            iatom = iatom + 1
            ia_m = ia_m + 1
            tau(1,iatom) = tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
            tau(2,iatom) = tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
            tau(3,iatom) = tau0_tmp(3)
            at(iatom) = TRIM(atmA)
            ityp(iatom) = 1
          END DO
        END DO
        !
        ! ... metal #2
        !
        tau0_tmp(1) = a_0 / SQRT(2.D0) + dtau(1)
        tau0_tmp(2) = c_0 * 0.5D0      + dtau(2)
        tau0_tmp(3) = z
        DO i2 = 1, n2
          DO i1 = 1, n1
            iatom = iatom + 1
            ia_m = ia_m + 1
            tau(1,iatom) = tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
            tau(2,iatom) = tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
            tau(3,iatom) = tau0_tmp(3)
            at(iatom) = TRIM(atmA)
            ityp(iatom) = 1
          END DO
        END DO
        !
        ! ...  oxygen #1
        !
        tau0_tmp(1) = SQRT(2.D0) * u_0 * a_0 + dtau(1)
        tau0_tmp(2) = 0.0d0                  + dtau(2)
        tau0_tmp(3) = z
        DO i2 = 1, n2
          DO i1 = 1, n1
            iatom = iatom + 1
            ia_o = ia_o + 1
            tau(1,iatom) = tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
            tau(2,iatom) = tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
            tau(3,iatom) = tau0_tmp(3)
            at(iatom) = TRIM(atmB)
            ityp(iatom) = 2
          END DO
        END DO
        !
        ! ...  oxygen #2
        !
        tau0_tmp(1) = - SQRT(2.D0) * u_0 * a_0 + dtau(1)
        tau0_tmp(2) =   0.0d0                  + dtau(2)
        tau0_tmp(3) = z
        DO i2 = 1, n2
          DO i1 = 1, n1
            iatom = iatom + 1
            ia_o = ia_o + 1
            tau(1,iatom) = tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
            tau(2,iatom) = tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
            tau(3,iatom) = tau0_tmp(3)
            at(iatom) = TRIM(atmB)
            ityp(iatom) = 2
          END DO
        END DO
        !
        ! ...  oxygen #3 (Upper bridging oxygen)
        !
        IF(iterm /= 1 .OR. ilayer /= 1)THEN
          tau0_tmp(1) = 0.0D0       + dtau(1)
          tau0_tmp(2) = 0.5D0 * c_0 + dtau(2)
          tau0_tmp(3) = SQRT(2.D0) * (0.5D0 - u_0) * a_0 + z
          DO i2 = 1, n2
            DO i1 = 1, n1
              iatom = iatom + 1
              ia_o = ia_o + 1
              tau(1,iatom) = tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
              tau(2,iatom) = tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
              tau(3,iatom) = tau0_tmp(3)
              at(iatom) = TRIM(atmB)
              ityp(iatom) = 2
            END DO
          END DO
        END IF
        !
        ! ...  oxygen #4 (Lower bridging oxygen)
        !
        IF(iterm /= 1 .OR. ilayer /= nlayer)THEN
          tau0_tmp(1) = 0.0D0       + dtau(1)
          tau0_tmp(2) = 0.5D0 * c_0 + dtau(2)
          tau0_tmp(3) = - SQRT(2.D0) * (0.5D0 - u_0) * a_0 + z 
          DO i2 = 1, n2
            DO i1 = 1, n1
              iatom = iatom + 1
              ia_o = ia_o + 1
              tau(1,iatom) = tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
              tau(2,iatom) = tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
              tau(3,iatom) = tau0_tmp(3)
              at(iatom) = TRIM(atmB)
              ityp(iatom) = 2
            END DO
          END DO
        END IF
        !
        ! ... In the case of the O excess surface (O_cus)
        !
        IF(iterm == 2)THEN
          IF(ilayer == 1)THEN
            !
            ! ... oxygen in the upper layer
            !
            tau0_tmp(1) = a_0 / SQRT(2.D0) + dtau(1)
            tau0_tmp(2) = c_0 * 0.5D0      + dtau(2)
            tau0_tmp(3) = z + SQRT(2.D0) * u_0 * a_0
            DO i2 = 1, n2
              DO i1 = 1, n1
                iatom = iatom + 1
                ia_o = ia_o + 1
                tau(1,iatom) = tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
                tau(2,iatom) = tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
                tau(3,iatom) = tau0_tmp(3)
                at(iatom) = TRIM(atmB)
                ityp(iatom) = 2
              END DO
            END DO
          ELSE IF(ilayer == nlayer)THEN
            !
            ! ... oxygen in the lower layer
            !
            tau0_tmp(1) = a_0 / SQRT(2.D0) + dtau(1)
            tau0_tmp(2) = c_0 * 0.5D0      + dtau(2)
            tau0_tmp(3) = z - SQRT(2.D0) * u_0 * a_0
            DO i2 = 1, n2
              DO i1 = 1, n1
                iatom = iatom + 1
                ia_o = ia_o + 1
                tau(1,iatom) = tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
                tau(2,iatom) = tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
                tau(3,iatom) = tau0_tmp(3)
                at(iatom) = TRIM(atmB)
                ityp(iatom) = 2
              END DO
            END DO
          END IF
        END IF
      END DO LAYER_LOOP
!     ==--------------------------------------------------------------==
!     == Set atomic positions in the reduced coordinate               ==
!     ==--------------------------------------------------------------==
      DO ii = 1, 3
        xmat(ii,1) = a1_sc(ii)
        xmat(ii,2) = a2_sc(ii)
        xmat(ii,3) = a3_sc(ii)
      END DO
      CALL matinv(xmat,xmati,sum)
      DO iatom = 1, natom
        DO ii = 1, 3
          sum = 0.0D0
          DO jj = 1, 3
            sum = sum + xmati(ii,jj) * tau(jj,iatom)
          END DO
          taus(ii,iatom) = sum
        END DO
      END DO
!     ==--------------------------------------------------------------==
      WRITE(iout,'(A,A)')     ' Cation: ',TRIM(atmA)
      WRITE(iout,'(A,A)')     ' Anion : ',TRIM(atmB)
      WRITE(iout,*)
      WRITE(iout,'(A,F18.8)') ' a_0 = ', a_0
      WRITE(iout,'(A,F18.8)') ' c_0 = ', c_0
      WRITE(iout,'(A,F18.8)') ' a   = ', a_sf
      WRITE(iout,'(A,F18.8)') ' b   = ', b_sf
      IF(iterm == 0)THEN
        WRITE(iout,'(/,1X,A)') &
     &  'O_bri-terminated surface (stoichometic)'
      ELSE IF(iterm == 1)THEN
        WRITE(iout,'(/,1X,A,A)') &
     &  TRIM(atmnm),'-terminated surface (O deficit)'
      ELSE IF(iterm == 2)THEN
        WRITE(iout,'(/,1X,A)') &
     &  'O_cus-terminated surface (O excess)'
      END IF
      WRITE(iout,'(/,1X,A,I0,A,/)')'Slab of ',nlayer,' ML (trilayer)'
      WRITE(iout,'(1X,A,I2,A)') &
     &'Vacuum region corresponds to ',nvaclayer,' ML'
      WRITE(iout,'(1X,A,F6.2,A)') &
     &'Slabs are separated by ',rvac,' Bohr (metal-metal distance)'
      WRITE(iout,'(1X,A,F6.2,A)') &
     &'Slabs are separated by ',rvac*Bohr, &
     &' Angstrom (metal-metal distance)'
      WRITE(iout,*)
      WRITE(iout,'(1X,A,F18.8)')'Inter-layer distance = ',rlaydist
      WRITE(iout,'(1X,A,F18.8)')'Slab-slab distance   = ',rvac
      WRITE(iout,'(/,a)')    ' Lattice vectors:'
      WRITE(iout,'(3F20.12)')(a1(ii),ii=1,3)
      WRITE(iout,'(3F20.12)')(a2(ii),ii=1,3)
      WRITE(iout,'(3F20.12)')(a3(ii),ii=1,3)
      WRITE(iout,'(/,1X,A)')    'Supercell Lattice vectors:'
      WRITE(iout,'(3F20.12)')(a1_sc(ii),ii=1,3)
      WRITE(iout,'(3F20.12)')(a2_sc(ii),ii=1,3)
      WRITE(iout,'(3F20.12)')(a3_sc(ii),ii=1,3)
      WRITE(iout,'(/,1X,A,f20.12)')'Surface area: ',area
      !WRITE(iout,'(1X,A,F20.12)')'Surface area: ',vol/a3_sc(3)
      WRITE(iout,'(/,1X,A,F20.12)')'Volume of supercell: ',vol
      WRITE(iout,'(/,1X,A,I5)')    'Number of atoms:',natom
      WRITE(iout,'(A)')      ' Atoms: '
      DO iatom = 1, natom
        WRITE(iout,'(3F20.12,3I5,1X,A)') &
     &  (tau(ii,iatom),ii=1,3),1,1,ityp(iatom),TRIM(at(iatom))
      END DO
      WRITE(iout,'(/,1X,A)')'Atomic coordinates in reduced coordinate '
      DO iatom = 1, natom
        WRITE(iout,'(3F20.12,3I5,1X,A)') &
     &  (taus(ii,iatom),ii=1,3),1,1,ityp(iatom),TRIM(at(iatom))
      END DO
      WRITE(iout,'(/,1X,A)') &
     &'Atoms (center of the slab is set to the origin):' 
      DO iatom = 1, natom
        WRITE(iout,'(3F20.12,3I5,1X,A)') &
     &  (tau(ii,iatom)-cslb(ii),ii=1,3),1,1,ityp(iatom),TRIM(at(iatom))
      END DO
      IF(ipw == 1)THEN
! ... PW output
        filen = 'atps'
        OPEN(UNIT=iatps, FILE=filen, STATUS='UNKNOWN')
        !
        ! TO BE UPDATED AND INCLUDED LATER
        ! WRITE(iatps,'(a)')       ' ibrav = 4'
        ! WRITE(iatps,'(a,i3)')    ' nat   =',natom
        ! WRITE(iatps,'(a,i1)')    ' ntyp  =',1
        ! WRITE(iatps,'(a,f20.12)')' celldm(1) = ',a1_sc(1)
        ! WRITE(iatps,'(a,f20.12)')' celldm(3) = ',a3_sc(3)/a1_sc(1)
        WRITE(iatps,'(A)')'CELL_PARAMETERS (bohr)'
        WRITE(iatps,'(3F20.12)')(a1_sc(ii),ii=1,3)
        WRITE(iatps,'(3F20.12)')(a2_sc(ii),ii=1,3)
        WRITE(iatps,'(3F20.12)')(a3_sc(ii),ii=1,3)
        WRITE(iatps,'(A)')'ATOMIC_POSITIONS (bohr)'
        DO iatom = 1, natom
          WRITE(iatps,'(A2,3f20.12,3i5)') &
     &    trim(at(iatom)),(tau(ii,iatom),ii=1,3),1,1,1
        END DO
        WRITE(iatps,'(A)')''
        WRITE(iatps,'(A)')'# Atomic positions centered at z=0'
        WRITE(iatps,'(A)')'ATOMIC_POSITIONS (bohr)'
        DO iatom = 1, natom
          WRITE(iatps,'(A2,3F20.12,3I5)') &
     &    trim(at(iatom)),(tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
        END DO
        CLOSE(iatps) 
! ... PW output in Angstrom
        filen='atps_angstrom'
        OPEN(UNIT=iatps, FILE=filen, STATUS='unknown')
        ! TO BE UPDATED AND INCLUDED LATER
        ! WRITE(iatps,'(a)')       ' ibrav = 4'
        ! WRITE(iatps,'(a,i3)')    ' nat   =',natom
        ! WRITE(iatps,'(a,i1)')    ' ntyp  =',1
        ! WRITE(iatps,'(a,f20.12)')' A = ',a1_sc(1)*bohr
        ! WRITE(iatps,'(a,f20.12)')' C = ',a3_sc(3)*bohr
        WRITE(iatps,'(A)')'CELL_PARAMETERS (angstrom)'
        WRITE(iatps,'(3F20.12)')(a1_sc(ii)*bohr,ii=1,3)
        WRITE(iatps,'(3F20.12)')(a2_sc(ii)*bohr,ii=1,3)
        WRITE(iatps,'(3F20.12)')(a3_sc(ii)*bohr,ii=1,3)
        WRITE(iatps,'(A)')'ATOMIC_POSITIONS (angstrom)'
        DO iatom = 1, natom
          WRITE(iatps,'(A2,3F20.12,3I5)') &
     &    TRIM(at(iatom)),(tau(ii,iatom)*bohr,ii=1,3),1,1,1
        END DO
        WRITE(iatps,'(A)')''
        WRITE(iatps,'(A)')'# Atomic positions centered at z=0'
        WRITE(iatps,'(A)')'ATOMIC_POSITIONS (angstrom)'
        DO iatom = 1, natom
          WRITE(iatps,'(A2,3F20.12,3I5)') &
     &    TRIM(at(iatom)),((tau(ii,iatom)-cslb(ii))*bohr,ii=1,3),1,1,1
        END DO 
        CLOSE(iatps) 
      END IF
!     ==--------------------------------------------------------------==
!     == generate XYZ file                                            ==
!     ==--------------------------------------------------------------==
      WRITE(n_m,'(i0)')ia_m
      n_m=ADJUSTL(n_m)
      WRITE(n_o,'(I0)')ia_o
      n_o=ADJUSTL(n_o)
      filen = TRIM(atmA)//TRIM(n_m)//TRIM(atmB)//TRIM(n_o)//'.xyz'
      WRITE(stderr,'(/,A,A)')'Generating ',filen
      OPEN(UNIT=ixyz, FILE=filen, STATUS='unknown')
      WRITE(ixyz,'(I3)') natom
      WRITE(ixyz,'(A)') trim(atmA)//trim(n_m)//TRIM(atmB)//trim(n_o)
      DO iatom = 1, natom
        WRITE(ixyz,'(A,3F18.8)')at(iatom) &
     &  ,(tau(ii,iatom)*bohr,ii=1,3)
      END DO
      CLOSE(ixyz)
!     ==--------------------------------------------------------------==
!     == generate XSF file                                            ==
!     ==--------------------------------------------------------------==
      filen = TRIM(atmA)//TRIM(n_m)//TRIM(atmB)//TRIM(n_o)//'.xsf'
      WRITE(*,'(/,A,A)')'Generating ',filen
      OPEN(UNIT=ixyz, FILE=filen, STATUS='unknown')
      WRITE(ixyz,'(A)')' SLAB'
      WRITE(ixyz,'(A)')' PRIMVEC'
      WRITE(ixyz,'(3F15.6)')(a1_sc(ii)*bohr,ii=1,3)
      WRITE(ixyz,'(3F15.6)')(a2_sc(ii)*bohr,ii=1,3)
      WRITE(ixyz,'(3F15.6)')(a3_sc(ii)*bohr,ii=1,3)
      WRITE(ixyz,'(A)')' PRIMCOORD'
      idum = 1
      WRITE(ixyz,'(i5,1x,i1)')natom,idum
      DO iatom = 1, natom
        idum=zatom(TRIM(at(iatom)))
        WRITE(ixyz,'(I5,3(F15.9,2X))')idum &
     &  ,(tau(ii,iatom)*bohr,ii=1,3)
      END DO
      CLOSE(ixyz)
!     ==--------------------------------------------------------------==
      CLOSE(iout)
!     ==--------------------------------------------------------------==
      END program
!     ==================================================================

