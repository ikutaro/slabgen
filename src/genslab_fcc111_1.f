      program main
c     ==--------------------------------------------------------------==
c     == This is a utility program to generate an fcc (111)           ==
c     ==--------------------------------------------------------------==
      implicit none
      integer iatom,natom,matom
      parameter(matom=400)
      integer ii,ilayer,nlayer,nvaclayer,n1,n2
      double precision a0, a, bohr,rlaydist,rvac,z
      double precision a1(3),a2(3),a3(3),a1_sc(3),a2_sc(3),a3_sc(3)
      double precision tau(3,matom),taus(3,matom),cslb(3)
      character*2 atmnm
      character*3 num
      character*20 filen
      integer input,iout,stdin,stdout,stderr,ios,len
      character*80 infile,outfile
      logical fexist
c     ==--------------------------------------------------------------==
c     == set constants                                                ==
c     ==--------------------------------------------------------------==
      stdin =05
      stdout=06
      stderr=00
      input =10
      iout  =20
      bohr=0.529177
c     ==--------------------------------------------------------------==
      write(stdout,'(a,$)') 'Enter input file> '
      read(stdin,'(a)') infile
      write(stdout,'(a,$)') 'Enter output file> '
      read(stdin,'(a)') outfile
      inquire(file=infile,exist=fexist)
      if(.not.fexist)then
        len=index(infile,' ')
        write(stderr,'(/,a,a)') infile(:len-1),' does not exist.'
        stop
      endif
      open(input,file=infile,iostat=ios,status='old')
      if(ios.ne.0)then
        write(stderr,'(a)')
     &  '*** ERROR occurs while opening the input file.'
        stop
      endif
      open(iout,file=outfile,status='unknown')
c     ==--------------------------------------------------------------==
      write(iout,'(/,a,a,a,/,a,a,a/,a,a,a,/)')
     &' ***',
     &'***********************************************************',
     &'****',
     &' ***',
     &' This program generates an fcc (111) 1x1 clean surface     ',
     &' ***',
     &' ***',
     &'***********************************************************',
     &'****'
c     ==--------------------------------------------------------------==
      read(input,'(a)')atmnm
      read(input,*) a0
      read(input,*)nlayer
      read(input,*)nvaclayer
      read(input,*)n1,n2
c     ==--------------------------------------------------------------==
c     == set lattice vectors                                          ==
c     ==--------------------------------------------------------------==
      a=a0/sqrt(2.0d0)
      rlaydist=a0/sqrt(3.0d0)
c     rvac=rlaydist*dble(nvaclayer+1)
      rvac=rlaydist*dble(nvaclayer)
      a1(1)=0.50d0*a
      a1(2)=sqrt(3.0d0)/2.0d0*a
      a1(3)=0.0d0
      a2(1)=-a1(1)
      a2(2)=a1(2)
      a2(3)=0.0d0
      a3(1)=0.0d0
      a3(2)=0.0d0
      a3(3)=rlaydist*dble(nlayer)+rlaydist*dble(nvaclayer)
c     ==--------------------------------------------------------------==
c     == set atomic positions                                         ==
c     ==--------------------------------------------------------------==
      iatom=0
      natom=nlayer*n1*n2
      if(natom.gt.matom)then
        write(stderr,'(/,a)')
     &  ' *** ERROR: natom > matom'
        write(*,'(a,i5,a,i5)')' natom= ',natom,' matom= ',matom
        stop
      endif
      do ilayer=1,nlayer
        z=(-1.0d0)*dble(ilayer-1)*rlaydist
        if(mod(ilayer,3).eq.1)then
          iatom=iatom+1
          tau(1,iatom)=0.0d0
          tau(2,iatom)=0.0d0
          tau(3,iatom)=z
        elseif(mod(ilayer,3).eq.2)then
          iatom=iatom+1
          tau(1,iatom)=0.0d0
          tau(2,iatom)=+a/sqrt(3.0d0)
          tau(3,iatom)=z
        elseif(mod(ilayer,3).eq.0)then
          iatom=iatom+1
          tau(1,iatom)=0.0d0
          tau(2,iatom)=-a/sqrt(3.0d0)
          tau(3,iatom)=z
        endif
      enddo
c     ==--------------------------------------------------------------==
      write(iout,*)
      write(iout,'(a,f18.8)')' a_0= ',a0
      write(iout,'(a,f18.8)')' a  = ',a
      write(iout,'(/,a,i2,a)')' Slab of ',nlayer,' ML'
      write(iout,'(a,i2,a)')
     &' Vacuum region corresponds to ',nvaclayer,' ML'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac,' Bohr'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac*Bohr,
     &' Angstrom'
      write(iout,'(/,a)')      ' Lattice vectors:'
      write(iout,'(3f18.8)')(a1(ii),ii=1,3)
      write(iout,'(3f18.8)')(a2(ii),ii=1,3)
      write(iout,'(3f18.8)')(a3(ii),ii=1,3)
      write(iout,'(a,i5)')   ' Number of atoms:',natom
      write(iout,'(a)')      ' Atoms: '
      do iatom=1,natom
        write(iout,'(3f18.8)')(tau(ii,iatom),ii=1,3)
      enddo 
c     ==--------------------------------------------------------------==
c     == generate XYZ file                                            ==
c     ==--------------------------------------------------------------==
      if(natom.lt.10)then
        write(num,'(i1)')natom
        filen=atmnm//num(1:1)//'.xyz'
      elseif(natom.lt.100)then
        write(num,'(i2)')natom
        filen=atmnm//num(1:2)//'.xyz'
      elseif(natom.lt.1000)then
        write(num,'(i3)')natom
        filen=atmnm//num(1:3)//'.xyz'
      endif
      write(*,'(/,a,a)')'Generating ',filen
      open(iout,file=filen,status='unknown')
      write(iout,'(i3)') natom
      write(iout,'(a)') atmnm//num
      do iatom=1,natom
        write(iout,'(a,3f18.8)')atmnm
     &  ,(tau(ii,iatom)*bohr,ii=1,3)
      enddo
      close(iout)
c     ==--------------------------------------------------------------==
      stop
      end
