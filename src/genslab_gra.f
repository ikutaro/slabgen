c     ==================================================================
      program main
c     ==--------------------------------------------------------------==
c     == This is a utility program to generate a graphite surface     ==
c     ==--------------------------------------------------------------==
      implicit none
      integer iatom,natom,matom
      parameter(matom=400)
      integer ii,ilayer,nlayer,nvaclayer,i1,i2,n1,n2
      integer ivac
      double precision a_0, c_0, a, a_sf, bohr,rlaydist,rvac,z,cslb(3)
      double precision a1(3),a2(3),a3(3),a1_sc(3),a2_sc(3),a3_sc(3)
      double precision tau(3,matom),taus(3,matom),tau_tmp(3)
      double precision tau0_tmp(3,4)
      double precision vac
      character*2 atmnm
      character*3 num
      character*20 filen
      character*256 str
      integer input,iout,stdin,stdout,stderr, ios,len
      integer :: iatps
      character*80 infile,outfile
      logical fexist
c ... arguments
      integer :: iarg, iargc, narg, marg
      character(len=80) :: arg
c ... options
      integer :: ipw=0, istate=0
c ... others
      integer :: idum
c     ==--------------------------------------------------------------==
c     == set constants                                                ==
c     ==--------------------------------------------------------------==
      stdin  = 05
      stdout = 06
      stderr = 00
      input  = 10
      iout   = 20
      bohr = 0.529177
      num = '   '
c     ==--------------------------------------------------------------==
c ... arguments
      narg=iargc()
      if(narg.gt.0)then
        call getarg(1,arg)
        select case(trim(arg))
          case('-pw') 
            ipw=1
          case('-state')
            istate=1
        end select 
      endif
c     ==--------------------------------------------------------------==
      write(stdout,'(a,$)')'Enter input file> '
      read(stdin,'(a)')infile
      write(stdout,'(a,$)')'Enter output file> '
      read(stdin,'(a)')outfile
      len=index(infile,' ')
      write(stdout,'(/,/,a,a)')'Input file : ',infile(:len-1)
      len=index(outfile,' ')
      write(stdout,'(a,a)')    'Output file: ',outfile(:len-1) 
      inquire(file=infile,exist=fexist)
      if(.not.fexist)then
        len=index(infile,' ')
        write(stderr,'(/,a,a)') infile(:len-1),' does not exist.'
        stop
      endif
      open(input,file=infile,iostat=ios,status='old')
      if(ios.ne.0)then
        write(stderr,'(a)')
     &  '*** ERROR occurs while opening the input file.'
        stop
      endif
      open(iout,file=outfile,status='unknown')
c     ==--------------------------------------------------------------==
      write(iout,'(/,a,a,a,/,a,a,a,/,a,a,a,/)')
     &' ***',
     &'***********************************************************',
     &'****',
     &' ***',
     &' This program generates a graphite surface                 ',
     &' ***',
     &' ***',
     &'***********************************************************',
     &'****'
c     ==--------------------------------------------------------------==
      read(input,'(a)') atmnm
      read(input,*) a_0, c_0
      read(input,*) nlayer
      read(input,*) nvaclayer
      read(input,*) n1, n2
      a_0 = dble(a_0)
      c_0 = dble(c_0)
      atmnm = 'C'
c     ==--------------------------------------------------------------==
c     == set lattice vectors                                          ==
c     ==--------------------------------------------------------------==
      do ii = 1, 3
        a1(ii) = 0.0d0
        a2(ii) = 0.0d0
        a3(ii) = 0.0d0
      enddo
      a = a_0
      a_sf = a_0
      rlaydist = c_0 * 0.5d0
      rvac = c_0
      a1(1) = a
      a1(2) = 0.0d0
      a2(1) = -0.5d0 * a
      a2(2) =  0.5d0 * sqrt(3.d0) * a
      a3(3) = rlaydist * dble(nlayer) + rlaydist * dble(nvaclayer)
c     center of slab (cslb)
      cslb(1) = 0.0d0
      cslb(2) = 0.0d0
      cslb(3) = -rlaydist * dble(nlayer-1) / 2.0d0 
c     ==--------------------------------------------------------------==
      natom=nlayer*2
      iatom=0
      do ilayer=1,nlayer
        z=(-1.0d0)*dble(ilayer-1)*rlaydist
        tau0_tmp(1,1)= 0.0d0
        tau0_tmp(2,1)= 0.0d0
        tau0_tmp(3,1)= z
        tau0_tmp(1,2)= 1.d0/3.d0
        tau0_tmp(2,2)= 2.d0/3.d0
        tau0_tmp(3,2)= z
        tau0_tmp(1,3)= 0.00d0
        tau0_tmp(2,3)= 0.00d0
        tau0_tmp(3,3)= z
        tau0_tmp(1,4)= 2.d0/3.d0
        tau0_tmp(2,4)= 1.d0/3.d0
        tau0_tmp(3,4)= z
        if(mod(ilayer,2).eq.1)then
          iatom=iatom+1
          tau(1,iatom) = tau0_tmp(1,1) * a1(1) + tau0_tmp(2,1) * a2(1)
          tau(2,iatom) = tau0_tmp(1,1) * a1(2) + tau0_tmp(2,1) * a2(2)
          tau(3,iatom) = tau0_tmp(3,1)
          iatom=iatom+1
          tau(1,iatom) = tau0_tmp(1,2) * a1(1) + tau0_tmp(2,2) * a2(1)
          tau(2,iatom) = tau0_tmp(1,2) * a1(2) + tau0_tmp(2,2) * a2(2)
          tau(3,iatom) = tau0_tmp(3,2)
        elseif(mod(ilayer,2).eq.0)then
          iatom=iatom+1
          tau(1,iatom) = tau0_tmp(1,3) * a1(1) + tau0_tmp(2,3) * a2(1)
          tau(2,iatom) = tau0_tmp(1,3) * a1(2) + tau0_tmp(2,3) * a2(2)
          tau(3,iatom) = tau0_tmp(3,3)
          iatom=iatom+1
          tau(1,iatom) = tau0_tmp(1,4) * a1(1) + tau0_tmp(2,4) * a2(1)
          tau(2,iatom) = tau0_tmp(1,4) * a1(2) + tau0_tmp(2,4) * a2(2)
          tau(3,iatom) = tau0_tmp(3,4)
        endif
      enddo
c     ==--------------------------------------------------------------==
      write(iout,*)
      write(iout,'(a,f18.8)')' a_0= ',a_0
      write(iout,'(a,f18.8)')' c_0= ',c_0
      write(iout,'(a,f18.8)')' Inter layer distance: ',rlaydist
      write(iout,'(/,a,i2,a)')' Slab of ',nlayer,' ML'
      write(iout,'(a,i2,a)')
     &' Vacuum region corresponds to ',nvaclayer,' ML'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac,' Bohr'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac*Bohr
     &,' Angstrom'
      write(iout,'(a,f6.2,a)')
     &' Size of the supercell along the z axis: '
     &,abs(a3(3)),' Bohr'
      write(iout,'(a,f6.2,a)')
     &' Size of the supercell along the z axis: '
     &,abs(a3(3))*Bohr,' Angstrom'
      write(iout,'(/,a)')      ' Primitive lattice vectors:'
      write(iout,'(3f20.12)')(a1(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3(ii),ii=1,3)
      write(iout,'(/,a)')      ' Supercell lattice vectors:'
      write(iout,'(3f20.12)')(a1_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3_sc(ii),ii=1,3)
      write(iout,'(a,i5)')   ' Number of atoms:',natom
      write(iout,'(a)')      ' Atoms: '
      do iatom=1,natom
        write(iout,'(3f20.12,3i5)')(tau(ii,iatom),ii=1,3),1,1,1
      enddo 
      write(iout,'(/,a)')
     &' Atoms (center of the slab is set to the origin):'
      do iatom=1,natom
        write(iout,'(3f20.12,3i5)')(tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
      enddo 
      if(ipw.eq.1)then
c ... PW output
        filen='atps'
        open(iatps,file=filen,status='unknown')
        write(iatps,'(a)')       ' ibrav = 4'
        write(iatps,'(a,i3)')    ' nat   =',natom
        write(iatps,'(a,i1)')    ' ntyp  =',1
        write(iatps,'(a,f20.12)')' celldm(1) = ',a1_sc(1)
        write(iatps,'(a,f20.12)')' celldm(3) = ',a3_sc(3)/a1_sc(1)
        write(iatps,'(a)')'CELL_PARAMETERS (bohr)'
        write(iatps,'(3f20.12)')(a1_sc(ii),ii=1,3)
        write(iatps,'(3f20.12)')(a2_sc(ii),ii=1,3)
        write(iatps,'(3f20.12)')(a3_sc(ii),ii=1,3)
        write(iatps,'(a)')'ATOMIC_POSITIONS (bohr)'
        do iatom=1,natom
          write(iatps,'(a,3f20.12,3i5)')
     &    trim(atmnm),(tau(ii,iatom),ii=1,3),1,1,1
        enddo 
        close(iatps) 
c ... PW output (Angstrom)
        filen='atps_angstrom'
        open(iatps,file=filen,status='unknown')
        write(iatps,'(a)')       ' ibrav = 4'
        write(iatps,'(a,i3)')    ' nat   =',natom
        write(iatps,'(a,i1)')    ' ntyp  =',1
        write(iatps,'(a,f20.12)')' A = ',a1_sc(1)*bohr
        write(iatps,'(a,f20.12)')' C = ',a3_sc(3)*bohr
        write(iatps,'(a)')'CELL_PARAMETERS (angstrom)'
        write(iatps,'(3f20.12)')(a1_sc(ii)*bohr,ii=1,3)
        write(iatps,'(3f20.12)')(a2_sc(ii)*bohr,ii=1,3)
        write(iatps,'(3f20.12)')(a3_sc(ii)*bohr,ii=1,3)
        write(iatps,'(a)')'ATOMIC_POSITIONS (angstrom)'
        do iatom=1,natom
          write(iatps,'(a,3f20.12,3i5)')
     &    trim(atmnm),(tau(ii,iatom)*bohr,ii=1,3),1,1,1
        enddo 
        close(iatps) 
      endif
c     ==--------------------------------------------------------------==
      stop
      end
c     ==================================================================
