      program main
!     ==--------------------------------------------------------------==
!     == This is a utility program to generate an fcc (111) (r13xr13) ==
!     ==--------------------------------------------------------------==
      implicit none
      integer :: iatom,natom
      integer, parameter :: matom = 400
      integer :: ii, jj, ilayer,nlayer,nvaclayer,i1,i2,n1,n2, m1, m2, &
     &           n1m, n1p, n2m, n2p
      real(kind=8) :: a_0, a, a_sf,bohr,rlaydist,rvac,z,area,vol
      real(kind=8) :: a1(3),a2(3),a3(3),a1_sc(3),a2_sc(3),a3_sc(3)
      real(kind=8) :: tau(3,matom),taus(3,matom),tau0_tmp(3,matom), &
     &                cslb(3)
      real(kind=8) :: tau_new(3,matom)
      character(len=2) :: atmnm
      character(len=3) :: num
      character(len=20) filen
      integer :: input,iout,ixyz,iatps,stdin,stdout,stderr,ios,len
      character(len=80) :: infile,outfile
      logical :: fexist
      real(kind=8) :: sum
      real(kind=8) :: xmat(3,3),xmati(3,3)
      real(kind=8) :: ab(3),bc(3),ca(3)
      integer :: zatom
! ... arguments
      integer :: iarg, iargc, narg, marg
      character(len=80) :: arg
! ... options
      integer :: ipw = 0, istate = 0
! ... others
      integer :: idum
!     ==--------------------------------------------------------------==
!     == set constants                                                ==
!     ==--------------------------------------------------------------==
      stdin = 05
      stdout= 06
      stderr= 00
      input = 10
      iout  = 20
      ixyz  = 25
      iatps = 26
      bohr = 0.529177210903D0
!     ==--------------------------------------------------------------==
! ... arguments
      narg=iargc()
      if(narg > 0)then
        call getarg(1,arg)
        select case(trim(arg))
          case('-pw') 
            ipw=1
          case('-state')
            istate=1
        end select 
      endif
!     ==--------------------------------------------------------------==
      write(stdout,'(a,$)') 'Enter input file> '
      read(stdin,'(a)') infile
      len=index(infile,' ')
      write(stdout,'(/,a,a,a)')'Input file is ',infile(:len-1),'.'
      write(stdout,'(a,$)') 'Enter output file> '
      read(stdin,'(a)') outfile
      len=index(outfile,' ')
      write(stdout,'(/,a,a,a)')'Output file is ',outfile(:len-1),'.'
      inquire(file=infile,exist=fexist)
      if(.not. fexist)then
        len=index(infile,' ')
        write(stderr,'(/,a,a)') infile(:len-1),' does not exist.'
        stop
      endif
      open(input,file=infile,iostat=ios,status='old')
      if(ios /= 0)then
        write(stderr,'(a)') &
     &  '*** ERROR occurs while opening the input file.'
        stop
      endif
      open(iout,file=outfile,status='unknown')
!     ==--------------------------------------------------------------==
      write(iout,'(/,a,a,a,/,a,a,a/,a,a,a,/)') &
     &' ***', &
     &'***********************************************************', &
     &'****', &
     &' ***', &
     &' This program generates an fcc (111) (r13xr13) surface     ', &
     &' ***', &
     &' ***', &
     &'***********************************************************', &
     &'****'
!     ==--------------------------------------------------------------==
      read(input,'(a)') atmnm
      read(input,*) a_0
      read(input,*) nlayer
      read(input,*) nvaclayer
      read(input,*) n1, n2
!     ==--------------------------------------------------------------==
!     == set lattice vectors                                          ==
!     ==--------------------------------------------------------------==
      a_sf = a_0 / sqrt(2.0d0)
      rlaydist = a_0 / sqrt(3.0d0)
      rvac = rlaydist*dble(nvaclayer+1)
!     rvac = rlaydist*dble(nvaclayer)
      a1(1) =  3.5d0 * a_sf
      a1(2) =  0.5d0 * sqrt(3.0d0) * a_sf
      a1(3) =  0.0d0
      a2(1) = -2.5d0 * a_sf
      a2(2) =  1.5d0 * sqrt(3.0d0) * a_sf
      a2(3) =  0.0d0
      a3(1) =  0.0d0
      a3(2) =  0.0d0
      a3(3) =  rlaydist*dble(nlayer) + rlaydist*dble(nvaclayer)
      do ii = 1, 2
        a1_sc(ii) = dble(n1) * a1(ii)
        a2_sc(ii) = dble(n2) * a2(ii)
        a3_sc(ii) = a3(ii)
      enddo
      a1_sc(3) = a1(3)
      a2_sc(3) = a2(3)
      a3_sc(3) = a3(3)
      ab(1) = a1_sc(2) * a2_sc(3) - a1_sc(3) * a2_sc(2)
      ab(2) = a1_sc(3) * a2_sc(1) - a1_sc(1) * a2_sc(3)
      ab(3) = a1_sc(1) * a2_sc(2) - a1_sc(2) * a2_sc(1)
      area = 0.0d0
      do ii = 1, 3
        area = area + ab(ii) * ab(ii)
      enddo
      area = abs(area)
      area = sqrt(area)
      vol = 0.0d0
      do ii = 1, 3
        vol = vol + ab(ii) * a3_sc(ii)
      enddo
      vol = abs(vol)
      cslb(1) = 0.d0
      cslb(2) = 0.d0
      cslb(3) = -rlaydist * dble(nlayer-1) * 0.5d0
      if(n1 > 1)then
        if (int(n1/2)*2 == n1) then
          n1m = -int(n1/2)
          n1p =  int(n1/2) - 1
        else
          n1m = -int(n1/2)
          n1p =  int(n1/2)
        end if
      else
        n1m = 1
        n1p = n1
      end if
      if (n2 > 1) then
        if (int(n2/2)*2 == n2) then
          n2m = -int(n2/2)
          n2p =  int(n2/2) - 1
        else
          n2m = -int(n2/2)
          n2p =  int(n2/2)
        end if
      else
        n2m = 1
        n2p = n2
      end if
!     ==--------------------------------------------------------------==
!     == set atomic positions                                         ==
!     ==--------------------------------------------------------------==
      natom = 13 * nlayer * n1 * n2
      if (natom > matom) then
        write(stderr,'(/,a)') &
     &  ' *** ERROR: natom > matom'
        write(*,'(a,i5,a,i5)')' natom= ',natom,' matom= ',matom
        stop
      end if
      iatom = 0
      do ilayer = 1, nlayer
!
        z = (-1.0d0) * dble(ilayer-1) * rlaydist
!
        tau0_tmp(1, 1) =  0.d0;         tau0_tmp(2, 1) = 0.d0
!
        tau0_tmp(1, 2) = -0.5d0 * a_sf; tau0_tmp(2, 2) = 0.5d0 * sqrt(3.d0) * a_sf
        tau0_tmp(1, 3) =  0.5d0 * a_sf; tau0_tmp(2, 3) = 0.5d0 * sqrt(3.d0) * a_sf
        tau0_tmp(1, 4) =  1.5d0 * a_sf; tau0_tmp(2, 4) = 0.5d0 * sqrt(3.d0) * a_sf
        tau0_tmp(1, 5) =  2.5d0 * a_sf; tau0_tmp(2, 5) = 0.5d0 * sqrt(3.d0) * a_sf
!
        tau0_tmp(1, 6) = -1.0d0 * a_sf; tau0_tmp(2, 6) = sqrt(3.d0) * a_sf
        tau0_tmp(1, 7) =  0.0d0       ; tau0_tmp(2, 7) = sqrt(3.d0) * a_sf
        tau0_tmp(1, 8) =  1.0d0 * a_sf; tau0_tmp(2, 8) = sqrt(3.d0) * a_sf
        tau0_tmp(1, 9) =  2.0d0 * a_sf; tau0_tmp(2, 9) = sqrt(3.d0) * a_sf
!
        tau0_tmp(1,10) = -1.5d0 * a_sf; tau0_tmp(2,10) = 1.5d0 * sqrt(3.d0) * a_sf
        tau0_tmp(1,11) = -0.5d0 * a_sf; tau0_tmp(2,11) = 1.5d0 * sqrt(3.d0) * a_sf
        tau0_tmp(1,12) =  0.5d0 * a_sf; tau0_tmp(2,12) = 1.5d0 * sqrt(3.d0) * a_sf
        tau0_tmp(1,13) =  1.5d0 * a_sf; tau0_tmp(2,13) = 1.5d0 * sqrt(3.d0) * a_sf
!
!       tau0_tmp(1, 1) =  0.d0;         tau0_tmp(2, 1) = 0.d0
!       tau0_tmp(1, 2) =  a_sf;         tau0_tmp(2, 2) = 0.d0
!       tau0_tmp(1, 3) = -a_sf;         tau0_tmp(2, 3) = 0.d0
!       tau0_tmp(1, 4) =  2.d0 * a_sf;  tau0_tmp(2, 4) = 0.d0
!       tau0_tmp(1, 5) = -2.d0 * a_sf;  tau0_tmp(2, 5) = 0.d0
!       tau0_tmp(1, 6) =  0.5d0 * a_sf; tau0_tmp(2, 6) = 0.5d0 * sqrt(3.d0) * a_sf
!       tau0_tmp(1, 7) = -0.5d0 * a_sf; tau0_tmp(2, 7) = 0.5d0 * sqrt(3.d0) * a_sf
!       tau0_tmp(1, 8) =  1.5d0 * a_sf; tau0_tmp(2, 8) = 0.5d0 * sqrt(3.d0) * a_sf
!       tau0_tmp(1, 9) = -1.5d0 * a_sf; tau0_tmp(2, 9) = 0.5d0 * sqrt(3.d0) * a_sf
!       tau0_tmp(1,10) =  0.5d0 * a_sf; tau0_tmp(2,10) = -0.5d0 * sqrt(3.d0) * a_sf
!       tau0_tmp(1,11) = -0.5d0 * a_sf; tau0_tmp(2,11) = -0.5d0 * sqrt(3.d0) * a_sf
!       tau0_tmp(1,12) =  1.5d0 * a_sf; tau0_tmp(2,12) = -0.5d0 * sqrt(3.d0) * a_sf
!       tau0_tmp(1,13) = -1.5d0 * a_sf; tau0_tmp(2,13) = -0.5d0 * sqrt(3.d0) * a_sf
!
        tau0_tmp(3,1:13) = z
        if (mod(ilayer,3) == 1) then
          do i2 = n2m, n2p
            do i1 = n1m, n1p
              do ii = 1, 13
                iatom = iatom + 1
                tau(1,iatom) = tau0_tmp(1,ii)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
                tau(2,iatom) = tau0_tmp(2,ii)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
                tau(3,iatom) = tau0_tmp(3,ii)
              end do
            end do
          end do
        else if (mod(ilayer,3) == 2) then
          do ii = 1, 13
            tau0_tmp(2,ii) = tau0_tmp(2,ii) + a_sf / sqrt(3.0d0)
          end do
          do i2 = n2m, n2p
            do i1 = n1m, n1p
              do ii = 1, 13
                iatom = iatom + 1
                tau(1,iatom) = tau0_tmp(1,ii)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
                tau(2,iatom) = tau0_tmp(2,ii)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
                tau(3,iatom) = tau0_tmp(3,ii)
              end do
            end do
          end do
        else if (mod(ilayer,3) == 0) then
          do ii = 1, 13
            tau0_tmp(2,ii) = tau0_tmp(2,ii) - a_sf / sqrt(3.0d0)
          end do
          do i2 = n2m, n2p
            do i1 = n1m, n1p
              do ii = 1, 13
                iatom = iatom + 1
                tau(1,iatom) = tau0_tmp(1,ii)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
                tau(2,iatom) = tau0_tmp(2,ii)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
                tau(3,iatom) = tau0_tmp(3,ii)
              end do
            end do
          end do
        end if
      end do
      if (iatom /= natom) then
        write(stderr,'(a,a,i0,a,i0)')&
     &  '*** ERROR: # of atoms inconsistent.',&
     &  ' iatom=',iatom,' natom=',natom
        stop
      end if
!     ==--------------------------------------------------------------==
      do ii = 1, 3
        xmat(ii,1) = a1_sc(ii)
        xmat(ii,2) = a2_sc(ii)
        xmat(ii,3) = a3_sc(ii)
      enddo
      call matinv(xmat,xmati,sum)
      do iatom = 1, natom
        do ii = 1, 3
          sum = 0.0d0
          do jj = 1, 3
            sum = sum + xmati(ii,jj) * tau(jj,iatom)
          enddo
          taus(ii,iatom) = sum
        enddo
      enddo
!     ==--------------------------------------------------------------==
      write(iout,*)
      write(iout,'(/,a,f18.8)')' a_0= ',a_0
      write(iout,'(a,f18.8)')  ' a  = ',a_sf
      write(iout,'(/,a,i2,a)')' Slab of ',nlayer,' ML'
      write(iout,'(a,i2,a)') &
     &' Vacuum region corresponds to ',nvaclayer,' ML'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac,' Bohr'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac*Bohr, &
     &' Angstrom'
      write(iout,*)
      write(iout,'(a,f18.8)')' Inter-layer distance = ',rlaydist
      write(iout,'(a,f18.8)')' Slab-slab distance   = ',rvac
      write(iout,'(/,a)')      ' Lattice vectors:'
      write(iout,'(3f20.12)')(a1(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3(ii),ii=1,3)
      write(iout,'(/,a)')      ' Supercell Lattice vectors:'
      write(iout,'(3f20.12)')(a1_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3_sc(ii),ii=1,3)
      write(iout,'(/,a,f20.12)')' Surface area: ',area
      write(iout,'(a,f20.12)')' Surface area: ',vol/a3_sc(3)
      write(iout,'(/,a,f20.12)')' Volume of unit cell: ',vol
      write(iout,'(/,a,i5)')   ' Number of atoms:',natom
      write(iout,'(a)')      ' Atoms: '
      do iatom = 1, natom
        write(iout,'(3f20.12,3i5)')(tau(ii,iatom),ii=1,3),1,1,1
      enddo 
      write(iout,'(/,a)')' Atomic coordinates in reduced coordinate '
      do iatom = 1, natom
        write(iout,'(3f20.12,3i5)')(taus(ii,iatom),ii=1,3),1,1,1
      enddo 
      write(iout,'(/,a)') &
     &' Atoms (center of the slab is set to the origin):' 
      do iatom = 1, natom
        write(iout,'(3f20.12,3i5)')(tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
      enddo 
      if(ipw == 1)then
! ... PW output
        filen='atps'
        open(iatps,file=filen,status='unknown')
        write(iatps,'(a)')       ' ibrav = 0'
        write(iatps,'(a,i3)')    ' nat   =',natom
        write(iatps,'(a,i1)')    ' ntyp  =',1
        !write(iatps,'(a,f20.12)')' celldm(1) = ',a1_sc(1)
        !write(iatps,'(a,f20.12)')' celldm(3) = ',a3_sc(3)/a1_sc(1)
        write(iatps,'(a)')'CELL_PARAMETERS (bohr)'
        write(iatps,'(3f20.12)')(a1_sc(ii),ii=1,3)
        write(iatps,'(3f20.12)')(a2_sc(ii),ii=1,3)
        write(iatps,'(3f20.12)')(a3_sc(ii),ii=1,3)
        write(iatps,'(a)')'ATOMIC_POSITIONS (bohr)'
        do iatom = 1, natom
          write(iatps,'(a,3f20.12,3i5)') &
     &    trim(atmnm),(tau(ii,iatom),ii=1,3),1,1,1
        enddo 
        write(iatps,'(a)')''
        write(iatps,'(a)')'# Atomic positions centered at z=0'
        write(iatps,'(a)')'ATOMIC_POSITIONS (bohr)'
        do iatom = 1, natom
          write(iatps,'(a,3f20.12,3i5)') &
     &    trim(atmnm),(tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
        enddo 
        close(iatps) 
! ... PW output in Angstrom
        filen='atps_angstrom'
        open(iatps,file=filen,status='unknown')
        write(iatps,'(a)')       ' ibrav = 0'
        write(iatps,'(a,i3)')    ' nat   =',natom
        write(iatps,'(a,i1)')    ' ntyp  =',1
        !write(iatps,'(a,f20.12)')' A = ',a1_sc(1)*bohr
        !write(iatps,'(a,f20.12)')' C = ',a3_sc(3)*bohr
        write(iatps,'(a)')'CELL_PARAMETERS (angstrom)'
        write(iatps,'(3f20.12)')(a1_sc(ii)*bohr,ii=1,3)
        write(iatps,'(3f20.12)')(a2_sc(ii)*bohr,ii=1,3)
        write(iatps,'(3f20.12)')(a3_sc(ii)*bohr,ii=1,3)
        write(iatps,'(a)')'ATOMIC_POSITIONS (angstrom)'
        do iatom = 1, natom
          write(iatps,'(a,3f20.12,3i5)') &
     &    trim(atmnm),(tau(ii,iatom)*bohr,ii=1,3),1,1,1
        enddo 
        write(iatps,'(a)')''
        write(iatps,'(a)')'# Atomic positions centered at z=0'
        write(iatps,'(a)')'ATOMIC_POSITIONS (angstrom)'
        do iatom=1,natom
          write(iatps,'(a,3f20.12,3i5)') &
     &    trim(atmnm),((tau(ii,iatom)-cslb(ii))*bohr,ii=1,3),1,1,1
        enddo 
        close(iatps) 
      endif
!     ==--------------------------------------------------------------==
!     == generate XYZ file                                            ==
!     ==--------------------------------------------------------------==
      write(num,'(i0)')natom
      num=adjustl(num)
      filen = trim(atmnm)//trim(num)//'.xyz'
!     if(natom < 10)then
!       write(num,'(i1)')natom
!       filen=atmnm//num(1:1)//'.xyz'
!     elseif(natom < 100)then
!       write(num,'(i2)')natom
!       filen=atmnm//num(1:2)//'.xyz'
!     elseif(natom < 1000)then
!       write(num,'(i3)')natom
!       filen=atmnm//num(1:3)//'.xyz'
!     endif
      write(*,'(/,a,a)')'Generating ',filen
      open(ixyz,file=filen,status='unknown')
      write(ixyz,'(i3)') natom
      write(ixyz,'(a)') atmnm//num
      do iatom=1,natom
        write(ixyz,'(a,3f18.8)')atmnm &
     &  ,(tau(ii,iatom)*bohr,ii=1,3)
      enddo
      close(ixyz)
!     ==--------------------------------------------------------------==
!     == generate XSF file                                            ==
!     ==--------------------------------------------------------------==
      write(num,'(i0)')natom
      num=adjustl(num)
      filen = trim(atmnm)//trim(num)//'.xsf'
!     if(natom.lt.10)then
!       write(num,'(i1)')natom
!       filen=atmnm//num(1:1)//'.xsf'
!     elseif(natom.lt.100)then
!       write(num,'(i2)')natom
!       filen=atmnm//num(1:2)//'.xsf'
!     elseif(natom.lt.1000)then
!       write(num,'(i3)')natom
!       filen=atmnm//num(1:3)//'.xsf'
!     endif
      write(*,'(/,a,a)')'Generating ',filen
      open(ixyz,file=filen,status='unknown')
      write(ixyz,'(a)')' SLAB'
      write(ixyz,'(a)')' PRIMVEC'
      write(ixyz,'(3f15.6)')(a1_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(3f15.6)')(a2_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(3f15.6)')(a3_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(a)')' PRIMCOORD'
      idum = 1
      write(ixyz,'(i5,i1)')natom,idum
      do iatom = 1, natom
        idum=zatom(trim(atmnm))
        write(ixyz,'(i5,3(f15.9,2x))')idum &
     &  ,(tau(ii,iatom)*bohr,ii=1,3)
      enddo
      close(ixyz)
!     ==--------------------------------------------------------------==
      close(iout)
!     ==--------------------------------------------------------------==
      end program

