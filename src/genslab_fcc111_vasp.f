      program main
c     ==--------------------------------------------------------------==
c     == This is a utility program to generate an fcc (111)           ==
c     ==--------------------------------------------------------------==
      implicit none
      integer iatom,natom,matom
      parameter(matom=400)
      integer ii,jj,ilayer,nlayer,nvaclayer,i1,i2,n1,n2
      double precision a_0, a, a_sf,bohr,rlaydist,rvac,z,area,vol
      double precision a1(3),a2(3),a3(3),a1_sc(3),a2_sc(3),a3_sc(3)
      double precision tau(3,matom),taus(3,matom),tau0_tmp(3),cslb(3)
      double precision tau_new(3,matom),tau_tmp(3)
      character*2 atmnm
      character*3 num
      character*20 filen
      integer input,iout,ixyz,ipos,stdin,stdout,stderr,ios,len
      character*80 infile,outfile
      logical fexist
      double precision sum
      double precision xmat(3,3),xmati(3,3)
      double precision ab(3),bc(3),ca(3)
c     ==--------------------------------------------------------------==
c     == set constants                                                ==
c     ==--------------------------------------------------------------==
      stdin =05
      stdout=06
      stderr=00
      input =10
      iout  =20
      ixyz  =25
      ipos  =25
      bohr=0.529177d0
c     ==--------------------------------------------------------------==
      write(stdout,'(a,$)') 'Enter input file> '
      read(stdin,'(a)') infile
      len=index(infile,' ')
      write(stdout,'(/,a,a,a)')'Input file is ',infile(:len-1),'.'
      write(stdout,'(a,$)') 'Enter output file> '
      read(stdin,'(a)') outfile
      len=index(outfile,' ')
      write(stdout,'(/,a,a,a)')'Output file is ',outfile(:len-1),'.'
      inquire(file=infile,exist=fexist)
      if(.not.fexist)then
        len=index(infile,' ')
        write(stderr,'(/,a,a)') infile(:len-1),' does not exist.'
        stop
      endif
      open(input,file=infile,iostat=ios,status='old')
      if(ios.ne.0)then
        write(stderr,'(a)')
     &  '*** ERROR occurs while opening the input file.'
        stop
      endif
      open(iout,file=outfile,status='unknown')
c     ==--------------------------------------------------------------==
      write(iout,'(/,a,a,a,/,a,a,a/,a,a,a,/)')
     &' ***',
     &'***********************************************************',
     &'****',
     &' ***',
     &' This program generates an fcc (111) 1x1 clean surface     ',
     &' ***',
     &' ***',
     &'***********************************************************',
     &'****'
c     ==--------------------------------------------------------------==
      read(input,'(a)')atmnm
      read(input,*) a_0
      read(input,*)nlayer
      read(input,*)nvaclayer
      read(input,*)n1,n2
      a_0=dble(a_0)
c     ==--------------------------------------------------------------==
c     == set lattice vectors                                          ==
c     ==--------------------------------------------------------------==
      a_sf=a_0/sqrt(2.0d0)
      rlaydist=a_0/sqrt(3.0d0)
c     rvac=rlaydist*dble(nvaclayer+1)
      rvac=rlaydist*dble(nvaclayer)
      a1(1)=a_sf
      a1(2)=0.0d0
      a1(3)=0.0d0
      a2(1)=0.50d0*a_sf
      a2(2)=0.50d0*sqrt(3.0d0)*a_sf
      a2(3)=0.0d0
      a3(1)=0.0d0
      a3(2)=0.0d0
      a3(3)=rlaydist*dble(nlayer)+rlaydist*dble(nvaclayer)
      do ii=1,2
        a1_sc(ii)=dble(n1)*a1(ii)
        a2_sc(ii)=dble(n2)*a2(ii)
        a3_sc(ii)=a3(ii)
      enddo
      a1_sc(3)=a1(3)
      a2_sc(3)=a2(3)
      a3_sc(3)=a3(3)
      ab(1)=a1_sc(2)*a2_sc(3)-a1_sc(3)*a2_sc(2)
      ab(2)=a1_sc(3)*a2_sc(1)-a1_sc(1)*a2_sc(3)
      ab(3)=a1_sc(1)*a2_sc(2)-a1_sc(2)*a2_sc(1)
      area=0.0d0
      do ii=1,3
        area=area+ab(ii)*ab(ii)
      enddo
      area=abs(area)
      area=sqrt(area)
      vol=0.0d0
      do ii=1,3
        vol=vol+ab(ii)*a3_sc(ii)
      enddo
      vol=abs(vol)
      cslb(1)=0.d0
      cslb(2)=0.d0
      cslb(3)=-rlaydist*dble(nlayer-1)*0.5d0
c     ==--------------------------------------------------------------==
c     == set atomic positions                                         ==
c     ==--------------------------------------------------------------==
      iatom=0
      natom=nlayer*n1*n2
      if(natom.gt.matom)then
        write(stderr,'(/,a)')
     &  ' *** ERROR: natom > matom'
        write(*,'(a,i5,a,i5)')' natom= ',natom,' matom= ',matom
        stop
      endif
      do ilayer=1,nlayer
        z=(-1.0d0)*dble(ilayer-1)*rlaydist
        if(mod(ilayer,3).eq.1)then
          tau0_tmp(1)=0.0d0
          tau0_tmp(2)=0.0d0
          tau0_tmp(3)=z
          do i2=1,n2
            do i1=1,n1
              iatom=iatom+1
              tau(1,iatom)=tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
              tau(2,iatom)=tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
              tau(3,iatom)=tau0_tmp(3)
            enddo
          enddo
        elseif(mod(ilayer,3).eq.2)then
          tau0_tmp(1)=0.0d0
          tau0_tmp(2)=a_sf/sqrt(3.0d0)
          tau0_tmp(3)=z
          do i2=1,n2
            do i1=1,n1
              iatom=iatom+1
              tau(1,iatom)=tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
              tau(2,iatom)=tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
              tau(3,iatom)=tau0_tmp(3)
            enddo
          enddo
        elseif(mod(ilayer,3).eq.0)then
          tau0_tmp(1)=0.0d0
          tau0_tmp(2)=-a_sf/sqrt(3.0d0)
          tau0_tmp(3)=z
          do i2=1,n2
            do i1=1,n1
              iatom=iatom+1
              tau(1,iatom)=tau0_tmp(1)+dble(i1-1)*a1(1)+dble(i2-1)*a2(1)
              tau(2,iatom)=tau0_tmp(2)+dble(i1-1)*a1(2)+dble(i2-1)*a2(2)
              tau(3,iatom)=tau0_tmp(3)
            enddo
          enddo
        endif
      enddo
c     ==--------------------------------------------------------------==
      do ii=1,3
        xmat(ii,1)=a1(ii)
        xmat(ii,2)=a2(ii)
        xmat(ii,3)=a3(ii)
      enddo
      call matinv(xmat,xmati,sum)
      do iatom=1,natom
        do ii=1,3
          sum=0.0d0
          do jj=1,3
            sum=sum+xmati(ii,jj)*tau(jj,iatom)
          enddo
          taus(ii,iatom)=sum
        enddo
      enddo
c     ==--------------------------------------------------------------==
      write(iout,*)
      write(iout,'(a,f18.8)')' a_0= ',a_0
      write(iout,'(a,f18.8)')' a  = ',a_sf
      write(iout,'(/,a,i2,a)')' Slab of ',nlayer,' ML'
      write(iout,'(a,i2,a)')
     &' Vacuum region corresponds to ',nvaclayer,' ML'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac,' Bohr'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac*Bohr,
     &' Angstrom'
      write(iout,*)
      write(iout,'(a,f18.8)')' Inter-layer distance = ',rlaydist
      write(iout,'(a,f18.8)')' Slab-slab distance   = ',rvac
      write(iout,*)
      write(iout,'(/,a)')      ' Lattice vectors:'
      write(iout,'(3f20.12)')(a1(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3(ii),ii=1,3)
      write(iout,'(/,a)')      ' Supercell Lattice vectors:'
      write(iout,'(3f20.12)')(a1_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3_sc(ii),ii=1,3)
      write(iout,'(/,a,f20.12)')' Surface area: ',area
      write(iout,'(a,f20.12)')' Surface area: ',vol/a3_sc(3)
      write(iout,'(/,a,f20.12)')' Volume of unit cell: ',vol
      write(iout,'(/,a,i5)')   ' Number of atoms:',natom
      write(iout,'(a)')      ' Atoms: '
      do iatom=1,natom
        write(iout,'(3f20.12)')(tau(ii,iatom),ii=1,3)
      enddo 
      write(iout,'(/,a)')' Atomic coordinates in reduced coordinate '
      do iatom=1,natom
        write(iout,'(3f24.12)')(taus(ii,iatom),ii=1,3)
      enddo 
      write(iout,'(/,a)')
     &' Atoms (center of the slab is set to the origin):' 
      do iatom=1,natom
        write(iout,'(3f24.12)')(tau(ii,iatom)-cslb(ii),ii=1,3)
      enddo 
c     ==--------------------------------------------------------------==
c     == generate XYZ file                                            ==
c     ==--------------------------------------------------------------==
c     if(natom.lt.10)then
c       write(num,'(i1)')natom
c       filen=atmnm//num(1:1)//'.xyz'
c     elseif(natom.lt.100)then
c       write(num,'(i2)')natom
c       filen=atmnm//num(1:2)//'.xyz'
c     elseif(natom.lt.1000)then
c       write(num,'(i3)')natom
c       filen=atmnm//num(1:3)//'.xyz'
c     endif
c     write(*,'(/,a,a)')'Generating ',filen
c     open(ixyz,file=filen,status='unknown')
c     write(ixyz,'(i3)') natom
c     write(ixyz,'(a)') atmnm//num
c     do iatom=1,natom
c       write(ixyz,'(a,3f18.8)')atmnm
c    &  ,(tau(ii,iatom)*bohr,ii=1,3)
c     enddo
c     close(ixyz)
      if(natom.lt.10)then
        write(num,'(i1)')natom
      elseif(natom.lt.100)then
        write(num,'(i2)')natom
      elseif(natom.lt.1000)then
        write(num,'(i3)')natom
      endif
      filen='POSCAR'
      open(ipos,file=filen,status='unknown')
      write(ipos,'(a)')atmnm//num
      write(ipos,'(f20.12)')1.d0
      write(ipos,'(3f20.12)')(a1_sc(ii)*bohr,ii=1,3)
      write(ipos,'(3f20.12)')(a2_sc(ii)*bohr,ii=1,3)
      write(ipos,'(3f20.12)')(a3_sc(ii)*bohr,ii=1,3)
      write(ipos,'(a)')atmnm
      write(ipos,'(i5)')natom
      write(ipos,'(a)')'Selective dynamics'
      write(ipos,'(a)')'Cartesian'
      do iatom=1,natom
        do ii=1,3
          tau_tmp(ii)=tau(ii,iatom)-cslb(ii)*2.d0
          tau_tmp(ii)=tau_tmp(ii)*bohr
        enddo
        write(ipos,'(3f20.12,a)')(tau_tmp(ii),ii=1,3),' T T T'
      enddo
      close(ipos) 
      filen='POSCAR_direct'
      open(ipos,file=filen,status='unknown')
      write(ipos,'(a)')atmnm//num
      write(ipos,'(f20.12)')1.d0
      write(ipos,'(3f20.12)')(a1_sc(ii)*bohr,ii=1,3)
      write(ipos,'(3f20.12)')(a2_sc(ii)*bohr,ii=1,3)
      write(ipos,'(3f20.12)')(a3_sc(ii)*bohr,ii=1,3)
      write(ipos,'(a)')atmnm
      write(ipos,'(i5)')natom
      write(ipos,'(a)')'Direct'
      do iatom=1,natom
        do ii=1,3
           tau_tmp(ii) = taus(ii,iatom) 
        enddo
        write(ipos,'(3f20.12)')(tau_tmp(ii),ii=1,3)
        !write(ipos,'(3f20.12,a)')(tau_tmp(ii),ii=1,3),' T T T'
      enddo
      close(ipos) 
c
      filen='POSCAR_sym'
      open(ipos,file=filen,status='unknown')
      write(ipos,'(a)')atmnm//num
      write(ipos,'(f20.12)')1.d0
      write(ipos,'(3f20.12)')(a1_sc(ii)*bohr,ii=1,3)
      write(ipos,'(3f20.12)')(a2_sc(ii)*bohr,ii=1,3)
      write(ipos,'(3f20.12)')(a3_sc(ii)*bohr,ii=1,3)
      write(ipos,'(a)')atmnm
      write(ipos,'(i5)')natom
      write(ipos,'(a)')'Selective dynamics'
      write(ipos,'(a)')'Cartesian'
      do iatom=1,natom
        do ii=1,3
          tau_tmp(ii)=tau(ii,iatom)-cslb(ii)
          tau_tmp(ii)=tau_tmp(ii)*bohr
        enddo
        write(ipos,'(3f20.12,a)')(tau_tmp(ii),ii=1,3),' T T T'
      enddo
      close(ipos) 
c
c     ==--------------------------------------------------------------==
c     == test                                                         ==
c     ==--------------------------------------------------------------==
c     do iatom=1,natom
c       call pbc(tau(1,iatom),tau_new(1,iatom),a1_sc,a2_sc,a3_sc)
c     enddo
c     write(iout,'(/,a)')      ' New atoms: '
c     do iatom=1,natom
c       write(iout,'(3f18.8)')(tau_new(ii,iatom),ii=1,3)
c     enddo 
c     ==--------------------------------------------------------------==
      close(iout)
c     ==--------------------------------------------------------------==
      stop
      end
