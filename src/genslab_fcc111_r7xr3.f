      program main
c     ==--------------------------------------------------------------==
c     == This is a utility program to generate an fcc(111)            ==
c     == (sqrt(7) x sqrt(3))                                          ==
c     ==--------------------------------------------------------------==
      implicit none
      integer iatom,natom,matom,natom_prim
      parameter(matom=1000)
      integer ii,jj,ilayer,nlayer,nvaclayer,i1,i2,n1,n2,idum
      double precision a_0, a, a_sf,bohr,rlaydist,rvac,z,area,vol
      double precision a1(3),a2(3),a3(3),a1_sc(3),a2_sc(3),a3_sc(3)
      double precision tau(3,matom),taus(3,matom),tau0_tmp(3,6),cslb(3)
      integer zatom
      character*2 atmnm
      character*3 num
      character*20 filen
      integer input,iout,stdin,stdout,stderr,ios,len
      integer ixyz,iatps
      character*80 infile,outfile
      logical fexist
      double precision sum
      double precision xmat(3,3),xmati(3,3)
      double precision ab(3)
c ... arguments
      integer iarg, iargc, narg, marg
      character*80 arg
c ... options
      integer ipw, istate
c     ==--------------------------------------------------------------==
c     == set constants                                                ==
c     ==--------------------------------------------------------------==
      stdin  = 05
      stdout = 06
      stderr = 00
      input  = 10
      iout   = 20
      ixyz   = 25
      iatps  = 26
      bohr   = 0.529177210903d0
c     ==--------------------------------------------------------------==
      ipw=0
      istate=0
c     ==--------------------------------------------------------------==
c ... arguments
      narg=iargc()
      if(narg.gt.0)then
        call getarg(1,arg)
        select case(trim(arg))
          case('-pw') 
            ipw=1
          case('-state')
            istate=1
        end select 
      endif
c     ==--------------------------------------------------------------==
      write(stdout,'(a,$)') 'Enter input file> '
      read(stdin,'(a)') infile
      write(stdout,'(a,$)') 'Enter output file> '
      read(stdin,'(a)') outfile
      inquire(file=infile,exist=fexist)
      if(.not.fexist)then
        len=index(infile,' ')
        write(stderr,'(/,a,a)') infile(:len-1),' does not exist.'
        stop
      endif
      open(input,file=infile,iostat=ios,status='old')
      if(ios.ne.0)then
        write(stderr,'(a)')
     &  '*** ERROR occurs while opening the input file.'
        stop
      endif
      open(iout,file=outfile,status='unknown')
c     ==--------------------------------------------------------------==
      write(iout,'(/,a,a,a,/,a,a,a/,a,a,a,/)')
     &' ***',
     &'***********************************************************',
     &'****',
     &' ***',
     &' This program generates an fcc (111) 1x1 clean surface     ',
     &' ***',
     &' ***',
     &'***********************************************************',
     &'****'
c     ==--------------------------------------------------------------==
      read(input,'(a)')atmnm
      read(input,*) a_0
      read(input,*)nlayer
      read(input,*)nvaclayer
      n1=1
      n2=1
      read(input,*)n1,n2
      a_0 = dble( a_0 )
c ... currently supercell is not considered and n1 and n2 are forced
c ... to be 1
      if( n1 * n2 .gt. 1 )then
        write(stderr,'(a)')
     &  '*** WARNING: n1, n2 > 1 case is not yet considered.'
        write(stderr,'(a)')
     &  '***          n1 and n2 are forced to be 1.         '
       n1 = 1
       n2 = 1
      endif     
c     ==--------------------------------------------------------------==
c     == set lattice vectors                                          ==
c     ==--------------------------------------------------------------==
      a_sf=dble(a_0)/sqrt(2.0d0)
      rlaydist=a_0/sqrt(3.0d0)
c     rvac=rlaydist*dble(nvaclayer+1)
      rvac=rlaydist*dble(nvaclayer)
      a1(1)=2.5d0*a_sf
      a1(2)=0.5d0*sqrt(3.d0)*a_sf
      a1(3)=0.0d0
      a2(1)=0.d0
      a2(2)=sqrt(3.d0)*a_sf
      a2(3)=0.0d0
      a3(1)=0.0d0
      a3(2)=0.0d0
      a3(3)=rlaydist*dble(nlayer)+rlaydist*dble(nvaclayer)
      do ii=1,2
        a1_sc(ii)=dble(n1)*a1(ii)
        a2_sc(ii)=dble(n2)*a2(ii)
        a3_sc(ii)=a3(ii)
      enddo
      a1_sc(3)=a1(3)
      a2_sc(3)=a2(3)
      a3_sc(3)=a3(3)
      ab(1)=a1_sc(2)*a2_sc(3)-a1_sc(3)*a2_sc(2)
      ab(2)=a1_sc(3)*a2_sc(1)-a1_sc(1)*a2_sc(3)
      ab(3)=a1_sc(1)*a2_sc(2)-a1_sc(2)*a2_sc(1)
      area=0.0d0
      do ii=1,3
        area=area+ab(ii)*ab(ii)
      enddo
      area=abs(area)
      area=sqrt(area)
      vol=0.0d0
      do ii=1,3
        vol=vol+ab(ii)*a3_sc(ii)
      enddo
      vol=abs(vol)
c     ==--------------------------------------------------------------==
c     == set atomic positions                                         ==
c     ==--------------------------------------------------------------==
      natom_prim=5
      iatom=0
      natom=nlayer*natom_prim
      if(natom.gt.matom)then
        write(stderr,'(/,a)')
     &  ' *** ERROR: natom > matom'
        write(*,'(a,i5,a,i5)')' natom= ',natom,' matom= ',matom
        stop
      endif
      do ilayer=1,nlayer
        z=(-1.0d0)*dble(ilayer-1)*rlaydist
        tau0_tmp(1,1)= 0.0d0
        tau0_tmp(2,1)= 0.0d0
        tau0_tmp(3,1)= z
        tau0_tmp(1,2)= 0.5d0 * a_sf
        tau0_tmp(2,2)= 0.5d0 * sqrt(3.d0) * a_sf
        tau0_tmp(3,2)= z
        tau0_tmp(1,3)= 1.5d0 * a_sf
        tau0_tmp(2,3)= 0.5d0 * sqrt(3.d0) * a_sf
        tau0_tmp(3,3)= z
        tau0_tmp(1,4)= a_sf
        tau0_tmp(2,4)= sqrt(3.d0) * a_sf
        tau0_tmp(3,4)= z
        tau0_tmp(1,5)= 2.d0 * a_sf
        tau0_tmp(2,5)= sqrt(3.d0) * a_sf
        tau0_tmp(3,5)= z
c
c       tau0_tmp(1,4)=-0.5d0 * a_sf
c       tau0_tmp(2,4)=-0.5d0 * sqrt(3.d0) * a_sf
c       tau0_tmp(3,4)= z
c       tau0_tmp(1,5)=-1.5d0 * a_sf
c       tau0_tmp(2,5)=-0.5d0 * sqrt(3.d0) * a_sf
c       tau0_tmp(3,5)= z
c
        if(mod(ilayer,3).eq.1)then
          do jj=1,natom_prim
            iatom=iatom+1
            tau(1,iatom)=tau0_tmp(1,jj)
            tau(2,iatom)=tau0_tmp(2,jj)
            tau(3,iatom)=tau0_tmp(3,jj)
          enddo
        elseif(mod(ilayer,3).eq.2)then
          do jj=1,natom_prim
            iatom=iatom+1
            tau(1,iatom)=tau0_tmp(1,jj)
            tau(2,iatom)=tau0_tmp(2,jj)+a_sf/sqrt(3.0d0)
            tau(3,iatom)=tau0_tmp(3,jj)
          enddo
        elseif(mod(ilayer,3).eq.0)then
          do jj=1,natom_prim
            iatom=iatom+1
            tau(1,iatom)=tau0_tmp(1,jj)
            tau(2,iatom)=tau0_tmp(2,jj)-a_sf/sqrt(3.0d0)
            tau(3,iatom)=tau0_tmp(3,jj)
          enddo
        endif
      enddo
      if(iatom.ne.natom)then
        write(stderr,'(a)')   ' *** ERROR: iatom != natom'
        write(stderr,'(a,i3)')'            iatom= ',iatom
        stop
      endif
c     ==--------------------------------------------------------------==
      do ii=1,3
        xmat(ii,1)=a1(ii)
        xmat(ii,2)=a2(ii)
        xmat(ii,3)=a3(ii)
      enddo
      call matinv(xmat,xmati,sum)
      do iatom=1,natom
        do ii=1,3
          sum=0.0d0
          do jj=1,3
            sum=sum+xmati(ii,jj)*tau(jj,iatom)
          enddo
          taus(ii,iatom)=sum
        enddo
      enddo
c     ==--------------------------------------------------------------==
      write(iout,*)
      write(iout,'(a,f18.8)')' a_0= ',a_0
      write(iout,'(a,f18.8)')' a  = ',a_sf
      write(iout,'(/,a,i2,a)')' Slab of ',nlayer,' ML'
      write(iout,'(a,i2,a)')
     &' Vacuum region corresponds to ',nvaclayer,' ML'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac,' Bohr'
      write(iout,'(a,f6.2,a)')' Slabs are separated by ',rvac*Bohr,
     &' Angstrom'
      write(iout,'(/,a)')      ' Lattice vectors:'
      write(iout,'(3f20.12)')(a1(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3(ii),ii=1,3)
      write(iout,'(/,a)')      ' Supercell Lattice vectors:'
      write(iout,'(3f20.12)')(a1_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a2_sc(ii),ii=1,3)
      write(iout,'(3f20.12)')(a3_sc(ii),ii=1,3)
      write(iout,'(/,a,f20.12)')' Surface area: ',area
      write(iout,'(a,f20.12)')' Surface area: ',vol/a3_sc(3)
      write(iout,'(/,a,f20.12)')' Volume of unit cell: ',vol
      write(iout,'(/,a,i5)')   ' Number of atoms:',natom
      write(iout,'(a)')      ' Atoms: '
      do iatom=1,natom
        write(iout,'(3f20.12,3i5)')(tau(ii,iatom),ii=1,3),1,1,1
      enddo 
      write(iout,'(/,a)')' Atomic coordinates in reduced coordinate '
      do iatom=1,natom
        write(iout,'(3f20.12,3i5)')(taus(ii,iatom),ii=1,3),1,1,1
      enddo 
c     ==--------------------------------------------------------------==
      if(ipw.eq.1)then
c ... PW output
        filen='atps'
        open(iatps,file=filen,status='unknown')
        write(iatps,'(a)')       ' ibrav = 0'
        write(iatps,'(a,i3)')    ' nat   =',natom
        write(iatps,'(a,i1)')    ' ntyp  =',1
c       write(iatps,'(a,f20.12)')' celldm(1) = ',a1_sc(1)
c       write(iatps,'(a,f20.12)')' celldm(3) = ',a3_sc(3)/a1_sc(1)
        write(iatps,'(a)')'CELL_PARAMETERS (bohr)'
        write(iatps,'(3f20.12)')(a1_sc(ii),ii=1,3)
        write(iatps,'(3f20.12)')(a2_sc(ii),ii=1,3)
        write(iatps,'(3f20.12)')(a3_sc(ii),ii=1,3)
        write(iatps,'(a)')'ATOMIC_POSITIONS (bohr)'
        do iatom=1,natom
          write(iatps,'(a,3f20.12,3i5)')
     &    trim(atmnm),(tau(ii,iatom),ii=1,3),1,1,1
        enddo 
        write(iatps,'(a)')''
        write(iatps,'(a)')'# Atomic positions centered at z=0'
        write(iatps,'(a)')'ATOMIC_POSITIONS (bohr)'
        do iatom=1,natom
          write(iatps,'(a,3f20.12,3i5)')
     &    trim(atmnm),(tau(ii,iatom)-cslb(ii),ii=1,3),1,1,1
        enddo 
        close(iatps) 
c ... PW output in Angstrom
        filen='atps_angstrom'
        open(iatps,file=filen,status='unknown')
        write(iatps,'(a)')       ' ibrav = 0'
        write(iatps,'(a,i3)')    ' nat   =',natom
        write(iatps,'(a,i1)')    ' ntyp  =',1
c       write(iatps,'(a,f20.12)')' A = ',a1_sc(1)*bohr
c       write(iatps,'(a,f20.12)')' C = ',a3_sc(3)*bohr
        write(iatps,'(a)')'CELL_PARAMETERS (angstrom)'
        write(iatps,'(3f20.12)')(a1_sc(ii)*bohr,ii=1,3)
        write(iatps,'(3f20.12)')(a2_sc(ii)*bohr,ii=1,3)
        write(iatps,'(3f20.12)')(a3_sc(ii)*bohr,ii=1,3)
        write(iatps,'(a)')'ATOMIC_POSITIONS (angstrom)'
        do iatom=1,natom
          write(iatps,'(a,3f20.12,3i5)')
     &    trim(atmnm),(tau(ii,iatom)*bohr,ii=1,3),1,1,1
        enddo 
        write(iatps,'(a)')''
        write(iatps,'(a)')'# Atomic positions centered at z=0'
        write(iatps,'(a)')'ATOMIC_POSITIONS (angstrom)'
        do iatom=1,natom
          write(iatps,'(a,3f20.12,3i5)')
     &    trim(atmnm),((tau(ii,iatom)-cslb(ii))*bohr,ii=1,3),1,1,1
        enddo 
        close(iatps) 
      endif
c     ==--------------------------------------------------------------==
c     == generate XYZ file                                            ==
c     ==--------------------------------------------------------------==
      if(natom.lt.10)then
        write(num,'(i1)')natom
        filen=atmnm//num(1:1)//'.xyz'
      elseif(natom.lt.100)then
        write(num,'(i2)')natom
        filen=atmnm//num(1:2)//'.xyz'
      elseif(natom.lt.1000)then
        write(num,'(i3)')natom
        filen=atmnm//num(1:3)//'.xyz'
      endif
      write(*,'(/,a,a)')'Generating ',filen
      open(iout,file=filen,status='unknown')
      write(iout,'(i3)') natom
      write(iout,'(a)') atmnm//num
      do iatom=1,natom
        write(iout,'(a,3f18.8)')atmnm
     &  ,(tau(ii,iatom)*bohr,ii=1,3)
      enddo
      close(iout)
c     ==--------------------------------------------------------------==
c     == generate XSF file                                            ==
c     ==--------------------------------------------------------------==
      if(natom.lt.10)then
        write(num,'(i1)')natom
        filen=atmnm//num(1:1)//'.xsf'
      elseif(natom.lt.100)then
        write(num,'(i2)')natom
        filen=atmnm//num(1:2)//'.xsf'
      elseif(natom.lt.1000)then
        write(num,'(i3)')natom
        filen=atmnm//num(1:3)//'.xsf'
      endif
      write(*,'(/,a,a)')'Generating ',filen
      open(ixyz,file=filen,status='unknown')
      write(ixyz,'(a)')' SLAB'
      write(ixyz,'(a)')' PRIMVEC'
      write(ixyz,'(3f15.6)')(a1_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(3f15.6)')(a2_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(3f15.6)')(a3_sc(ii)*bohr,ii=1,3)
      write(ixyz,'(a)')' PRIMCOORD'
      idum=1
      write(ixyz,'(i5,i1)')natom,idum
      do iatom=1,natom
        idum=zatom(trim(atmnm))
        write(ixyz,'(i5,3(f15.9,2x))')idum
     &  ,(tau(ii,iatom)*bohr,ii=1,3)
      enddo
      close(ixyz)
c     ==--------------------------------------------------------------==
      stop
      end
