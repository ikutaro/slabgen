c     ==================================================================
      function atmnm(zatom)
c     ==--------------------------------------------------------------==
      implicit none
      character*2 atmnm
c     Argument
      integer zatom
c     ==--------------------------------------------------------------==
      atmnm='  '
      if(zatom.gt.103)return
c
      if(zatom.eq. 1)atmnm='H'
      if(zatom.eq. 2)atmnm='He'
      if(zatom.eq. 3)atmnm='Li'
      if(zatom.eq. 4)atmnm='Be'
      if(zatom.eq. 5)atmnm='B'
      if(zatom.eq. 6)atmnm='C'
      if(zatom.eq. 7)atmnm='N'
      if(zatom.eq. 8)atmnm='O'
      if(zatom.eq. 9)atmnm='F'
      if(zatom.eq.10)atmnm='Ne'
      if(zatom.eq.11)atmnm='Na'
      if(zatom.eq.12)atmnm='Mg'
      if(zatom.eq.13)atmnm='Al'
      if(zatom.eq.14)atmnm='Si'
      if(zatom.eq.15)atmnm='P '
      if(zatom.eq.16)atmnm='S '
      if(zatom.eq.17)atmnm='Cl'
      if(zatom.eq.18)atmnm='Ar'
      if(zatom.eq.19)atmnm='K '
      if(zatom.eq.20)atmnm='Ca'
      if(zatom.eq.21)atmnm='Sc'
      if(zatom.eq.22)atmnm='Ti'
      if(zatom.eq.23)atmnm='V '
      if(zatom.eq.24)atmnm='Cr'
      if(zatom.eq.25)atmnm='Mn'
      if(zatom.eq.26)atmnm='Fe'
      if(zatom.eq.27)atmnm='Co'
      if(zatom.eq.28)atmnm='Ni'
      if(zatom.eq.29)atmnm='Cu'
      if(zatom.eq.30)atmnm='Zn'
      if(zatom.eq.31)atmnm='Ga'
      if(zatom.eq.32)atmnm='Ge'
      if(zatom.eq.33)atmnm='As'
      if(zatom.eq.34)atmnm='Se'
      if(zatom.eq.35)atmnm='Br'
      if(zatom.eq.36)atmnm='Kr'
      if(zatom.eq.37)atmnm='Rb'
      if(zatom.eq.38)atmnm='Sr'
      if(zatom.eq.39)atmnm='Y '
      if(zatom.eq.40)atmnm='Zr'
      if(zatom.eq.41)atmnm='Nb'
      if(zatom.eq.42)atmnm='Mo'
      if(zatom.eq.43)atmnm='Tc'
      if(zatom.eq.44)atmnm='Ru'
      if(zatom.eq.45)atmnm='Rh'
      if(zatom.eq.46)atmnm='Pd'
      if(zatom.eq.47)atmnm='Ag'
      if(zatom.eq.48)atmnm='Cd'
      if(zatom.eq.49)atmnm='In'
      if(zatom.eq.50)atmnm='Sn'
      if(zatom.eq.51)atmnm='Sb'
      if(zatom.eq.52)atmnm='Te'
      if(zatom.eq.53)atmnm='I '
      if(zatom.eq.54)atmnm='Xe'
      if(zatom.eq.55)atmnm='Cs'
      if(zatom.eq.56)atmnm='Ba'
      if(zatom.eq.57)atmnm='La'
      if(zatom.eq.58)atmnm='Ce'
      if(zatom.eq.59)atmnm='Pr'
      if(zatom.eq.60)atmnm='Nd'
      if(zatom.eq.61)atmnm='Pm'
      if(zatom.eq.62)atmnm='Sm'
      if(zatom.eq.63)atmnm='Eu'
      if(zatom.eq.64)atmnm='Gd'
      if(zatom.eq.65)atmnm='Tb'
      if(zatom.eq.66)atmnm='Dy'
      if(zatom.eq.67)atmnm='Ho'
      if(zatom.eq.68)atmnm='Er'
      if(zatom.eq.69)atmnm='Tm'
      if(zatom.eq.70)atmnm='Yb'
      if(zatom.eq.71)atmnm='Lu'
      if(zatom.eq.72)atmnm='Hf'
      if(zatom.eq.73)atmnm='Hf'
      if(zatom.eq.74)atmnm='W '
      if(zatom.eq.75)atmnm='Re'
      if(zatom.eq.76)atmnm='Os'
      if(zatom.eq.77)atmnm='Ir'
      if(zatom.eq.78)atmnm='Pt'
      if(zatom.eq.79)atmnm='Au'
      if(zatom.eq.80)atmnm='Hg'
      if(zatom.eq.81)atmnm='Tl'
      if(zatom.eq.82)atmnm='Pb'
      if(zatom.eq.83)atmnm='Bi'
      if(zatom.eq.84)atmnm='Po'
      if(zatom.eq.85)atmnm='At'
      if(zatom.eq.86)atmnm='Rn'
c     ==--------------------------------------------------------------==
      return
      end
c     ==================================================================
      integer function zatom(atmnm)
c     ==--------------------------------------------------------------==
      implicit none
c     Argument
      character*(*) atmnm
c     ==--------------------------------------------------------------==
      zatom=0
      if(atmnm .eq. 'H' .or. atmnm .eq. 'H ' .or. atmnm .eq. ' H')
     &zatom=1
      if(atmnm .eq. 'He')zatom=2
      if(atmnm .eq. 'Li')zatom=3
      if(atmnm .eq. 'Be')zatom=4
      if(atmnm .eq. 'B' .or. atmnm .eq. 'B ' .or. atmnm .eq. ' B')
     &   zatom=5
      if(atmnm .eq. 'C' .or. atmnm .eq. 'C ' .or. atmnm .eq. ' C')
     &   zatom=6
      if(atmnm .eq. 'N' .or. atmnm .eq. 'N ' .or. atmnm .eq. ' N')
     &   zatom=7
      if(atmnm .eq. 'O' .or. atmnm .eq. 'O ' .or. atmnm .eq. ' O')
     &   zatom=8
      if(atmnm .eq. 'F' .or. atmnm .eq. 'F ' .or. atmnm .eq. ' F')
     &   zatom=9
      if(atmnm .eq. 'Ne')zatom=10
      if(atmnm .eq. 'Na')zatom=11
      if(atmnm .eq. 'Mg')zatom=12
      if(atmnm .eq. 'Al')zatom=13
      if(atmnm .eq. 'Si')zatom=14
      if(atmnm .eq. 'P' .or. atmnm .eq. 'P ' .or. atmnm .eq. ' P')
     &   zatom=15
      if(atmnm .eq. 'S' .or. atmnm .eq. 'S ' .or. atmnm .eq. ' S')
     &   zatom=16
      if(atmnm .eq. 'Cl')zatom=17
      if(atmnm .eq. 'Ar')zatom=18
      if(atmnm .eq. 'K' .or. atmnm .eq. 'K ' .or. atmnm .eq. ' K')
     &   zatom=19
      if(atmnm .eq. 'Ca')zatom=20
      if(atmnm .eq. 'Sc')zatom=21
      if(atmnm .eq. 'Ti')zatom=22
      if(atmnm .eq. 'V' .or. atmnm .eq. 'V ' .or. atmnm .eq. ' V')
     &   zatom=23
      if(atmnm .eq. 'Cr')zatom=24
      if(atmnm .eq. 'Mn')zatom=25
      if(atmnm .eq. 'Fe')zatom=26
      if(atmnm .eq. 'Co')zatom=27
      if(atmnm .eq. 'Ni')zatom=28
      if(atmnm .eq. 'Cu')zatom=29
      if(atmnm .eq. 'Zn')zatom=30
      if(atmnm .eq. 'Ga')zatom=31
      if(atmnm .eq. 'Ge')zatom=32
      if(atmnm .eq. 'As')zatom=33
      if(atmnm .eq. 'Se')zatom=34
      if(atmnm .eq. 'Br')zatom=35
      if(atmnm .eq. 'Kr')zatom=36
      if(atmnm .eq. 'Rb')zatom=37
      if(atmnm .eq. 'Sr')zatom=38
      if(atmnm .eq. 'Y' .or. atmnm .eq. 'Y ' .or. atmnm .eq. ' Y')
     &   zatom=39
      if(atmnm .eq. 'Zr')zatom=40
      if(atmnm .eq. 'Nb')zatom=41
      if(atmnm .eq. 'Mo')zatom=42
      if(atmnm .eq. 'Tc')zatom=43
      if(atmnm .eq. 'Ru')zatom=44
      if(atmnm .eq. 'Rh')zatom=45
      if(atmnm .eq. 'Pd')zatom=46
      if(atmnm .eq. 'Ag')zatom=47
      if(atmnm .eq. 'Cd')zatom=48
      if(atmnm .eq. 'In')zatom=49
      if(atmnm .eq. 'Sn')zatom=50
      if(atmnm .eq. 'Sb')zatom=51
      if(atmnm .eq. 'Te')zatom=52
      if(atmnm .eq. 'I' .or. atmnm .eq. 'I ' .or. atmnm .eq. ' I')
     &   zatom=53
      if(atmnm .eq. 'Xe')zatom=54
      if(atmnm .eq. 'Cs')zatom=55
      if(atmnm .eq. 'Ba')zatom=56
c
      if(atmnm .eq. 'La')zatom=57
      if(atmnm .eq. 'Ce')zatom=58
      if(atmnm .eq. 'Pr')zatom=59
      if(atmnm .eq. 'Nd')zatom=60
      if(atmnm .eq. 'Pm')zatom=61
      if(atmnm .eq. 'Sm')zatom=62
      if(atmnm .eq. 'Eu')zatom=63
      if(atmnm .eq. 'Gd')zatom=64
      if(atmnm .eq. 'Tb')zatom=65
      if(atmnm .eq. 'Dy')zatom=66
      if(atmnm .eq. 'Ho')zatom=67
      if(atmnm .eq. 'Er')zatom=68
      if(atmnm .eq. 'Tm')zatom=69
      if(atmnm .eq. 'Yb')zatom=70
      if(atmnm .eq. 'Lu')zatom=71
c
      if(atmnm .eq. 'Hf')zatom=72
      if(atmnm .eq. 'Ta')zatom=73
      if(atmnm .eq. 'W' .or. atmnm .eq. 'W ' .or. atmnm .eq. ' W')
     &   zatom=74
      if(atmnm .eq. 'Re')zatom=75
      if(atmnm .eq. 'Os')zatom=76
      if(atmnm .eq. 'Ir')zatom=77
      if(atmnm .eq. 'Pt')zatom=78
      if(atmnm .eq. 'Au')zatom=79
      if(atmnm .eq. 'Hg')zatom=80
      if(atmnm .eq. 'Tl')zatom=81
      if(atmnm .eq. 'Pb')zatom=82
      if(atmnm .eq. 'Bi')zatom=83
      if(atmnm .eq. 'Po')zatom=84
      if(atmnm .eq. 'At')zatom=85
      if(atmnm .eq. 'Rn')zatom=86
c     ==--------------------------------------------------------------==
      return
      end
