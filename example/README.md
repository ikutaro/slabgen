
This is an example to explain how to run a program contained in the slabgen utility suite.

## Input file example and description

### cu.in
```
Cu
6.83119688
23
14
1  1
```
### description

* 1st line: Name of the element
* 2nd line: lattice constant for the bulk in Bohr
* 3rd line: number of atomic layers for slab
* 4th line: number of atomic layers for vacuum
* 5th line: supercell dimension

### slab genometry
```
 ||     |||     ||| ...
 || ... ||| ... ||| ...
 ||     |||     ||| ...
 <-------> <---> <-... 
   nlay     nvac
 <------------->
    supercell  
```
## Running the program

Execute the following, and one is prompted to answer input and output file names
```
../src/genslab_fcc111
```
Alternatively one can execute
```
../src/genslab_fcc111 < cu.file
```
To obtain the atomic positions in the Quantum-ESPRESSO format, execute:
```
../src/genslab_fcc111 -pw
```
One obtains a file `atps`, which contains the cell parameters and atomic positions.
