======
README
======

This is a set of fortran programs that generate slab models for low index surfaces of elemental materials

Compilation
-----------

Go to the src/ directory, edit Makefile, and type "make."

Usage 
-----

See the example/ directory for usage and examples

Programs
--------

* genslab_fcc111		    : generates a slab for the fcc(111) surface
* genslab_fcc111_r3		    : generates a slab for the fcc(111) (r3 x r3) surface
* genslab_fcc111_3xr3		    : generates a slab for the fcc(111) (3 x r3) surface
* genslab_fcc111_r7xr3		    : generates a slab for the fcc(111) (r7 x r3) surface
* genslab_fcc110		    : generates a slab for the fcc(110) surface
* genslab_fcc110_1x2recon	    : generates a slab for the fcc(110) (1 x 2) reconstructed surface
* genslab_fcc110_1x3recon	    : generates a slab for the fcc(111) (1 x 3) reconstructed surface
* genslab_fcc100		    : generates a slab for the fcc(111) (100) surface
* genslab_fcc322		    : generates a slab for the fcc(111) (322) surface
* genslab_hcp0001		    : generates a slab for the hcp(0001) surface
* genslab_hcp0001_r3		    : generates a slab for the hcp(0001) (r3 x r3) surface
* genslab_bcc100		    : generates a slab for the bcc(100) surface
* genslab_gra			    : generates a slab for the graphite(0001) surface
* genslab_gr			    : generates a graphene structure
* genslab_bn			    : generates a h-BN structure
* water_on_fcc111_r3		    : generates an initial structure for water on a fcc(111) (r3 x r3) surface
* bylayer_up_fcc111_r3		    : generates an initial structure for H-up water bilayer on a fcc(111) (r3 x r3) surface
* bylayer_down_fcc111_r3	    : generates an initial structure for H-down water bilayer on a fcc(111) (r3 x r3) surface
* bilayer_half_dissoc_fcc_fcc111_r3 : generates an initial structure for half-disso. H2O layer on a fcc(111) (r3 x r3) surface

Authors
-------

Ikutaro Hamada
