 program main
 implicit none
 integer :: iatm,natm
 real(kind=8), allocatable :: cps(:,:)
 integer, allocatable :: iwei(:),imdtyp(:),ityp(:)
 real(kind=8)::tx,ty,tz
 real(kind=8)::pi=4.d0*atan(1.d0)
 integer :: iarg,iargc,narg,marg
 character(len=256) :: arg,arg2
 integer :: stdin=05,stdout=06,stderr=00
 integer :: input=10,iout=20
 integer :: ios
 logical :: fexist
 character(len=256) :: infile,outfile
!
 integer :: ii,jj,kk
 character(len=256) :: str
!
 infile=''
 outfile=''
 tx=0.d0
 ty=0.d0
 tz=0.d0
!
 narg=iargc()
 marg=0
 arg_loop: do
   marg=marg+1
   if(marg>narg)exit arg_loop
   call getarg(marg,arg)
   if(trim(arg)=='-x')then
     marg=marg+1
     call getarg(marg,arg2)
     read(arg2,*)tx
     tx=dble(tx)
   elseif(trim(arg)=='-y')then
     marg=marg+1
     call getarg(marg,arg2)
     read(arg2,*)ty
     ty=dble(ty)
   elseif(trim(arg)=='-z')then
     marg=marg+1
     call getarg(marg,arg2)
     read(arg2,*)tz
     tz=dble(tz)
   elseif(trim(arg)=='-h')then
     marg=marg+1
     write(*,'(a)')&
&    'Usage: rotcps [-x|y|z][angle around x|y|z axis] [coordination file]'
     stop
   else
     if(infile.eq.'')then
       infile=trim(arg)
     else
       outfile=trim(arg)
     endif
   endif
 enddo arg_loop
!
 if(infile.eq.'')then
   input=stdin
 endif
 if(outfile.eq.'')then
   iout=stdout
 endif
 if(input==stdin)then
   write(stderr,'(a)')'input file should be specified.'
   stop
 endif
 write(stderr,'(a,a)')'input file : ',trim(infile)
 write(stderr,'(a,a)')'output file: ',trim(outfile)
 write(stderr,'(a,f8.4)')'theta(xy) : ',tx
 write(stderr,'(a,f8.4)')'theta(yz) : ',ty
 write(stderr,'(a,f8.4)')'theta(zx) : ',tz
!
 inquire(file=infile,exist=fexist)
 if(.not.fexist)then
   write(stderr,'(a,a)')trim(infile),' not found.'
   stop
 endif
 open(input,file=infile,status='old')
 natm=0
 do
   read(input,'(a)',iostat=ios)str
   if(ios/=0)exit
   natm=natm+1
 enddo
 rewind(input)
 allocate(cps(3,natm),iwei(natm),imdtyp(natm),ityp(natm))
 do iatm=1,natm
   read(input,*)cps(:,iatm),iwei(iatm),imdtyp(iatm),ityp(iatm)
 enddo
 close(input)
 cps(:,:)=dble(cps(:,:))
!
 do iatm=1,natm
   write(stderr,'(3f20.12,3i5)')cps(:,iatm),iwei(iatm),imdtyp(iatm),ityp(iatm)
 enddo
!
 tx=tx/180.d0*pi
 ty=ty/180.d0*pi
 tz=tz/180.d0*pi
!
 call rotx(cps,natm,tx)
 call roty(cps,natm,ty)
 call rotz(cps,natm,tz)

!
 if(iout/=stdout)then
   open(iout,file=outfile,status='unknown')
 endif
 do iatm=1,natm
   write(iout,'(3f20.12,3i5)')cps(:,iatm),iwei(iatm),imdtyp(iatm),ityp(iatm)
 enddo
 if(iout/=stdout)then
   close(iout)
 endif
!
 stop
 end
!
 subroutine rotz(cps,natm,ang)
!rotate coordinates around z axis
 implicit none
 integer::iatm,natm
 real(kind=8)::cps(3,natm)
 real(kind=8)::ang
 real(kind=8)::cps_tmp(3)
 real(kind=8)::cosa,sina
 cosa=cos(ang)
 sina=sin(ang)
 do iatm=1,natm
   cps_tmp(:)=cps(:,iatm)
   cps(1,iatm)=cosa*cps_tmp(1)-sina*cps_tmp(2)
   cps(2,iatm)=sina*cps_tmp(1)+cosa*cps_tmp(2)
 enddo
 return
 end
!
 subroutine rotx(cps,natm,ang)
!rotate coordinates around x axis
 implicit none
 integer::iatm,natm
 real(kind=8)::cps(3,natm)
 real(kind=8)::ang
 real(kind=8)::cps_tmp(3)
 real(kind=8)::cosa,sina
 cosa=cos(ang)
 sina=sin(ang)
 do iatm=1,natm
   cps_tmp(:)=cps(:,iatm)
   cps(2,iatm)=cosa*cps_tmp(2)-sina*cps_tmp(3)
   cps(3,iatm)=sina*cps_tmp(2)+cosa*cps_tmp(3)
 enddo
 return
 end
!
 subroutine roty(cps,natm,ang)
!rotate coordinates around y axis
 implicit none
 integer::iatm,natm
 real(kind=8)::cps(3,natm)
 real(kind=8)::ang
 real(kind=8)::cps_tmp(3)
 real(kind=8)::cosa,sina
 cosa=cos(ang)
 sina=sin(ang)
 do iatm=1,natm
   cps_tmp(:)=cps(:,iatm)
   cps(3,iatm)=cosa*cps_tmp(3)-sina*cps_tmp(1)
   cps(1,iatm)=sina*cps_tmp(3)+cosa*cps_tmp(1)
 enddo
 return
 end
