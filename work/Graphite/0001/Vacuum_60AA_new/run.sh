#!/bin/sh

nl_list='1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20'

for nls in ${nl_list}
do

cat > gra.in << EOF
Graphite
4.65470981 12.55663615
${nls}
60.0
1 1
#
# Experimental values:
# a = 2.4589 Angstrom
# c = 3.34 Angstrom
EOF

../../../genslab_gra_va < gra.files

nls2=`expr ${nls} \* 2`

grep -A 3 'Primitive' gra.out | tail -3 > altv_${nls}L+${nlv}L
grep -A ${nls2} center gra.out | tail -${nls2} > cps_${nls}L+${nlv}L

grep -A 3 'Primitive' gra.out | tail -3 > sec02
grep -A ${nls2} center gra.out | tail -${nls2} > sec04

mv gra.out gra_${nls}L+${nlv}L.out

natm=`expr ${nls} \* 2`

nbnd=`expr ${natm} \* 2` 

neg=`expr ${nbnd} \+ 40`

cat > sec01 << EOF
 0 0 0 0 0 0                    : input for Platinum in fcc structure
 8.0 30.0 1 ${natm} ${natm}     : GMAX, GMAXP, NTYP, NATM, NATM2
1017  0                         : num_space_group, type of bravis lattice
Cartesian
EOF

cat > sec03 << EOF
  24  24   1   1   1   1        : knx, kny, knz, k-point shift
   1   0                        : NCORD, NINV
EOF

cat > sec05 << EOF
 6  0.1500   12.0107 1 1 0.d0   : TYPE 1IATOMN,ALFA,AMION,ILOC,IVAN,ZETA1
 0 0 0 0 0                      : icond, inipos, inivel, ininos, iniacc
 0 1                            : IPRE, IPRI
 400 200    0   86000.00  0     : NMD1, NMD2, iter_last, CPUMAX,ifstop
    3   1                       : Simple=1,Broyd2=3,Blugel=6,    1:charge, 2:potential mix.
    0   20  0.3                 : starting mixing, kbxmix,alpha
   0.60  0.50  0.60  0.70  1.00 : DTIM1, DTIM2, DTIM3, DTIM4, dtim_last
  30.00     2     1    1.D-10   : DTIO ,IMDALG, IEXPL, EDELTA
  -0.0010  0.10D+03    0        : WIDTH,FORCCR,ISTRESS
rev-vdW-DF2 1                   : XCTYPE, nspin
   1.00     3                   : destm, n_stm
   4                            : NBZTYP 0-SF, 1-BK, 2-SC, 3-BCC, 4-FCC, 5-DIA, 6-HEX, 100-LT, 101-LTC(knx>18), 102-LTC
    0   0   0                   : NKX, NKY, NKZ  (dummy)
    0   0   0                   : NKX2,NKY2,NKZ2 (dummy)
   ${neg}                       : NEG
        1                       : NEXTST(MB)
        0                       : 0; random numbers, 1; matrix diagon
        2                       : imsd, i_2lm, i_sd2another, wksz for phase
        0                       : evaluation of eko difference.0 = no ,1 = yes
        0                       : npdosao(0=no pdos)
        0    0.0                : SM_N, DOPPING(for empirical f-satte correction)
!&ESM
! BOUNDARY_CONDITION BARE
!&END
EOF

cat sec01 sec02 sec03 sec04 sec05 > nfinp_${nls}L_scf

done
