#!/bin/sh

###############################################################################
# STATE
###############################################################################
state_src_dir=~/STATE/src/STATE_5.6.1_20160909
#state_src_dir=~/STATE/src/STATE_5.5.4
STATE=STATE
ln -fs ${state_src_dir}/${STATE} STATE

###############################################################################
# Pseudopotential(s)
###############################################################################
ppdir=~/STATE/gncpp
pps1='C_pbe6TM'		# for 1st-3rd pseudopotentials
pps2=''			# for 4th-6th pseudopotentials

###############################################################################

iopp=36

for pp in $pps1
do
  if [ ! -d ${ppdir}/${pp} ];then
    echo ${ppdir}/${pp} not found.
    exit
  else
    echo ${ppdir}/${pp}
  fi
  iopp=`expr $iopp + 1`
  if [ $iopp -gt 39 ];
  then
    echo 'Number of pseudopotentials specified in variable pps1 is too large.'
    echo 'They must be less than 3.'
    exit
  fi
  echo ${pp} '->' fort.${iopp}
  ln -fs ${ppdir}/${pp}/#vnew.data fort.${iopp}
done

iopp=44

for pp in $pps2
do
  if [ ! -d ${ppdir}/${pp} ];then
    echo ${ppdir}/${pp} not found.
    exit
  else
    echo ${ppdir}/${pp}
  fi
  iopp=`expr $iopp + 1`
  if [ $iopp -gt 46 ];
  then
    echo 'Number of pseudopotentials specified in variable pps2 is too large.'
    echo 'They must be less than 3.'
    exit
  fi
  echo ${pp} '->' fort.${iopp}
  ln -fs ${ppdir}/${pp}/#vnew.data fort.${iopp}
done

ln -fs ${ppdir}/vdwdphi.dat_d0.1 vdwdphi.dat

 
