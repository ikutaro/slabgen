
#list='15L+06L 15L+08L'

bulk_list='18 21 24 27'
vac_list='18'

# bulk_list='15'
# vac_list='06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30'

for b in ${bulk_list}
do

for v in ${vac_list}
do

s=${b}'L+'${v}'L'

cat > files << EOF
cu_${s}.in
cu_${s}.out
EOF

cat > cu_${s}.in << EOF
Cu
6.83110804
${b}
${v} 
 1  1
# Cu: 6.83110804 rev-vdW-DF2 & PBE1(gmax/gmaxp:6/30.0)
# Ni: 6.66562106 PBE PBE4(gmax/gmaxp:5.0/15.0)
# Cu: 6.88248947 PBE PBE3(gmax/gmaxp:5.0/15.0)
# Cu: adjusted to graphene lattice constant of 6.5930016957925712 Bohr
EOF

../../../genslab_fcc111 < files

done 

done
