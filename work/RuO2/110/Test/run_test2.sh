#!/bin/sh

BINDIR=~/STATE/tools/SLABGEN/src
EXEC=genslab_rutile110

INPUT_FILE=ruo2.in
OUTPUT_FILE=ruo2.out

cat > ${INPUT_FILE} << EOF
RuO2
8.486574435396852D0 5.870247573118257D0 : a, c
0.30478D0                               : u
3                                       : # of layers
6                                       : Corresponding # of layers for vacuum
1 1                                     : Supercell dimension
EOF

${BINDIR}/${EXEC} -pw << EOF
${INPUT_FILE}
${OUTPUT_FILE}
EOF
